#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jun  1 16:06:20 2018

@author: thalita

Extracting inception v3 features for mnist
It is a bit of an overkill since:
1) It is an RGB processing net
2) It expects a minimum image size of 139x139 pixels.
This yields features dimension of 2048.
The default size is 299x299

To comply with the input format I repeat the gray-levels
image on the 3 channels and do a 2D interpolation
to augment it.
"""


from keras.applications.inception_v3 import (InceptionV3,
                                             preprocess_input)
from keras.datasets import mnist
from keras import backend as K
import numpy as np
from tqdm import tqdm
import tensorflow as tf


def inceptionV3_feature(img_batch):
    resize = tf.image.resize_bilinear(
        tf.convert_to_tensor(img_batch), (139, 139))
    with tf.Session() as sess:
        img_batch = sess.run(resize)
    _, h, w, ch = img_batch.shape
    model = InceptionV3(weights='imagenet', input_shape=(h,w,ch),
                        include_top=False)
    img_batch = preprocess_input(img_batch)
    features = model.predict(img_batch)
    return features

def extract_features():
    tf.logging.set_verbosity(tf.logging.INFO)

    (x_train, y_train), (x_test, y_test) = mnist.load_data()
    images = np.concatenate((x_train, x_test), axis=0)


    if K.image_data_format() == 'channels_first':
        images = images.reshape((images.shape[0], 1, images.shape[1], images.shape[2]))
        images = np.repeat(images, 3, axis=1)
    else:  # channels last
        images = images.reshape((images.shape[0], images.shape[1], images.shape[2], 1))
        images = np.repeat(images, 3, axis=-1)

    batch_size = bs = images.shape[0]//5
    features = [inceptionV3_feature(images[i:i+batch_size, ...])
                for i in tqdm(range(0, images.shape[0], bs),
                              desc="feature xtract")]
    features = np.concatenate(features, axis=0)
    np.save("./Data/mnist_inceptionV3_feature_maps.npy", features)
    if K.image_data_format() == 'channels_first':
        features = np.mean(features, axis=(3, 4))
    else:  # channels last
        features = np.mean(features, axis=(1, 2))

    np.save("./Data/mnist_inceptionV3_features.npy", features)

if __name__ == "__main__":
    extract_features()