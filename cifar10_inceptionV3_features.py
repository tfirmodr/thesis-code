#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr 03 17:32:52 2017

@author: thalita

Extracting inception v3 features for cifar10.
It expects a minimum image size of 139x139 pixels.
The default size is 299x299
This yields features dimension of 2048.
It is quite an overkill as the images are 32x32.


"""

from keras.applications.inception_v3 import (InceptionV3,
                                             preprocess_input)
import numpy as np
from tqdm import tqdm
import tensorflow as tf

from keras.datasets import cifar10

def inceptionV3_feature(img_batch):
    resize = tf.image.resize_bilinear(
        tf.convert_to_tensor(img_batch), (139, 139))
    with tf.Session() as sess:
        img_batch = sess.run(resize)
    _, h, w, ch = img_batch.shape
    model = InceptionV3(weights='imagenet', input_shape=(h,w,ch),
                        include_top=False)
    img_batch = preprocess_input(img_batch)
    features = model.predict(img_batch)
    return features

def extract_features():
    tf.logging.set_verbosity(tf.logging.INFO)

    (x_train, y_train), (x_test, y_test) = cifar10.load_data()
    images = np.concatenate([x_train,x_test])

    batch_size = bs = images.shape[0]//15
    features = [inceptionV3_feature(images[i:i+batch_size, :, :, :])
                for i in tqdm(range(0, images.shape[0], bs),
                              desc="feature xtract")]
    features = np.concatenate(features)
    np.save("./Data/cifar10_inceptionV3_feature_maps.npy", features)
    features = np.mean(features, axis=(1, 2))
    np.save("./Data/cifar10_inceptionV3_features.npy", features)

if __name__ == "__main__":
    extract_features()