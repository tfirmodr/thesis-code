#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Nov 24 16:55:06 2017

@author: thalita
"""
import numpy as np

def center_crop(images):
    # read dimensions properly
    if images.ndim == 3:
        n_images, height, width = images.shape
    elif images.ndim == 4:
        n_images, height, width, _ = images.shape
    # center crop to square images in case they are not square
    d = int(np.abs(height-width))
    if height > width:
        if d / 2 == 0:
            d1 = d//2
            images = images[:, d1:height-d1, :, ...]
        else:
            d1 = (d-1)//2
            d2 = (d+1)//2
            images = images[:, d1:height-d2, :, ...]

    elif width > height:
        if d / 2 == 0:
            d1 = d/2
            images = images[:, :, d1:width-d1, ...]
        else:
            d1 = (d-1)//2
            d2 = (d+1)//2
            images = images[:, :, d1:width-d2, ...]

    return images


def sprite(images):
    # make a copy of the images array
    images = images[...]
    # center crop to square images in case they are not square
    images = center_crop(images)
    # read dimensions properly
    if images.ndim == 3:
        n_images, height, width = images.shape
    elif images.ndim == 4:
        n_images, height, width, _ = images.shape
    assert(height==width)
    # define number of rows and columns for display grid
    size = int(np.ceil(np.sqrt(n_images)))
    # sprite needs to be sqaure so complete with zero "images" until reaching
    # a perfect square
    while (size**2) > images.shape[0]:
        print(images.shape[0])
        images = np.append(images,
                           np.expand_dims(np.zeros_like(images[0,...]), 0),
                           axis=0)
    # rehsape n_images dimension
    images = images.reshape((size, size, height, width,-1))
    images = images.transpose([0, 2, 1, 3, -1])
    # reshape to image shape
    images = images.reshape(size*height,size*width, -1)
    return images.squeeze()


def gen_image(string, shape):
    from PIL import Image, ImageDraw, ImageFont
    font = ImageFont.truetype("Arial.ttf", 11)
    testImg = Image.new('RGB', shape, (255,255,255))
    d = ImageDraw.Draw(testImg)
    d.text([2,2], string, fill=(0,0,255), font=font)
    return np.fromstring(testImg.tobytes(), dtype=np.uint8).reshape(list(shape)+[3])

def gen_images(M, shape):
    from skimage.color import rgb2gray
    images = []
    gray = []
    for i in range(M):
        img = gen_image(str(i), shape)
        images.append(img)
        gray.append(rgb2gray(img))
    images = np.stack(images)
    gray = np.stack(gray)
    return images, gray

def test_sprite():
    from skimage.io import imshow
    import matplotlib.pyplot as plt
    M = 15
    shape=(28,28)
    images, gray = gen_images(M, shape)
    sp = sprite(images)
    print(sp.shape)
    assert(sp.shape==(112,112,3))
    imshow(sp)
    sp = sprite(gray)
    print(sp.shape)
    assert(sp.shape==(112,112))
    plt.figure()
    imshow(sp)
    M = 15
    shape=(28,29)
    images, gray = gen_images(M, shape)
    sp = sprite(images)
    print(sp.shape)
    assert(sp.shape==(112,112,3))
    plt.figure()
    imshow(sp)
    sp = sprite(gray)
    print(sp.shape)
    assert(sp.shape==(112,112))
    plt.figure()
    imshow(sp)

if __name__ == "__main__":
    test_sprite()