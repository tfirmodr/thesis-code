#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct 15 16:22:27 2018

@author: thalita
"""
# %% Imports
import os
from glob import glob
import numpy as np

import torch
import torch.nn as nn
import torch.optim as optim

from torch.utils.data import Dataset, ConcatDataset
from torchvision import models

from sklearn.model_selection import RepeatedStratifiedKFold
from sklearn.base import clone

from hyperopt import hp, fmin, tpe, Trials, space_eval

from skorch import NeuralNetClassifier
from skorch.callbacks import LRScheduler, Checkpoint, ProgressBar
from skorch.helper import filtered_optimizer
from skorch.helper import filter_requires_grad
from skorch.helper import predefined_split

import cv2
import albumentations as alb

from sacred import Experiment
from sacred.observers import FileStorageObserver
from sacred.serializer import restore
import json

import pandas as pd

from skorch_utils import MLP

# %% General definitions
name = 'sumatra_transferlearning'



class AlbumentationsDataset(Dataset):
    """
    Dataset structure for albumentations
    __init__ and __len__ functions are the same as in TorchvisionDataset
    """
    def __init__(self, filepaths, labels, transform=None):
        self.file_paths = filepaths
        self.labels = labels
        self.transform = transform

    def __len__(self):
        return len(self.file_paths)

    def __getitem__(self, idx):
        label = self.labels[idx]
        file_path = self.file_paths[idx]

        # Read an image with OpenCV
        image = cv2.imread(file_path)

        # By default OpenCV uses BGR color space for color images,
        # so we need to convert the image to RGB color space.
        image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        if self.transform:
            augmented = self.transform(image=image)
            image = augmented['image']
            # format is channels first
            image = image.transpose([2,0,1])
        return image, label


class AugmentedDataset(ConcatDataset):
    def __init__(self, filepaths, labels, transforms=None):
        datasets = []
        for t in transforms:
            datasets.append(AlbumentationsDataset(filepaths, labels, t))
            super().__init__(datasets)


class Rotate90(alb.RandomRotate90):
    def __init__(self, factor=90,  p=1.0):
        super().__init__(p)
        self.factor = factor

    def get_params(self):
        return {'factor': self.factor}


def get_filepaths():
    path = "Data/fotos_sumatra/"
    dirs = os.listdir(path)
    bad = [e for e in dirs if e.find('AVEC') > -1][0]
    good = [e for e in dirs if e.find('SANS') > -1][0]
    bad = os.path.join(path, bad)
    good = os.path.join(path, good)
    bad = [os.path.join(bad, f) for f in os.listdir(bad)]
    good = [os.path.join(good, f) for f in os.listdir(good)]

    labels = np.array([0]*len(good) + [1]*len(bad))

    return good+bad, labels


def get_transforms():

    '''
    Datasets are normalized with mean: [0.485, 0.456, 0.406],
    and standard deviation: [0.229, 0.224, 0.225].
    These values are the means and standard deviations of the ImageNet images.
    We used these values because the pretrained model was trained on ImageNet.
    '''
    train_transforms = alb.Compose([
        alb.Resize(256, 256),
        alb.RandomCrop(224, 224),
        alb.Rotate(limit=315, p=1),
        alb.RandomBrightness(limit=0.2, p=0.5),
        alb.Flip(p=0.5),
        alb.Normalize(
            mean=[0.485, 0.456, 0.406],
            std=[0.229, 0.224, 0.225])
    ])

    val_pre_transf = [alb.Resize(224, 224)]
    val_pos_transf = [alb.Normalize(
            mean=[0.485, 0.456, 0.406],
            std=[0.229, 0.224, 0.225])]
    val_transfs = [
            alb.NoOp(),
            Rotate90(factor=1, p=1),
            Rotate90(factor=2, p=1),
            Rotate90(factor=3, p=1)]
    val_transforms = [
            alb.Compose([t], val_pre_transf, val_pos_transf)
            for t in val_transfs]
    return train_transforms, val_transforms



# %% Sacred experiment
ex = Experiment(name)
observer = FileStorageObserver.create(name)
ex.observers.append(observer)


@ex.config
def conf():
    seed=204573976
    weight_decay=1e-4
    module__drop_proba=0.5
    module__num_units=[512]  # list of sizes for hidden layers, use [0] for no hidden layer
    lr = 0.001  # learning rate
    lr_decay = 0.1
    lr_step = 7
    batch_size=4  #batch size
    max_epochs=25  # max number of epochs to train
    n_splits=2  # number of splits for RepeatedStratifiedKFold
    n_repeats=5 # number of repeats for RepeatedStratifiedKFold
    cuda=False  # use cuda or not (bool)
    max_evals=50  # for hyperopt
    resume=0  # run id to resume trials_space from (hyperopt)
    run_id=None  # for results command

def get_run_results(fname):
    with open(fname, 'r') as file:
        run = restore(json.load(file))
    return run['result']


@ex.command(unobserved=True)
def results(run_id=None):
    if run_id is not None:
        fname = os.path.join(name, str(run_id), 'run.json')
        print(get_run_results(fname))
    else:
        info_files = glob(name + "/*/info.json")
        for fname in info_files:
            try:
                print(get_run_results(fname))
            except AttributeError:
                pass


@ex.capture
def get_model(
        _run,
        seed,
        lr,
        lr_decay,
        lr_step,
        weight_decay,
        module__drop_proba, module__num_units,
        batch_size,
        max_epochs,
        cuda):
    # load resnet 18
    # model has conv1, layer1 to layer4 then fc
    model_ft = models.resnet18(pretrained=True)
    num_ftrs = model_ft.fc.in_features
    # replace fc with a MLP (0,1 or multiple fc layers)
    model_ft.fc = MLP(n_in=num_ftrs,
                      num_units=module__num_units,
                      n_out=2,
                      drop_proba=module__drop_proba,
                      nonlin=torch.tanh)
    # freeze layers
    for name, param in model_ft.named_parameters():
        if not name.startswith('fc'):
            param.requires_grad_(False)

    # callbacks
    lrscheduler = LRScheduler(
        policy='StepLR', step_size=lr_step, gamma=lr_decay)

    weights_file = '/tmp/best_model.pt'
    checkpoint = Checkpoint(
        f_params=weights_file,
        monitor='valid_acc_best')

    callbacks = [lrscheduler, checkpoint]

    # optimizer
    optimizer = filtered_optimizer(
        optim.SGD, filter_requires_grad
    )

    net = NeuralNetClassifier(
        model_ft,
        criterion=nn.CrossEntropyLoss,
        lr=lr,
        batch_size=batch_size,
        max_epochs=max_epochs,
        train_split=None,
        optimizer=optimizer,
        optimizer__momentum=0.9,
        optimizer__weight_decay=weight_decay,
        iterator_train__shuffle=True,
        iterator_train__num_workers=4,
        iterator_valid__shuffle=True,
        iterator_valid__num_workers=4,
        callbacks=callbacks,
        device='cuda' if cuda else 'cpu' # comment to train on cpu
    )
    return net

@ex.capture
def cv_score(_run, seed, n_splits, n_repeats,
             param_dict=None):

    # get nn estimator
    if param_dict is not None:
        kwargs = {}
        delkeys = []
        for param in filter(lambda k: k.startswith('module'), param_dict):
            kwargs.update({param: param_dict[param]})
            delkeys.append(param)
        for key in delkeys: del param_dict[key]
        estimator = get_model(**kwargs)
    else:
        estimator = get_model()

    # setup data
    train_transforms, val_transforms = get_transforms()
    filepaths, labels = get_filepaths()

    # cross validate
    cv = RepeatedStratifiedKFold(
            n_splits=n_splits, n_repeats=n_repeats, random_state=seed)
    dummyX, dummyY = np.zeros((len(labels),1)), labels

    if not 'cv' in _run.info:
       _run.info['cv'] = []
    _run.info['cv'].append(dict())
    cv_order = len(_run.info['cv']) - 1

    for split, (train, val) in enumerate(cv.split(dummyX, dummyY)):
        train_ds = AlbumentationsDataset(
                [filepaths[i] for i in train],
                [labels[i] for i in train],
                train_transforms)
        val_ds = AugmentedDataset(
                [filepaths[i] for i in val],
                [labels[i] for i in val],
                val_transforms)

        net = clone(estimator)
        net.initialize()
        net.set_params(train_split=predefined_split(val_ds))
        if param_dict is not None:
            net.set_params(**param_dict)

        net.fit(train_ds, y=None)

        _run.info['cv'][-1]['history'] = {}
        _run.info['cv'][-1]['history'][split] = net.history
        ex.add_artifact(
                net.get_params()['callbacks__Checkpoint__f_params'],
                name='weights_%d_%d.pt' % (cv_order, split))

    val_accs = [np.max(history[:, 'valid_acc'])
        for split, history in _run.info['cv'][-1]['history'].items()]

    _run.info['cv'][-1]['results'] = {}
    _run.info['cv'][-1]['results']['mean_val_acc'] = np.mean(val_accs)
    _run.info['cv'][-1]['results']['std_val_acc'] = np.std(val_accs)
    print("validation results: %0.4f +- %0.4f" %
          (np.mean(val_accs), np.std(val_accs)))
    return {'mean_val_acc': np.mean(val_accs),
            'std_val_acc' : np.std(val_accs),
            'val_accs': val_accs}


@ex.capture
def hyperopt_space(seed,
         lr,
         lr_decay,
         lr_step,
         weight_decay,
         module__drop_proba,
         batch_size,
         max_epochs,
         module__num_units):
    space = {}

    def add_param(param_name, param_dist,
                  *param_dist_args, **param_dist_kwargs):
        space[param_name] = param_dist(param_name,
             *param_dist_args,**param_dist_kwargs)

    if isinstance(lr, list):
        add_param('optimizer__lr', hp.uniform, *lr)
    if isinstance(max_epochs, list):
        add_param('max_epochs', hp.choice, max_epochs)
    if isinstance(batch_size, list):
        add_param('batch_size', hp.choice, batch_size)
    if isinstance(weight_decay, list):
        lims = [v*np.log(10) for v in weight_decay]
        add_param('optimizer__weight_decay', hp.loguniform, *lims)

    if isinstance(lr_step, list):
        add_param('callbacks__LRScheduler__step_size', hp.choice, lr_step)
    if isinstance(lr_decay, list):
        add_param('callbacks__LRScheduler__gamma', hp.uniform, *lr_decay)

    if isinstance(module__drop_proba, list):
        add_param('module__drop_proba', hp.uniform, *module__drop_proba)
    if isinstance(module__num_units, list):
        if isinstance(module__num_units[0], list):
            add_param('module__num_units', hp.choice, module__num_units)
    return space


@ex.command
def hyperopt(_rnd, _run,_log,  max_evals, resume):
    " give lists instead of single parameters to use the hp search"

    # The Trials object will store details of each iteration

    trials_fname = "/tmp/trials_space.pkl"

    if resume:
        basename = os.path.basename(trials_fname)
        archive_trials = os.path.join(name, str(resume), basename)
        if os.path.exists(archive_trials):
            _log.info("Resuming hyperopt trials from run ", str(resume))
            trials, space = pd.read_pickle(archive_trials)
        else:
            _log.warning("No trials file ", archive_trials,
                         " found in run " , str(resume))
    else:
        trials = Trials()
        # define search space according to arguments
        space = hyperopt_space()

    # define objective function
    def objective(params):
        # cv_score return accuracy, hyperopt is a minimizer
        mean_acc = cv_score(param_dict=params)['mean_val_acc']
        return (1-mean_acc)

    # Run the hyperparameter search using the tpe algorithm
    best = fmin(objective,
                space,
                algo=tpe.suggest,
                max_evals=max_evals,
                trials=trials,
                rstate=_rnd)
    # Get the values of the optimal parameters
    best_params = space_eval(space, best)
    import pdb; pdb.set_trace()

    #save trials and space to resume later
    pd.to_pickle((trials, space), trials_fname)
    ex.add_artifact(trials_fname)

    print("best params", best_params)
    return {"best params": best_params,
            "best_result": trials.best_trial['result']}

@ex.automain
def main():
    # cross validate
    return cv_score()



