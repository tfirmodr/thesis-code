#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr 18 17:46:32 2018

@author: thalita
"""

from sklearn.base import TransformerMixin, ClassifierMixin
import numpy as np
import sys

import torch
from torch import nn
import torch.nn.functional as F
from torch.optim import Optimizer

from skorch.utils import duplicate_items
from skorch.utils import get_dim
from skorch.utils import to_tensor, to_numpy
from skorch.utils import params_for
from skorch.callbacks import Callback
import skorch.callbacks.lr_scheduler as lr_scheduler
from torch.optim.lr_scheduler import _LRScheduler, ReduceLROnPlateau
from skorch.net import NeuralNet
from skorch.classifier import NeuralNetClassifier
from functools import partial
from tempfile import mktemp
from collections import defaultdict

import matplotlib.pyplot as plt


class StopperNet(object):
    """
    StopperNet.

    This class captures the keyboardInterrupt exception during the training
    loop, setting the attribute stop to true.
    Any model that needs to run fit() by mutiple calls to partial_fit() can use
    this class to know when this exception has been raised and stop training.
    Normal skorch behavior is to capture the exception and do nothing.
    """

    def initialize(self):
        super().initialize()
        self.stop = False

    def partial_fit(self, X, y=None, classes=None, **fit_params):
        """Fit the module.

        If the module is initialized, it is not re-initialized, which
        means that this method should be used if you want to continue
        training a model (warm start).

        Parameters
        ----------
        X : input data, compatible with skorch.dataset.Dataset
          By default, you should be able to pass:

            * numpy arrays
            * torch tensors
            * pandas DataFrame or Series
            * a dictionary of the former three
            * a list/tuple of the former three

          If this doesn't work with your data, you have to pass a
          ``Dataset`` that can deal with the data.

        y : target data, compatible with skorch.dataset.Dataset
          The same data types as for ``X`` are supported.

        classes : array, sahpe (n_classes,)
          Solely for sklearn compatibility, currently unused.

        **fit_params : dict
          Additional parameters passed to the ``forward`` method of
          the module and to the train_split call.

        """
        if not self.initialized_:
            self.initialize()

        self.notify('on_train_begin', X=X, y=y)
        try:
            self.fit_loop(X, y, **fit_params)
        except KeyboardInterrupt:
            self.stop = True
        self.notify('on_train_end', X=X, y=y)
        return self

class TransformerNet(TransformerMixin):
    def initialize(self):
        super().initialize()
        self.transform_args = None

    def transform(self, X, **forward_kwargs):
        self.transform_args = forward_kwargs
        out = self.predict_proba(X)
        self.transform_args = None
        return out

    def evaluation_step(self, Xi, training=False):
        """Perform a forward step to produce the output used for
        prediction and scoring.

        Therefore the module is set to evaluation mode by default
        beforehand which can be overridden to re-enable features
        like dropout by setting ``training=True``.

        """
        self.module_.train(training)
        if self.transform_args is not None:
            return self.infer(Xi, **self.transform_args)
        else:
            return self.infer(Xi)

class MLP(nn.Module):
    def __init__(self, n_in=2, num_units=20, n_out=10,
                 drop_proba=0.5, nonlin=F.relu):
        super().__init__()
        if num_units != 0:
            if type(num_units) is int:
                num_units = tuple([num_units,])
            self.n_hidden = len(num_units)
            self.__setattr__('hidden1', nn.Linear(n_in, num_units[0]))
            prev_layer = num_units[0]
            for layer_ix in range(1, len(num_units)):
                this_layer = num_units[layer_ix]
                self.__setattr__('hidden%d' % (layer_ix+1),
                                      nn.Linear(prev_layer, this_layer))
                prev_layer = this_layer
        else:
            self.n_hidden = 0
            num_units = [n_in]

        self.nonlin = nonlin
        if drop_proba is not None and drop_proba != 0:
            self.dropout = nn.Dropout(drop_proba)
        else:
            self.dropout = None
        self.output = nn.Linear(num_units[-1], n_out)


    def forward(self, X, name='output', **kwargs):
        if self.n_hidden >= 1:
            for i in range(self.n_hidden):
                layer_name = 'hidden%d' % (i + 1)
                layer = self.__getattr__(layer_name)
                X = layer(X)
                X = self.nonlin(X)
                if name == layer_name:
                    return X
        if self.dropout is not None:
            X = self.dropout(X)
        if name != 'output':
            raise Warning("name %s dos not correspont to any layers," % name +
                          " returning output" )
        #X = F.log_softmax(self.output(X), dim=-1)
        return X


class CNN(nn.Module):
    def __init__(self, in_shape, kernel_size=3, n_filters=16, n_out=10,
                 drop_proba=0.5, nonlin=F.relu):
        super().__init__()
        if type(n_filters) is int:
            n_filters = tuple([n_filters,])
        if type(kernel_size) is int:
            kernel_size = tuple([kernel_size[0]]*len(n_filters))
        if len(kernel_size) != len(n_filters):
            raise ValueError("kernel size is either one int or list of ints\
                             matching len of n_filters")
        self.n_layers = len(n_filters)
        channels, height, width = in_shape
        self.__setattr__('conv1',
                         nn.Conv2d(channels, n_filters[0], kernel_size[0]))
        prev_layer = n_filters[0]
        for layer_ix in range(1, len(n_filters)):
            this_layer = n_filters[layer_ix]
            kernel_size
            self.__setattr__('conv%d' % (layer_ix+1),
                             nn.Conv2d(prev_layer, this_layer,
                                       kernel_size[layer_ix]))
            prev_layer = this_layer
        self.nonlin = nonlin
        if drop_proba is not None and drop_proba != 0:
            self.dropout = nn.Dropout(drop_proba)
        else:
            self.dropout = None


        self.output = nn.Conv1D(n_filters[-1]*height*width, n_out, kernel_size=(1))


    def forward(self, X, name='output', **kwargs):
        for i in range(self.n_hidden):
            layer_name = 'hidden%d' % (i + 1)
            layer = self.__getattr__(layer_name)
            X = layer(X)
            X = self.nonlin(X)
            if name == layer_name:
                return X
        if self.dropout is not None:
            X = self.dropout(X)
        if name != 'output':
            raise Warning("name %s dos not correspont to any layers," % name +
                          " returning output" )
        X = self.output(X)
        X = F.avg_pool2d()
        X = F.softmax(self.output(X), dim=-1)
        return X


class NNClassifier(TransformerNet, NeuralNetClassifier, ClassifierMixin):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)


def learning_curve(model):
    net = model
    loss = [net.history[i, 'train_loss'] for i in range(len(net.history))]
    valid_loss = [net.history[i, 'valid_loss'] for i in range(len(net.history))]
    val_acc = [net.history[i, 'valid_acc'] for i in range(len(net.history))]
    plt.figure()
    plt.plot(loss, linestyle=':', label='train')
    lines = plt.plot(valid_loss, linestyle=':', label="valid")
    plt.xlabel("epochs")
    plt.ylabel("loss")
    plt.legend(loc='best')
    ax2 = plt.gca().twinx()
    ax2.plot(val_acc, label='valid', color=lines[0].get_color())
    plt.ylabel('accuracy')
    plt.title('Learning curve')


class SaveWeights(Callback):
    def __init__(self, every_n_epochs=100):
        self.every_n_epochs = every_n_epochs

    def initialize(self):
        self.params = []
        self.epochs = []
        return self

    def on_epoch_end(self, net, **kwargs):
        epochs = len(net.history)
        if not (epochs % self.every_n_epochs):
            p_t = list(net.module_.named_parameters())
            p_t = dict(p_t)
            for k, v in p_t.items():
                p_t[k] = to_numpy(v)
            self.params.append(p_t)
            self.epochs.append(epochs - 1)

class GradientInspector(Callback):
    def __init__(self, frequency=100):
        self.frequency = frequency

    def initialize(self):
        super().initialize()
        self.grads = defaultdict(list)

    def on_grad_computed(self, net, named_parameters, **kwargs):
        epochs = len(net.history)
        if not(epochs % self.frequency):
            for name, par in named_parameters:
                self.grads[name].append(par)


class NaNStopping(Callback):
    def on_epoch_end(self, net, **kwargs):
        if np.isnan(net.history[-1, 'train_loss']):
            print("NaN stopping @ ", len(net.history))
            raise KeyboardInterrupt

class _ReduceLROnPlateau(ReduceLROnPlateau):
    """
    Reimplementing to have a picklable version...
    -----------------------------------------------------
    Reduce learning rate when a metric has stopped improving.
    Models often benefit from reducing the learning rate by a factor
    of 2-10 once learning stagnates. This scheduler reads a metrics
    quantity and if no improvement is seen for a 'patience' number
    of epochs, the learning rate is reduced.

    Args:
        optimizer (Optimizer): Wrapped optimizer.
        mode (str): One of `min`, `max`. In `min` mode, lr will
            be reduced when the quantity monitored has stopped
            decreasing; in `max` mode it will be reduced when the
            quantity monitored has stopped increasing. Default: 'min'.
        factor (float): Factor by which the learning rate will be
            reduced. new_lr = lr * factor. Default: 0.1.
        patience (int): Number of epochs with no improvement after
            which learning rate will be reduced. Default: 10.
        verbose (bool): If ``True``, prints a message to stdout for
            each update. Default: ``False``.
        threshold (float): Threshold for measuring the new optimum,
            to only focus on significant changes. Default: 1e-4.
        threshold_mode (str): One of `rel`, `abs`. In `rel` mode,
            dynamic_threshold = best * ( 1 + threshold ) in 'max'
            mode or best * ( 1 - threshold ) in `min` mode.
            In `abs` mode, dynamic_threshold = best + threshold in
            `max` mode or best - threshold in `min` mode. Default: 'rel'.
        cooldown (int): Number of epochs to wait before resuming
            normal operation after lr has been reduced. Default: 0.
        min_lr (float or list): A scalar or a list of scalars. A
            lower bound on the learning rate of all param groups
            or each group respectively. Default: 0.
        eps (float): Minimal decay applied to lr. If the difference
            between new and old lr is smaller than eps, the update is
            ignored. Default: 1e-8.

    Example:
        >>> optimizer = torch.optim.SGD(model.parameters(), lr=0.1, momentum=0.9)
        >>> scheduler = ReduceLROnPlateau(optimizer, 'min')
        >>> for epoch in range(10):
        >>>     train(...)
        >>>     val_loss = validate(...)
        >>>     # Note that step should be called after validate()
        >>>     scheduler.step(val_loss)
    """

    def __init__(self, optimizer, mode='min', factor=0.1, patience=10,
                 verbose=False, threshold=1e-4, threshold_mode='rel',
                 cooldown=0, min_lr=0, eps=1e-8):

        if factor >= 1.0:
            raise ValueError('Factor should be < 1.0.')
        self.factor = factor

        if not isinstance(optimizer, Optimizer):
            raise TypeError('{} is not an Optimizer'.format(
                type(optimizer).__name__))
        self.optimizer = optimizer

        if isinstance(min_lr, list) or isinstance(min_lr, tuple):
            if len(min_lr) != len(optimizer.param_groups):
                raise ValueError("expected {} min_lrs, got {}".format(
                    len(optimizer.param_groups), len(min_lr)))
            self.min_lrs = list(min_lr)
        else:
            self.min_lrs = [min_lr] * len(optimizer.param_groups)

        self.patience = patience
        self.verbose = verbose
        self.cooldown = cooldown
        self.cooldown_counter = 0
        self.mode = mode
        self.threshold = threshold
        self.threshold_mode = threshold_mode
        self.best = None
        self.num_bad_epochs = None
        self.mode_worse = None  # the worse value for the chosen mode
        self.eps = eps
        self.last_epoch = -1
        self._init_is_better(mode=mode, threshold=threshold,
                             threshold_mode=threshold_mode)
        self._reset()

    def is_better(self, a, best):
        if self.mode == 'min' and self.threshold_mode == 'rel':
            rel_epsilon = 1. - self.threshold
            return a < best * rel_epsilon
        elif self.mode == 'min' and self.threshold_mode == 'abs':
            return a < best - self.threshold
        elif self.mode == 'max' and self.threshold_mode == 'rel':
            rel_epsilon = self.threshold + 1.
            return a > best * rel_epsilon
        else:  # mode == 'max' and epsilon_mode == 'abs':
            return a > best + self.threshold

    def _init_is_better(self, mode, threshold, threshold_mode):
        if mode not in {'min', 'max'}:
            raise ValueError('mode ' + mode + ' is unknown!')
        if threshold_mode not in {'rel', 'abs'}:
            raise ValueError('threshold mode ' + mode + ' is unknown!')
        if mode == 'min' and threshold_mode == 'rel':
            self.mode_worse = float('Inf')
        elif mode == 'min' and threshold_mode == 'abs':
            self.mode_worse = float('Inf')
        elif mode == 'max' and threshold_mode == 'rel':
            self.mode_worse = -float('Inf')
        else:  # mode == 'max' and epsilon_mode == 'abs':
            self.mode_worse = -float('Inf')


#class LRScheduler(Callback):
#    """ Reimplementing LRScheduler to fix kwargs issue
#    sklearn can't clone the object because kwargs is not set by constructor.
#    Callback that sets the learning rate of each
#    parameter group according to some policy.
#
#    Parameters
#    ----------
#
#    policy : str or _LRScheduler class (default='WarmRestartLR')
#      Learning rate policy name or scheduler to be used.
#
#    """
#
#    def __init__(self, policy="WarmRestartLR", policy_kwargs=None):
#        if isinstance(policy, str):
#            if policy == 'ReduceLROnPlateau':
#                self.policy = _ReduceLROnPlateau
#            else:
#                self.policy = getattr(lr_scheduler, policy)
#        else:
#            assert issubclass(policy, (_LRScheduler, _ReduceLROnPlateau))
#            self.policy = policy
#        self.policy_kwargs = policy_kwargs if policy_kwargs is not None else dict()
#
#    def initialize(self):
#        self._lr_scheduler = None
#
#    def on_train_begin(self, net, **kwargs):
#        self._lr_scheduler = self._get_scheduler(
#            net, self.policy, **self.policy_kwargs
#        )
#
#    def on_epoch_begin(self, net, **kwargs):
#        epoch = len(net.history)-1
#        if isinstance(self._lr_scheduler, _ReduceLROnPlateau):
#            metrics = lr_scheduler.previous_epoch_train_loss_score(net) \
#                if epoch else np.inf
#            self._lr_scheduler.step(metrics, epoch)
#        else:
#            self._lr_scheduler.step(epoch)
#
#    def _get_scheduler(self, net, policy, **scheduler_kwargs):
#        return policy(net.optimizer_, **scheduler_kwargs)

class LRScheduler(Callback):
    """Callback that sets the learning rate of each
    parameter group according to some policy.

    Parameters
    ----------

    policy : str or _LRScheduler class (default='WarmRestartLR')
      Learning rate policy name or scheduler to be used.

    monitor : str or callable (default=None)
      Value of the history to monitor or function/callable. In
      the latter case, the callable receives the net instance as
      argument and is expected to return the score (float) used to
      determine the learning rate adjustment.

    kwargs
      Additional arguments passed to the lr scheduler.

    """

    def __init__(self, policy='WarmRestartLR', monitor='train_loss', kwargs={}):
        self.policy = policy
        self.monitor = monitor
        self.kwargs = kwargs

    def simulate(self, steps, initial_lr):
        """
        Simulates the learning rate scheduler.

        Parameters
        ----------
        steps: int
          Number of steps to simulate

        initial_lr: float
          Initial learning rate

        Returns
        -------
        lrs: numpy ndarray
          Simulated learning rates

        """
        test = torch.ones(1, requires_grad=True)
        opt = torch.optim.SGD([{'params': test, 'lr': initial_lr}])
        sch = self.policy(opt, **self.kwargs)

        lrs = []
        has_batch_step = (hasattr(sch, 'batch_step')
                          and callable(sch.batch_step))
        for _ in range(steps):
            sch.batch_step() if has_batch_step else sch.step()
            lrs.append(sch.get_lr()[0])

        return np.array(lrs)

    def initialize(self):
        if isinstance(self.policy, str):
            self.policy_ = getattr(sys.modules[__name__], self.policy)
        else:
            self.policy_ = self.policy
        self.lr_scheduler_ = None
        return self

    def on_train_begin(self, net, **kwargs):
        self.lr_scheduler_ = self._get_scheduler(
            net, self.policy_, **self.kwargs
        )

    def on_epoch_begin(self, net, **kwargs):
        epoch = len(net.history) - 1
        if isinstance(self.lr_scheduler_, ReduceLROnPlateau):
            if callable(self.monitor):
                score = self.monitor(net)
            else:
                score = net.history[-2, self.monitor] if epoch else np.inf
            self.lr_scheduler_.step(score, epoch)
        else:
            self.lr_scheduler_.step(epoch)

    def on_batch_begin(self, net, **kwargs):
        if (
                hasattr(self.lr_scheduler_, 'batch_step') and
                callable(self.lr_scheduler_.batch_step)
        ):
            batch_idx = self._get_batch_idx(net)
            self.lr_scheduler_.batch_step(batch_idx)

    def _get_scheduler(self, net, policy, **scheduler_kwargs):
        """Return scheduler, based on indicated policy, with appropriate
        parameters.
        """
        if policy not in [lr_scheduler.CyclicLR, ReduceLROnPlateau] and \
           'last_epoch' not in scheduler_kwargs:
            last_epoch = len(net.history) - 1
            scheduler_kwargs['last_epoch'] = last_epoch

        if policy is lr_scheduler.CyclicLR and \
           'last_batch_idx' not in scheduler_kwargs:
            last_batch_idx = self._get_batch_idx(net)
            scheduler_kwargs['last_batch_idx'] = last_batch_idx
        return policy(net.optimizer_, **scheduler_kwargs)

    def _get_batch_idx(self, net):
        if not net.history:
            return -1
        epoch = len(net.history) - 1
        current_batch_idx = len(net.history[-1, 'batches']) - 1
        batch_cnt = len(net.history[-2, 'batches']) if epoch >= 1 else 0
        return epoch * batch_cnt + current_batch_idx
