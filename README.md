

# Dependencies
* numpy
* matplotlib
* scipy
* sklearn
* skimage
* tensorflow==1.1
* sacred==v0.7b4
* pycoco (if using MS COCO database, https://github.com/pdollar/coco/tree/master/PythonAPI)

If using GPU:
* GPU enabled tensorflow
* Nvidia drivers
* CUDA dev toolkit
* cuDNN

# How to use

> `python3 experiment.py with mode=[test,train,evaluate,train_and_eval]`

To see available params do:

> `python3 experiment.py print_config`

Parameters can be changed using `with param1=val1 param2.subparam1=val2`. For instance, to change the dataset, do:

> `python3 experiment.py with data=stanford`

Experiment is configured by default to log everything to a MongoDB at `./sacred/` directory. The DB server has to be started with:

> `mongod --dbpath sacred`

If you do not want to log this run, add the `-u` flag to your command line call.



