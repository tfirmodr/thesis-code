#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct 30 11:47:42 2017

@author: thalita

Experiment for few shot learning on omniglot

assumes metalearning through episodes

"""

import tensorflow as tf
import sacred
from sacred import Experiment
import models
from models.few_shot_prototypes import FewShotPrototypes
import datasets
from sacred.observers import FileStorageObserver
import os
import shutil

ex = Experiment('few_shot_classification')
tf.logging.set_verbosity(tf.logging.INFO)

@ex.config
def base():
    seed = 245921591
    mode = 'train_and_evaluate'
    output = 'results'
    resume = True  # wether to resume previous training session

    augment = False
    num_epochs = int(1e3)
    num_episodes = 8000
    val_episodes = 100
    test_episodes = 100
    classes_per_set = 20
    targets_per_class = 5
    samples_per_class = 5 # K for K shot


    model = 'fewshotprototypes'  # model name
    model_params = models.get_by_name(model).default_params
    model_params.update(decay_steps=1000*num_epochs)
    # Estimator's run params
    # Allowed properties are ['model_dir', 'tf_random_seed',
    # 'save_summary_steps', 'save_checkpoints_steps', 'save_checkpoints_secs',
    # 'session_config', 'keep_checkpoint_max', 'keep_checkpoint_every_n_hours',
    # 'log_step_count_steps']
    run_params = dict(
        save_summary_steps = 100,
        save_checkpoints_secs = 600,
        save_checkpoints_steps = None,
        session_config = None,
        keep_checkpoint_max = 3,
        keep_checkpoint_every_n_hours = 10000,
        log_step_count_steps = 100)

    exp_params = dict(
        eval_delay_secs=120,  # delay in sec. untill 1st evaluation
        continuous_eval_throttle_secs=60,
        min_eval_frequency=1,
        delay_workers_by_global_step=False)


@ex.named_config
def simple_prototypes():
    num_epochs = 1
    model = 'fewshotprototypes'  # model name
    model_params = FewShotPrototypes.default_params
    model_params['learning_rate'] = 1e-4
    model_params['n_proto'] = 0
    model_params.update(decay_steps=1000*num_epochs)


@ex.named_config
def baseline():
    num_epochs = 1000
    eval_epochs = 1000
    num_episodes = 50
    eval_episodes = 100
    model = 'fewshotcnn'
    model_params = models.EpisodicalCNN.default_params
    model_params['learning_rate'] = 1e-4
    model_params.update(decay_steps=0.0)
    #model_params['weights'] = 'omniglot'


@ex.capture
def inner_loop(estimator, data, split,
               num_epochs,
               num_episodes, val_episodes, test_episodes,
               classes_per_set, samples_per_class,
               augment,
               run_params):
    # inner loop is training/evaluating over one episode
    batch_size = classes_per_set * samples_per_class
    # get an episode
    features, labels = data.get_batch(split, augment)
    # run inner loop for num_epochs
    hooks = []
    if split == 'train':
        hooks.append(tf.train.StopAtStepHook(last_step=num_epochs*num_episodes))
        input_fn = tf.estimator.inputs.numpy_input_fn(
                features, labels,
                batch_size, num_epochs, shuffle=True)
        estimator.metatrain_step(input_fn, hooks=hooks)
        # evaluate on target set
        input_fn = tf.estimator.inputs.numpy_input_fn(
                features, labels,
                batch_size, num_epochs=1, shuffle=False)
        estimator.evaluate(input_fn)

    elif split == 'val' or spit == 'test':
        estimator.model_fn.meta_mode = tf.estimator.ModeKeys.EVAL
        hooks.append(tf.train.StopAtStepHook(num_steps=num_epochs))
        # train on support set
        input_fn = tf.estimator.inputs.numpy_input_fn(
                features, labels,
                batch_size,num_epochs, shuffle=True)
        estimator.train(input_fn=input_fn, hooks=hooks)
        # evaluate on target set
        input_fn = tf.estimator.inputs.numpy_input_fn(
                features, labels,
                batch_size, num_epochs=1, shuffle=False)
        estimator.evaluate(input_fn)


@ex.capture
def single_run(split,
               seed,
               model, model_params,
               run_params,
               num_epochs,
               num_episodes,
               eval_episodes,
               classes_per_set,
               samples_per_class,
               targets_per_class):

    # each batch here is an episode, will get one at a time
    data = datasets.OmniglotNShotDataset(
            1, classes_per_set,
            samples_per_class,
            targets_per_class,
            seed,
            shuffle_classes=True)
    run_params['model_dir'] += '/' + split
    config = tf.estimator.RunConfig().replace(
            tf_random_seed=seed, **run_params)
    model_params.update(n_classes=classes_per_set)
    estimator = models.get_by_name(model)(params=model_params,
                                  config=config)

    while 1:
        inner_loop(estimator, data, split)


@ex.capture
def evaluation_common(split, run_params, seed,
                      eval_epochs, eval_episodes):
    train_dir = os.path.join(run_params['model_dir'], 'train')
    eval_dir = os.path.join(run_params['model_dir'], split, '_%d')
    # copy all train files but the tfevents log
    files_to_copy = filter(lambda s: s.find('tfevents') < 0,
                           os.listdir(train_dir))
    for i in range(eval_episodes):
        eval_dir_i = eval_dir % (i)
        map(lambda s: shutil.copy(os.path.join(train_dir, s),
                                  eval_dir_i),
            files_to_copy)

        single_run(eval_dir_i.split('/')[-1])


@ex.command
def validation(run_params):
    evaluation_common('val')


@ex.command
def test():
    evaluation_common('test')


@ex.config_hook
def setup_observer(config, command_name, logger):
    if 'model_dir'in config['run_params']:
        model_dir = config['run_params']['model_dir']
        if model_dir is not None:
            ex.observers.append(FileStorageObserver.create(model_dir+'/sacred'))
    return {}

@ex.automain
def main():
    single_run('train')
