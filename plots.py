import tensorflow
import numpy as np
import os
import matplotlib.pyplot as plt
from tensorflow.tensorboard.backend.event_processing import event_accumulator

def get_data_plot(name_func, path):
    for file in os.listdir(path) :
        if file.find('tfevents') > -1 :
            path = path + '/' + file         
            #example of path is 'example_results/events.out.tfevents.1496749144.L-E7-thalita'
            ea = event_accumulator.EventAccumulator(path, 
                                                size_guidance={ # see below regarding this argument
                                                event_accumulator.COMPRESSED_HISTOGRAMS: 500,
                                                event_accumulator.IMAGES: 4,
                                                event_accumulator.AUDIO: 4,
                                                event_accumulator.SCALARS: 0,
                                                event_accumulator.HISTOGRAMS: 1,
                                                 })
            ea.Reload() # loads events from file 
            for function in ea.Tags()['scalars'] :
                if function.find(name_func) > -1 : #to find an approximate name_func 
                    values=[] #empty list
                    steps=[] #empty list
                    for element in (ea.Scalars(function)) :
                        values.append(element.value) #it is a named_tuple, element['value'] is wrong 
                        steps.append(element.step)  

                    return np.array(steps), np.array(values)
                    
                    
def plot_train_eval (name_func, paths) :
    if not isinstance(paths, list) :
        paths = [paths]
    plt.figure()
    for path in paths :
        x_train, y_train = get_data_plot(name_func, path)
        x_eval, y_eval = get_data_plot(name_func, path + '/eval/')
        plt.plot(x_train,y_train, 'rv--', label='train')
        plt.plot(x_eval, y_eval, '--', label='eval')
    plt.legend(loc='lower right')
    plt.xlabel('x_train, x_eval')
    plt.ylabel('y_train, y_eval')
    plt.title(name_func+' function train and eval')
    plt.show()
    plt.savefig(name_func+'train_eval.png')




