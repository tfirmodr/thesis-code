
# coding: utf-8


import numpy as np
import matplotlib.pyplot as plt
import os
from tqdm import tqdm

from sklearn.model_selection import RepeatedStratifiedKFold, StratifiedShuffleSplit


import pandas as pd
import itertools
from sklearn.decomposition import PCA
from sklearn.manifold import TSNE, Isomap
from itertools import combinations
from scipy.stats import ttest_ind, wilcoxon
from scipy.stats import friedmanchisquare
from stac import friedman_test

from keras.datasets import mnist

from prototype_utils import StratifiedShufflePredefinedTestSplit
from prototype_models import (classify_cluster_softmax, classify_prototypical,
                              classify_softmax)
from prototype_plots import plot_montage



random_state=10
N=10


def gen_mnist(N=10, plot=False, random_seed=None, random_state=None):
    if random_seed is not None:
        np.random.seed(random_seed)
    elif random_state is not None:
        np.random.set_state(random_state)
    (x_train, y_train), (x_test, y_test) = mnist.load_data()
    X_imgs = np.concatenate([x_train, x_test])
    y = np.concatenate([y_train, y_test]).squeeze()
    X = np.load('./Data/mnist_inceptionV3_features.npy')

    if plot:
        plot_montage(X_imgs, cmap='Greys')
    return X_imgs, X, y


def spliters(random_state, train_size=100, n_splits=1, cv_folds=2, cv_repeats=1):
    global skf, skf_cv
    test_fold = np.array(60000*[-1]+10000*[0])
    skf = StratifiedShufflePredefinedTestSplit(test_fold, train_size,
                                               n_splits, random_state)
    skf_cv = RepeatedStratifiedKFold(n_splits=cv_folds, n_repeats=cv_repeats,
                                     random_state=None)
    return skf, skf_cv





