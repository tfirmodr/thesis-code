#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Dec 11 17:33:59 2018

@author: thalita

Subclassing of test_tubes SlurmCluster, with some methods reimplemented
because I needed to change the calls to sbatch so multiple trials are done
becaus way too often on plafrim you reach user queue limits and queuing new jobs
is simply denied.
For now using a simple work around based on wait a random amount of time then
retying the submission.
"""

from test_tube.hpc import AbstractCluster
from test_tube import HyperOptArgumentParser, SlurmCluster, Experiment
import datetime
import time
import re
import os
from random import randint
from shutil import copyfile
from subprocess import call
from types import SimpleNamespace

ExperimentStatus = SimpleNamespace(
    NONE='not started',
    SUBMITTED='queued',
    RUNNING='running',
    INTERRUPTED='interrupted/waiting to restart',
    RESUBMITTED='requeued',
    COMPLETED='completed'
)

# decorator for your training function
def main_fn(train_fn):
    def wrapped_train_fn(hparams, cluster, *args):
        """Train your awesome model.

        :param hparams: The arguments to run the model with.
            
        In this fuction you should
        - initialise an instance of test_tube.Experiment
        - set save and load functions on the cluster object to allow for continuation jobs
        - call your training function or write your training loop
        """

        # The experiment class is responsible for logging info about this particular run
        # including hyperparameters used
        # Name and experiment version number are assigned by the cluster object
        # that spanned this process, using params in hparams
        # Initialize experiments and track all the hyperparameters
        exp = Experiment(
            name=hparams.test_tube_exp_name,
            # Location to save the metrics.
            save_dir=hparams.log_path,
            # do not enable autosave when running on cluster,
            # risk of race condition on the log files
            autosave=False,
            version=hparams.hpc_exp_number,
        )
        exp.argparse(hparams)
        exp.tag({'status': ExperimentStatus.RUNNING})
        exp.save()

        # We need to define save an load functions in case our job
        # is not finished before the allocated time is over
        # The save function is called some minutes before the job would be killed
        # The current job prepares a new sbatch script with the same parameters then dies.
        # The main proces will call this script later and the new spanned job
        # will call the load function as soon as it is set (see bellow)
        # Even if your training code handles checkpointing elsewhere
        # (be sure it is done safely regarding race conditions on the checkpoint files),
        # these functions still need to be declared and set on the cluster object
        # For the custom PlafrimCluster class, it is important to tag experiment status
        # and save experiment info
        def save_fn(**kwargs):
            exp.tag({'status':ExperimentStatus.INTERRUPTED})
            exp.save()
        def load_fn(**kwargs):
            print("trial %d - experiment loaded" % hparams.hpc_exp_number)
        # these functions need to take keyword args,
        # that have to be informed to the setter functions as a dict
        # in this case, since I have no kwargs, I pass an empty dict
        cluster.set_checkpoint_save_function(save_fn, {})
        # setting the load function on a continuation job automatically calls it
        cluster.set_checkpoint_load_function(load_fn, {})
        # call main train_fn
        train_fn(hparams, exp, cluster)
        # mark completed and save experiment log when finished
        # this tag is needed for the custom PlafrimCluster class
        # so that it knows the experiment is done
        exp.tag({'status':ExperimentStatus.COMPLETED})
        exp.save()
        print('done!')

    return wrapped_train_fn

class PlafrimCluster(SlurmCluster):
    def __init__(self, queue=None,
                 retry_delay_min=1,
                 retry_delay_max=10,
                 *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.retry_delay_min = retry_delay_min
        self.retry_delay_max = retry_delay_max

        if queue is not None:
            self.add_slurm_cmd(
                cmd='partition',
                value=queue,
                comment='cluster partition (queue)')
        self.scheduled_exps = set([])
        self.submitted_scripts = {}
        self.completed_exp_i = {}
                    
    def _SlurmCluster__optimize_parallel_cluster_internal(self, *args, **kwargs):
        if self.call_load_checkpoint:
            print('Im a restarted experiment')
        super()._SlurmCluster__optimize_parallel_cluster_internal(*args,*kwargs)
        if not self.is_from_slurm_object:
            try:
                self._wait_completed()
            except KeyboardInterrupt:
                for script, submitted in self.submitted_scripts.items():
                    if not submitted:
                        os.remove(script)

    def _wait_completed(self):
        """Wait until all created scripts were submitted and corresponding
        experiments are completed"""
        # jobs needing resubmission will generate a new script in slurm_scripts
        # which is watched for new scripts in this loop
        done = False
        while not done:
            current_scripts = [os.path.join(self.slurm_files_log_path, s)
                for s in os.listdir(self.slurm_files_log_path)]
            current_scripts.sort()
            for script in current_scripts:
                # get correponding experiment id
                exp_i = self._get_exp_id(script)
                # only continue if exp_id was scheduled by the current process
                if exp_i in self.scheduled_exps:
                    self._update_script_status(script, exp_i)
            # after looping over all existing scripts, see if
            # all registered scripts are submitted and all exps completed
            print('exp status:', self.completed_exp_i)
            submitted = [stat for stat in self.submitted_scripts.values()]
            completed = [(stat == 'completed')
                         for stat in self.completed_exp_i.values()]
            done = all(submitted) & all(completed)
            if not done:
                # wait a while before checking everything again
                max_delay = int(min(self.retry_delay_max*60,
                                self.slurm_time_to_seconds(self.job_time)))
                min_delay = int(min(self.retry_delay_min*60, max_delay))
                t = randint(min_delay, max_delay)
                print('new checkup in %d min...' % (t//60))
                time.sleep(t)
            else:
                print('All experiments completed.')

    def _update_script_status(self, script, exp_i):
        # update exp status 
        status = self._check_status(exp_i)
        self.completed_exp_i[exp_i] = status
        # check if exp is completed
        if status != ExperimentStatus.COMPLETED:
            # register script if not registered
            if script not in self.submitted_scripts:
                self.submitted_scripts[script] = False
            # try to submit if not submitted
            if not self.submitted_scripts[script]:
                submitted = self._submit_sbatch(script)
                # update submission status
                self.submitted_scripts[script] = submitted
                if submitted and status == ExperimentStatus.NONE:
                    self.completed_exp_i[exp_i] = ExperimentStatus.SUBMITTED
                elif submitted and status == ExperimentStatus.INTERRUPTED:
                    self.completed_exp_i[exp_i] = ExperimentStatus.RESUBMITTED
                
    def schedule_experiment(self, trial_params, exp_i):
        """Reimplmenting some methods to avoid the queue submission limits"""
        timestamp = datetime.datetime.now().strftime("%Y-%m-%d__%H-%M-%S")
        timestamp = 'trial_{}_{}'.format(exp_i, timestamp)

        # generate command
        slurm_cmd_script_path = os.path.join(self.slurm_files_log_path, '{}_slurm_cmd.sh'.format(timestamp))
        slurm_cmd = self._SlurmCluster__build_slurm_command(trial_params, slurm_cmd_script_path, timestamp, exp_i, self.on_gpu)
        self._SlurmCluster__save_slurm_cmd(slurm_cmd, slurm_cmd_script_path)

        # run script to launch job
        # thalita: here is my change
        # job will acctualy be submitted in another function
        print('Wrote script', slurm_cmd_script_path)
        self.scheduled_exps.add(exp_i)
        # start tracking experiment status
        self.completed_exp_i[exp_i] = ''

    def _submit_sbatch(self, slurm_cmd_script_path):
        # thalita: here is my change
        exp_id = self._get_exp_id(slurm_cmd_script_path)
        print('\nlaunching exp %d...' % exp_id)
        result = call('{} {}'.format(AbstractCluster.RUN_CMD, slurm_cmd_script_path), shell=True)
        if result == 0:
            print('launched exp ', slurm_cmd_script_path)
            return True
        else:
            print('launch failed...')
            return False

    def _get_exp_id(self, slurm_cmd_script_path):
        # get corresponding experiment id (version)
        script = slurm_cmd_script_path
        fname = script.split('/')[-1]
        exp_i = re.match('trial_([0-9]+)', fname)[1]
        return int(exp_i)

    def _check_status(self, exp_i):
        """
        Assumes a tag 'status': 'completed' will be written by the experiment
        when it ends succesfully.
        Checks for the tag if tag file is already there,
        otherwise returns false
        """
        # load tags file if exists and check status
        exp_tags_file = os.path.join(self.log_path, 'version_%d' % exp_i,
                                     'meta_tags.csv')

        if not os.path.exists(exp_tags_file):
            return ExperimentStatus.NONE

        with open(exp_tags_file, 'r') as f:
            for line in f.readlines():
                line = line.strip('\n')
                try:
                    key, value = line.split(',')
                    if key == 'status':
                        return value
                except ValueError:
                    return ExperimentStatus.NONE
       
    def _SlurmCluster__call_old_slurm_cmd(self, original_slurm_cmd_script_path, exp_i, copy_current=True):
        """
        Reimplmenting some methods to avoid the queue submission limits
        ---------------------------------------------------------------
        Copies old slurm script into a new one and adds a load flag in case it wasn't there.
        Then schedules the script again, but this time with the load flag which will signal the program
        to load the model so it can continue training.

        :param original_slurm_cmd_script_path:
        :param exp_i:
        :param copy_current:
        :return:
        """

        # generate command
        script_path = original_slurm_cmd_script_path.split('slurm_scripts')[0] + 'slurm_scripts'
        timestamp = datetime.datetime.now().strftime("%Y-%m-%d__%H-%M-%S")
        timestamp = 'trial_{}_{}'.format(exp_i, timestamp)
        new_slurm_cmd_script_path = os.path.join(script_path, '{}_slurm_cmd.sh'.format(timestamp))

        # add continue flag if not there
        with open(original_slurm_cmd_script_path) as old_file:
            lines = old_file.read().split('\n')
        last_line = lines[-1]
        lines = [line + '\n' for line in lines]
        lines[-1] = last_line
        if not HyperOptArgumentParser.SLURM_LOAD_CMD in lines[-1]:
            last_line = lines[-1]
            last_line = '{} --{}\n'.format(last_line, HyperOptArgumentParser.SLURM_LOAD_CMD)
            lines[-1] = last_line
            with open(new_slurm_cmd_script_path, 'w') as f:
                f.writelines(lines)
        else:
            # copy with new time
            copyfile(original_slurm_cmd_script_path, new_slurm_cmd_script_path)
            

        # run script to launch job
        # thalita: here is my change
        # job is resubmitted in main process
        print('Wrote script', new_slurm_cmd_script_path)
