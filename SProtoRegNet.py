#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Sep 21 16:15:47 2018

@author: thalita
"""


import numpy as np
from sklearn.neighbors import NearestNeighbors
from sklearn.base import TransformerMixin, BaseEstimator, ClassifierMixin
from sklearn.cluster.spectral import discretize
from sklearn.cluster import KMeans
from scipy.stats import mode
from scipy import sparse
from tqdm import tqdm
from skorch_utils import TransformerNet, StopperNet, NNClassifier, SaveWeights
from skorch.utils import to_tensor
from skorch.callbacks import Callback
import torch


from laplacian_utils import compute_W, compute_L, smalestSVD, gap


class SProtoRegNet(StopperNet, NNClassifier):
    __doc__ = """ NN with regularization based on prototypes from spectral clustering. """

    def __init__(self, module, layer_name,
                 reg=0.01,
                 n_neighbors=20, nn_radius=10, n_components=50,
                 n_clusters='gap', cluster_assigmnent='discretize',
                 split_proto_threshold=0.4,
                 tol=1e-4, max_iter=200, n_jobs=1,
                 concatenate_input=True,
                 epochs_proto_update=2,
                 max_epochs=200,
                 **kwargs):
        super().__init__(module=module, max_epochs=max_epochs, **kwargs)
        self.layer_name = layer_name
        self.concatenate_input = concatenate_input
        self.max_epochs = max_epochs
        self.epochs_proto_update = epochs_proto_update
        self.reg = reg
        self.split_proto_threshold = split_proto_threshold
        self.n_clusters = n_clusters
        self.n_jobs = n_jobs
        self.tol = tol
        self.max_iter = max_iter
        self.n_neighbors = n_neighbors
        self.nn_radius = nn_radius
        self.n_components = n_components
        self.cluster_assignment = cluster_assigmnent

    def _update_protos(self, X, y, ksi, X_imgs=None):
        if self.concatenate_input:
            input = X if X_imgs is None else X_imgs
            p = lambda ksi: np.concatenate([ksi, input], axis=-1)
        else:
            p = lambda ksi: ksi
        # Reuse previous protos if this is not the first update
        # valid only if using k-means
        if self.protos is None:
            kmeans_init = 'k-means++'
            kmeans_n_init = 10
        else:
            kmeans_init = self.protos
            kmeans_n_init = 1
        # Compute neighbohhood graph and Laplacian
        W = compute_W(p(ksi), self.n_neighbors, self.nn_radius)
        Ln, dd = compute_L(W, normalized=True, return_diag=True)
        # Smalest eigen vectors of Laplacian matrix
        svdLn = smalestSVD(Ln, k=self.n_components)
        # XXX gap sometimes gives a very large number of clusters
        #     which is not intersting and causes problems
        if self.n_clusters == 'gap':
            n_clusters = gap(svdLn.singular_values_)
        else:
            n_clusters = self.n_clusters
        # use first n_proto components for clustering
        embedding = svdLn.singular_vectors_[:, :n_clusters]
        if self.cluster_assignment == 'discretize':
            clusters = discretize(embedding, copy=False)
        elif self.cluster_assignment == 'kmeans':
            kmeans = KMeans(n_clusters, init=kmeans_init, n_init=kmeans_n_init)
            kmeans.fit(embedding)
            clusters = kmeans.labels_

        # find center in the embedding space, take closest sample as prototype
        protos = []
        protos_class = []
        for i in range(n_clusters):
            cluster_mask = (clusters == i)
            # get cluster classes
            y_masked = y[cluster_mask]
            cluster_classes, class_count = mode(y_masked)
            class_freq = class_count/class_count.max()
            cluster_classes = cluster_classes[class_freq >= self.split_proto_threshold]
            for subclass in cluster_classes:
                protos_class.append(subclass)
                class_mask = y_masked == subclass
                # get embedding points from this cluster and from the right class
                emb_masked = embedding[cluster_mask][class_mask]
                if emb_masked.shape[0] > 0:
                    if self.cluster_assignment == 'discretize':
                        emb_center = emb_masked.mean(axis=0)
                    elif self.cluster_assignment == 'kmeans':
                        emb_center = kmeans.cluster_centers_[i]
                    emb_dist = np.linalg.norm(emb_masked-emb_center, axis=1)
                # take closest sample of the most voted class as protoype
                center_id = np.argmin(emb_dist)
                center = ksi[cluster_mask][class_mask][center_id]
                protos.append(center)

        protos = np.array(protos)
        protos_class = np.array(protos_class)

        self.protos = protos
        self.protos_class = protos_class


    def initialize_module(self):
        super().initialize_module()
        self.protos = None
        self.protos_class = None

    def fit(self, X, y, X_imgs=None):
        if not self.warm_start or not self.initialized_:
            self.initialize()
        ksi = self.transform(X)

        self.ksi = ksi
        for i in range(0, self.max_epochs, self.epochs_proto_update):
            # proto update
            self._update_protos(X, y, self.ksi, X_imgs=X_imgs)
            self.protos = to_tensor(self.protos.astype(X.dtype),
                                    device=self.device)
            self.protos_class = to_tensor(self.protos_class.astype(y.dtype),
                                         device=self.device)

            # estimator update
            # pass X as dict for batch slicing of all elements
            self.partial_fit(X=dict(X=X),
                             y=y, epochs=self.epochs_proto_update)
            if self.stop:
                break
            self.ksi = self.transform(X)
        return self

    def transform(self, X):
        return super().transform(X, name=self.layer_name)

    def get_loss(self, y_pred, y_true, X=None, training=False):
        if isinstance(X, dict):
            X_X = X['X']
        else:
            X_X = X
        loss = super().get_loss(y_pred, y_true, X_X, training)
        if X is not None and self.protos is not None:
            n_classes = self.module_.output.weight.size(0)
            ksi = self.infer(X_X, name=self.layer_name)
            n_protos, n_features = self.protos.size()
            n_samples, _ = ksi.size()

            # create similarity tensor
            sim = torch.empty(n_samples, n_protos)
            for ix, p in enumerate(self.protos):
                sim[:, ix] = -torch.sum((ksi-p)**2, dim=1)

            sim = torch.nn.functional.softmax(sim, dim=1)
            proto2class = torch.zeros((n_protos, n_classes),
                                      device=self.device)
            proto2class.scatter_(1, self.protos_class.view(-1,1), 1)
            proto_class_pred = torch.mm(sim, proto2class)
            reg_loss = torch.nn.CrossEntropyLoss()
            reg_loss = reg_loss(proto_class_pred, y_true)

#            closest_proto = torch.argmax(sim, dim=1)
#
#            # check if closest proto class is the good class
#            closest_proto_class = self.proto_class[closest_proto]
#            index = torch.wher
#            right_class_proto = torch.gather(self.protos, 1, index)
#            bad_proto_indicator = (closest_proto_class != y_true).float()
#
#            sim[bad_class_indicator]
#
#            sim_other_centers = torch.sum(sim, dim=1) - max_sim
#
#            reg_loss = torch.mean(max_sim/sim_other_centers)

#            reg_loss = reg_loss * self.reg
#            loss += reg_loss
            loss = reg_loss
        return loss


class SaveVars(Callback):
    """ Callback to save ksi, alpha and Z"""
    def __init__(self, every_n_epochs=100):
        self.every_n_epochs = every_n_epochs

    def initialize(self):
        self.ksi = []
        self.protos = []
        self.protos_class = []
        self.last_save = 0
        self.epochs = []
        return self

    def on_train_end(self, net, **kwargs):
        """
        Method notified after partial fit, when it is interesting to save
        ldmnet vars.
        """
        epochs = len(net.history)
        if epochs - self.last_save >= self.every_n_epochs:
            self.ksi.append(net.ksi)
            self.protos.append(net.protos.cpu().numpy())
            self.protos_class.append(net.protos_class.cpu().numpy())
            self.last_save = epochs
            self.epochs.append(epochs - 1)
