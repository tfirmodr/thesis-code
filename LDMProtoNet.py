#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue 06-11-2018 12:04

@author: thalita

Module implementing low dim manifold regularization with spectral prototypes
"""
import torch
from torch import nn
import numpy as np
from skorch.callbacks import Callback
from skorch.utils import to_tensor
from SProtoRegCallback import ProtoUpdateBase
from LDMnetCallback import AlphaUpdate, LDMnetBase

from laplacian_utils import compute_W, compute_L


class LDMProtoNet(LDMnetBase):
    def __init__(self,
                 module,
                 layer_name,
                 reg=0.001,
                 mu=0.1,
                 epochs_update=2,
                 lambda_bar=0.01,
                 criterion=torch.nn.CrossEntropyLoss,
                 **kwargs):
        super().__init__(
                module, layer_name=layer_name,
                criterion=criterion,
                mu=mu, lambda_bar=lambda_bar,
                epochs_update=epochs_update,
                **kwargs)
        self.reg = reg

    def initialize_callbacks(self):
        self.callbacks.append(ManifoldUpdate(layer_name=self.layer_name,
                                             lambda_bar=self.lambda_bar,
                                             mu=self.mu,
                                             epochs_update=self.epochs_update))
        super().initialize_callbacks()

    def initialize(self):
        super().initialize()
        self.protos = None
        self.protos_class = None
        return self

    def get_loss(self, y_pred, y_true, X=None, training=False):
        if isinstance(X, dict):
            X_X = X['X']
        else:
            X_X = X
        loss = super().get_loss(y_pred, y_true, X_X, training)
        if not training:
            return loss
        if X is not None:
            # ksi needs to be infered here so that gradients are computed
            ksi = self.infer(X, name=self.layer_name)
            ldm_loss = 0
            reg_loss = 0
            if self.lambda_bar != 0:
                alpha = to_tensor(X['alpha'], device=self.device).to(X_X.dtype)
                Z = to_tensor(X['Z'], device=self.device).to(X_X.dtype)
                ldm_loss = self.mu * 0.5 * torch.norm(alpha - ksi + Z)
                ldm_loss = ldm_loss.mean(dim=0)
            if self.reg != 0:
                n_protos, n_features = self.protos.size()
                n_samples, _ = ksi.size()

                # create similarity tensor
                dist = torch.empty(n_samples, n_protos)
                for ix, p in enumerate(self.protos):
                    dist[:, ix] = torch.sum((ksi-p)**2, dim=1)

                # gather classes for protos
                # for each mini batch sample
                samples_proto_class = torch.empty(n_samples, n_protos)
                for sample in range(n_samples):
                    samples_proto_class[sample, :] = self.protos_class

                # only distances to wrong protos will enter the loss
                bad_proto_mask = samples_proto_class.to(
                    torch.long) != y_true.view(-1, 1)

                dist2bad = torch.mean(dist * bad_proto_mask.to(torch.float), dim=1)
                reg_loss = -(dist2bad).mean() * self.reg
            loss = max(0.0, (1-self.reg-self.lambda_bar)) * loss
            loss += reg_loss + ldm_loss

        return loss


class ManifoldUpdate(AlphaUpdate, ProtoUpdateBase):
    def __init__(self, layer_name='hidden1',
                 mu=0.01, lambda_bar=0.01,
                 split_proto_threshold=0.4,
                 epochs_update=2,
                 preconditionner=False,
                 n_neighbors=20, nn_radius=10, n_components=50,
                 n_clusters='gap', cluster_assigmnent='discretize',
                 svd_method='arpack',
                 gap_lim=0.01,
                 tol=1e-4, max_iter=200, n_jobs=1,
                 concatenate_input=True,
                 random_seed=None, **kwargs):
        super().__init__(
                concatenate_input = concatenate_input,
                mu = mu,
                lambda_bar = lambda_bar,
                n_neighbors = n_neighbors,
                n_jobs = n_jobs,
                tol = tol,
                max_iter = max_iter,
                preconditionner = preconditionner)
        self.svd_method = svd_method
        self.random_seed = random_seed
        self.epochs_update = epochs_update
        self.layer_name = layer_name
        self.concatenate_input = concatenate_input
        self.split_proto_threshold = split_proto_threshold
        self.n_clusters = n_clusters
        self.gap_lim = gap_lim
        self.nn_radius = nn_radius
        self.n_components = n_components
        self.cluster_assignment = cluster_assigmnent

    def initialize(self):
        super().initialize()
        self.emb_protos = None
        self.last_svd = None
        self.last_update_ = 0

    def on_epoch_begin(self, net, dataset_train, dataset_valid,
                       *args, **kwargs):
        epochs = len(net.history)
        if not (epochs-1) % self.epochs_update:
            if net.train_ is not None:
                assert np.all(net.train_ == dataset_train.indices)
            net.train_ = dataset_train.indices
            X, y = dataset_train[:]
            if 'X_imgs' in X:
                X_imgs = X['X_imgs']
            else:
                X_imgs = None
            X = X['X']
            if self.lambda_bar != 0 or net.reg != 0:
                # compute W, L and solve linsys to update alpha
                self._update_W_L(net.ksi[net.train_], X, X_imgs, y=None)
            if self.lambda_bar !=0:
                self._update_alpha(net)
            # if net.reg != 0:
            self._update_protos(net, net.ksi[net.train_], y, X)

    def _update_W_L(self, ksi, X, X_imgs=None, y=None):
        cat_ksi = self._cat_ksi(ksi, X, X_imgs=X_imgs)
        self.W_ = compute_W(cat_ksi,
                            n_neighbors=self.n_neighbors,
                            nn_radius=self.nn_radius, y=y)
        self.L_ = compute_L(self.W_, normalized=False)
        self.Ln_ = compute_L(self.W_, normalized=True)


class SaveVars(Callback):
    """ Callback to save ksi, alpha and Z"""

    def __init__(self, every_n_epochs=100):
        self.every_n_epochs = every_n_epochs

    def initialize(self):
        self.ksi = []
        self.Z = []
        self.alpha = []
        self.epochs = []
        return self

    def on_train_end(self, net, **kwargs):
        """
        Method notified after partial fit, when it is interesting to save
        ldmnet vars.
        """
        epochs = len(net.history)
        if not (epochs-1) % self.every_n_epochs:
            self.ksi.append(net.ksi.copy())
            self.Z.append(net.Z.copy())
            self.alpha.append(net.alpha.copy())
            self.epochs.append(epochs - 1)



def test_LDMProtoNet_fit():
    from skorch_utils import MLP, NaNStopping
    from skorch.callbacks import EarlyStopping
    from sklearn.datasets import make_moons
    import torch

    random_seed = 0
    X, y = make_moons(n_samples=500, noise=0.1, random_state=random_seed)
    X, y = X.astype(np.float32), y.astype(np.int64)
    callbacks = [EarlyStopping(monitor='valid_loss', patience=100),
                 NaNStopping()]
    torch.manual_seed(random_seed)
    net = LDMProtoNet(
            MLP,
            layer_name='hidden1',
            module__num_units=[2],
            module__n_out=2,
            module__drop_proba=0,
            module__nonlin=torch.tanh,
            criterion=torch.nn.CrossEntropyLoss,
            lr=2e-2,
            reg=0.1,
            lambda_bar=0.1,
            mu=0.001,
            max_epochs=20,
            callbacks=callbacks,
            callbacks__ManifoldUpdate__n_neighbors=50,
            callbacks__ManifoldUpdate__nn_radius=5,
            callbacks__ManifoldUpdate__random_seed=random_seed,
            callbacks__ManifoldUpdate__n_components=50,
            callbacks__ManifoldUpdate__cluster_assignment='discretize',
            callbacks__ManifoldUpdate__n_clusters='gap',
            callbacks__ManifoldUpdate__gap_lim=1.0,
            callbacks__ManifoldUpdate__split_proto_threshold=0.0,
            callbacks__ManifoldUpdate__max_iter=200,  # if using kmeans
            callbacks__ManifoldUpdate__concatenate_input=True,
            epochs_update=10,
            device='cpu')

    net.fit(X, y)
