#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar 16 10:42:47 2017

@author: thalita

Experiment launcher

Generates a grid of parameters accoding to what is passed in the command line
then calls multiple runs of experiment.py for each param set.

Ranges can be passed as begin:end:step, and will be expanded to a param list.
You can also specify a param list explicitly by usign commas.

./launcher.py with n_hidden=100:1000:100 model_params.pooling_type=MAX,AVG
 model_params.pooling_window=[[3,3]]
 model_params.pooling_strides=[[1,1], [2,2]]

"""
import os
from sacred.config.config_files import save_config_file
from multiprocessing_on_dill import Pool

from experiment import ex
from utils import expand_params


class GridLauncher(object):
    def __init__(self, exp):
        self.base_exp = exp
        self.param_grid, self.local = expand_params(self.base_exp)

    def run(self):
        if self.local:
            for i in range(len(self.param_grid)):
                self._local_run(i)
        else:
            for i in range(len(self.param_grid)):
                self._srun(i)

    def _srun(self, run_id):
        param_grid = self.param_grid
        path, cfg_file = self._pre_run(run_id)
        slurm_options = (' '.join([
            "-N 1 -c 2 --partition=court_sirocco --gres=gpu:1",
            "--time=3:00:00",
            "--output=%s" % path + "run.out",
            "--job-name=%s" % path.split('/')[1],
            "--mail-type=END,FAIL,TIME_LIMIT",
            "--mail-user={0}".format(os.environ["MAIL_USER"])
            ]))
        if "SLURM_OPT" in os.environ:
            slurm_options += " "
            slurm_options += os.environ["SLURM_OPT"]
        ex.logger.info("slurm options: %s", slurm_options)
        ex.logger.info("Submitting %s %s", cfg_file, str(param_grid[run_id]))
        os.system(" ".join([
            "srun", slurm_options,
            "experiment.py launcher with", cfg_file, "--print_config",
            "--mongo_db=devel11:27017:sacred",
            "&"]))

        return None

    def _local_run(self, run_id):
        path, cfg_file = self._pre_run(run_id)
        ex.run(command_name='launcher', named_configs=[cfg_file])

    def _pre_run(self, run_id):
        param_grid = self.param_grid
        path = ex.run('get_path', config_updates=param_grid[run_id],
                      options={'--unobserved': True}).info["model_path"]
        path += '/'
        cfg_file = path + 'config_upd_%d.json' % run_id
        save_config_file(param_grid[run_id], cfg_file)
        return path, cfg_file


if __name__ == "__main__":
    runner = GridLauncher(ex)
    runner.run()
