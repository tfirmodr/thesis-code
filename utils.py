#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Apr 24 21:05:36 2017

@author: thalita
"""
import sys
import numpy as np
import hashlib
import copy
import collections
from sklearn.model_selection import ParameterGrid
from collections import OrderedDict, Iterable
from sacred.arg_parser import get_config_updates, parse_args
from sacred.utils import convert_to_nested_dict, iterate_flattened


def expand_params(ex):
    '''
    From params read in command line, generate the grid of param sets
    Input : sacred.Experiment object
    Output:
    '''
    # Code from Experiment.run_commandline to parse command line args
    param_grid = None
    all_commands = ex.gather_commands()
    local = False
    for ix, elem in enumerate(sys.argv):
        if elem == '--local':
            sys.argv = sys.argv[0:ix] + sys.argv[ix+1:]
            local = True
            break
    options = parse_args(sys.argv,
                         description=ex.doc,
                         commands=OrderedDict(all_commands))
    if 'UPDATE' in options:
        # expand updates
        # updates is a tuple (config_updates, named_configs)
        # treating only config updates for now (thus index 0)
        updates = get_config_updates(options['UPDATE'])[0]
        print(updates)
        expand = {}
        for par, vals in iterate_flattened(updates):
            if isinstance(vals, str):
                if vals.find(':') >= 0:
                    try:
                        arange = np.fromstring(vals, dtype=np.int32, sep=':')
                    except ValueError:
                        arange = np.fromstring(vals, dtype=np.float32, sep=':')
                    beg, end, step = tuple(arange)
                    vals = np.arange(beg, end, step)
                elif (vals.find(',') >= 0  # comma separated
                      and vals.find('[') <= 0  # not list
                      and vals.find('{') <=0): # not dict
                    vals = vals.split(',')
                else:
                    vals = [vals]
            if not isinstance(vals, Iterable):
                vals = [vals]
            expand[par] = vals
        param_grid = [convert_to_nested_dict(cfg_upd)
                      for cfg_upd in ParameterGrid(expand)]
    return param_grid, local


def recursive_sort_dict(d):
    """
    Sort dict d recursively
    (dicts converted into lists of tuples when sorted).
    """
    d = copy.deepcopy(d)
    for k, v in d.items():
        if isinstance(v, collections.Mapping):
             d[k] = recursive_sort_dict(v)

    return sorted(d.items())


def recursive_get_name(d):
    d = copy.deepcopy(d)
    for k, v in d.items():
        if isinstance(v, collections.Mapping):
            d[k] = recursive_get_name(v)
        else:
            s = v.__repr__()
            if isinstance(v, list):
                 if len(v) ==1:
                     s = s[1:-1]
            d[k]=s
    config = sorted(d.items()).__repr__()
    if len(d) ==1:
        config = config[1:-1]
    config = config.replace("',", ".")\
                   .replace(", ", "_")\
                   .replace("(", "")\
                   .replace(")", "")\
                   .replace("'", "")\
                   .replace(" ", "")\
                   .replace('"','')\
                   .replace('_params','')\
                   .replace('optimizer',"optim")\
                   .replace("[", "")\
                   .replace("]","_")\
                   .replace("__","_")
    return config


def get_name(d, max_len=200):
    if d == {}:
        return 'default'
    name = recursive_get_name(d)
    if len(d) > 1:
        name = name[0:-1]
    return name[0:max_len]


def make_hash(o):
    """
    make hash from (nested) dict o
    """
    o = recursive_sort_dict(o)
    byterepr = o.__repr__().encode()
    return hashlib.md5(byterepr).hexdigest()
