import tensorflow as tf
import numpy as np
from slurm_manager import SlurmClusterManager

def main(_):
    slurm = SlurmClusterManager(starting_port=12000)

    cluster_spec, job, task_id = slurm.build_cluster_spec()
    tf.logging.set_verbosity('INFO')
    server  = tf.train.Server(cluster_spec,
                              job_name=job,
                              task_index=task_id)
    device_str = "job:{job}/task:{task_id}".format(job=job, task_id=task_id)

    if job == 'ps':
        print('i am ps')
        server.join()
    else:
        print('i am a worker')
        with tf.device(tf.train.replica_device_setter(worker_device=device_str,
                                                      cluster=cluster_spec)):
            c = tf.constant("I am"+device_str)
            tf.contrib.framework.get_or_create_global_step()
        with tf.train.MonitoredTrainingSession(master=server.target,
                                               is_chief=(task_id == 0),
                                               checkpoint_dir="/tmp/train_logs") as sess:
            print("created session", device_str, server.target)

tf.app.run(main)
