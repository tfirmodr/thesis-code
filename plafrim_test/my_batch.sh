#!/usr/bin/env bash
#Job name
#SBATCH -J TEST_Slurm
# Asking for x nodes
##SBATCH -N 2
# Ask for Sirocco nodes
#SBATCH --partition=court_sirocco
# Ask for x tasks
#SBATCH --ntasks=4
# Ask for a min of x cpus per task
#SBATCH -c 1
# Asking for minimum of xMB memory per CPU
#SBATCH --mem-per-cpu=100M
# Asking for x minutes max time
#SBATCH -t00:00:30
# Output results message
#SBATCH -o slurm.out
# Output error message
#SBATCH -e slurm.out
echo "=====my job informations ==== "echo "Node List: " $SLURM_NODELIST
echo "my jobID: " $SLURM_JOB_ID
echo "Partition: " $SLURM_JOB_PARTITION
echo "submit directory:" $SLURM_SUBMIT_DIR
echo "submit host:" $SLURM_SUBMIT_HOST
echo "nprocs: " $SLURM_NPROCS
echo "In the directory: `pwd`"
echo "As the user: `whoami`"
srun python3 launch.py
