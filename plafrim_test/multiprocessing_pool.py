#!/cm/shared/dev/apps/python/install/python/3.5.1/bin/python3

#SBATCH --output=log.out
#SBATCH -n 5 # 5 cores

# -*- coding: utf-8 -*-
"""
Created on Mon Mar 20 19:24:42 2017

@author: thalita

from example in http://stackoverflow.com/questions/39974874/using-pythons-multiprocessing-on-slurm
simple multiprocessing on slurm
launch with sbatch
multiprocessing only works within one node!
for multiple nodes, MPI is needed
"""


import sys
import os
import multiprocessing

# Necessary to add cwd to path when script run
# by SLURM (since it executes a copy)
sys.path.append(os.getcwd())

def hello():
    print("Hello World", os.environ['SLURM_PROCID'])

pool = multiprocessing.Pool()
jobs = []
for j in range(10):
    p = multiprocessing.Process(target = hello)
    jobs.append(p)
    p.start()