#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar 10 16:07:32 2017

@author: thalita
"""

import tensorflow as tf
from tensorflow.python.client import device_lib
import numpy as np
import os
from hostlist import expand_hostlist


task_index  = int( os.environ['SLURM_PROCID'] )
n_tasks     = int( os.environ['SLURM_NPROCS'] )
port        = 12355  # See range with scontrol show config | grep MpiParams
tf_hostlist = [ ("%s:%s" % (host,port)) for host in
                expand_hostlist( os.environ['SLURM_NODELIST']) ]

task_hostlist = dict(zip(range(n_tasks), tf_hostlist))

cluster = tf.train.ClusterSpec( {"your_taskname" : task_hostlist } )
server  = tf.train.Server( cluster.as_cluster_def(),
                           job_name   = "your_taskname",
                           task_index = task_index )
tf.logging.set_verbosity('INFO')
print(device_lib.list_local_devices())
for idx in range(n_tasks):
    with tf.device("/job:your_taskname/task:%d" % idx ):
        print('task ', idx, ' of ', n_tasks)
#        x = tf.constant(np.random.rand(50,100,150,3), dtype=tf.float32)
#        k = tf.constant(np.random.rand(7,7,3,5), dtype=tf.float32)
#        y = tf.nn.conv2d(x,k,padding='SAME',strides=[1,1,1,1])
#        tf.Session().run(y)

