#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Sep 14 11:46:09 2018

@author: thalita
"""

# %% Imports
from sklearn.model_selection import GridSearchCV, StratifiedKFold
from sklearn.base import clone
from sklearn.preprocessing import StandardScaler
from scipy.stats import uniform, reciprocal, randint
import pandas as pd
import numpy as np
import sacred
from sacred.observers import FileStorageObserver
import os
from torch.nn import functional as F
import torch
from prototype_data import get_dataset
from skorch_utils import MLP, NNClassifier, EarlyStopping, NaNStopping, LRScheduler


#%%
ex = sacred.Experiment()
ex.observers.append(FileStorageObserver.create('./SoftmaxVsMLPInception/'))

@ex.config
def cfg():
    seed = 662120624
    data_name = 'mnist'
    max_iter = 200
    max_epochs = 1000
    n_jobs = 3
    n_hparams = 50
    train_size = 1000
    early_stopping=100
    lr_init = 1e-4
    cuda=False

#%%
@ex.automain
def main(seed,
         data_name,
         max_iter,
         n_hparams,
         train_size,
         n_jobs,
         max_epochs,
         early_stopping,
         lr_init,
         cuda):
    # %% Data loading (fetures from inception)
    random_seed = seed
    np.random.seed(random_seed)
    torch.manual_seed(random_seed)
    if cuda:
        torch.cuda.manual_seed(random_seed)

    data = get_dataset(data_name, random_seed=random_seed)

    X_imgs, X, y = data.gen_data(plot=False)
    skf, _ = data.splitters(train_size=train_size)
    cv = StratifiedKFold(2, random_state=random_seed)
    train, test = list(skf.split(X,y))[0]
    # normalize features
    scaler = StandardScaler()
    X = scaler.fit(X[train]).transform(X)

    # %% instance classifiers and searches hparams

    ## Softmax classifier vs MLP

    model = NNClassifier(
            MLP,
            module__n_in=X.shape[1],
            module__n_out=10,
            module__drop_proba=0.0,
            module__num_units=0,
            module__nonlin=F.relu,
            optimizer=torch.optim.Adam,
            optimizer__lr=lr_init,
            optimizer__weight_decay=0.001,
            max_epochs=max_epochs,
            device= 'cuda' if cuda else 'cpu',
            callbacks=[EarlyStopping(early_stopping, 'valid_acc'),
                       NaNStopping(),
                       LRScheduler('ReduceLROnPlateau')])

    params =[{'optimizer__weight_decay': np.logspace(-5, 5, n_hparams),
              'module__num_units': [0]},
             {'optimizer__weight_decay': np.logspace(-5, 5, n_hparams),
              'module__nonlin': [F.relu, F.tanh, F.sigmoid],
              'module__num_units': [X.shape[1]]}]

    search = GridSearchCV(model, params,
                          cv=cv, n_jobs=n_jobs, verbose=1,
                          refit=True,
                          return_train_score=True)


    search.fit(X[train].astype(np.float32), y[train].astype(np.int64))


    results = pd.DataFrame(search.cv_results_)


    print("best acc:", search.best_score_, search.best_params_)




    #%% saving
    results = dict(results=results,
                   search=search)

    save_path = ex.observers[0].dir
    os.makedirs(save_path, exist_ok=True)
    pd.to_pickle(results, os.path.join(save_path, 'results.pkl'))

