#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May 29 18:21:05 2018


CNN training from keras tutorial:
Trains a simple convnet on the MNIST dataset.
Gets to 99.25% test accuracy after 12 epochs
(there is still a lot of margin for parameter tuning).
16 seconds per epoch on a GRID K520 GPU.
"""
import numpy as np
import keras
from keras import backend as K
from keras.preprocessing import image
from keras.datasets import mnist
from keras.models import Model, Sequential
from keras.layers import Dense, Lambda, Input
from keras.layers import Dropout, Flatten
from keras.layers import Conv2D, MaxPooling2D
import tensorflow as tf
import pandas as pd
import os

DIR = "./Results/mnist_keras"

batch_size = 50
epochs = 12
num_classes = 10
train_size = list(range(1,15)) + [15,20,30,40,50]
validation_split = 0.0

seed = 4753

# input image dimensions
img_rows, img_cols = 28, 28

def get_model(img_rows, img_cols):
    if K.image_data_format() == 'channels_first':
        input_shape = (1, img_rows, img_cols)
    else:
        input_shape = (img_rows, img_cols, 1)

    model = Sequential()
    model.add(Conv2D(32, kernel_size=(3, 3),
                     activation='relu',
                     input_shape=input_shape))
    model.add(Conv2D(64, (3, 3), activation='relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(0.25))
    model.add(Flatten())
    model.add(Dense(128, activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(num_classes, activation='softmax'))
    return model

if __name__ == '__main__':
    scores = []
    for t in train_size:
        np.random.seed(seed)
        tf.set_random_seed(seed)
        # the data, shuffled and split between train and test sets
        (x_train, y_train), (x_test, y_test) = mnist.load_data()

        if K.image_data_format() == 'channels_first':
            x_train = x_train.reshape(x_train.shape[0], 1, img_rows, img_cols)
            x_test = x_test.reshape(x_test.shape[0], 1, img_rows, img_cols)
            input_shape = (1, img_rows, img_cols)
        else:
            x_train = x_train.reshape(x_train.shape[0], img_rows, img_cols, 1)
            x_test = x_test.reshape(x_test.shape[0], img_rows, img_cols, 1)
            input_shape = (img_rows, img_cols, 1)

        subset =  np.random.randint(0, x_train.shape[0], size=t*num_classes)
        x_train_full, y_train_full = x_train, y_train
        x_train = x_train[subset]
        y_train = y_train[subset]

        x_train = x_train.astype('float32')
        x_test = x_test.astype('float32')
        x_train /= 255
        x_test /= 255
        print('x_train shape:', x_train.shape)
        print(x_train.shape[0], 'train samples')
        print(x_test.shape[0], 'test samples')

        # convert class vectors to binary class matrices
        y_train = keras.utils.to_categorical(y_train, num_classes)
        y_test = keras.utils.to_categorical(y_test, num_classes)

        model = get_model(img_rows, img_cols)

        model.compile(loss=keras.losses.categorical_crossentropy,
                      optimizer=keras.optimizers.Adam(),
                      metrics=['accuracy'])
        weights_file = os.path.join(DIR,'train_size_%d_weights.hdf5' % t)
        if not os.path.exists(weights_file):
            model.fit(x_train, y_train,
                      batch_size=batch_size,
                      epochs=epochs,
                      verbose=1,
                      validation_split=validation_split)

            os.makedirs(DIR, exist_ok=True)
            model.save_weights(weights_file)
        else:
            model.load_weights(weights_file)

        score = model.evaluate(x_test, y_test, verbose=0)
        print('Test loss:', score[0])
        print('Test accuracy:', score[1])
        scores.append(score[1])
        #%%
        pred_model = keras.Model(inputs=model.input, outputs=model.layers[-3].output)
        features_train = pred_model.predict(x_train_full)
        features_test = pred_model.predict(x_test)
        pd.to_pickle((features_train, features_test),
                     os.path.join(DIR, 'train_size_%d_features.pkl' % t))
        #%%
        os.makedirs(DIR, exist_ok=True)
        model.save_weights(os.path.join(DIR,'train_size_%d_weights.hdf5' % t))

    pd.to_pickle({'scores': scores,
                  'train_size': train_size},
        os.path.join(DIR,'scores.pkl'))
