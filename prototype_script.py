#! /usr/bin/python3
import numpy as np
import matplotlib.pyplot as plt
import tensorflow as tf
import tensorflow.contrib.slim as slim
from tensorflow.contrib.factorization import KMeans
from sklearn.model_selection import StratifiedShuffleSplit
import os
from sacred import Experiment
from sacred.observers import FileStorageObserver
from sprite import sprite
from skimage.io import imsave

RUN_DIR = 'prototype_toy_problem_runs'

ex = Experiment(name='prototype_toy_problem')
ex.logger = tf.logging._logger
tf.logging.set_verbosity(tf.logging.INFO)


@ex.config_hook
def observer(config, command_name, logger):
    if 'run_dir'in config:
        ex.observers.append(FileStorageObserver.create(config['run_dir']))


@ex.config
def base_params():
    run_dir = RUN_DIR
    data='random'
    N = 2 # number of classes
    extra_samples = 100
    K = 5 # K shot
    comp = 2  # comp is Number of components per class
    n_splits = 5
    std=0.01
    ndim=2

    num_proto = 2*N
    l1 = 3e-4 # sensitive around 1e-4, 5e-4 is already too much

    one_proto = 0.0
    positive_wc = 0.0
    T = 1  # softmax temperature

    learning_rate = 0.005
    epochs = int(5e4)
    decay_steps = 0.0


@ex.named_config
def omniglot_inceptionV3():
    run_dir = "prototype_omniglot_inceptionV3_runs"
    seed = 56168151
    data = 'omniglot'
    N = 20
    K = 5
    num_proto = 2*N
    n_splits=1
    learning_rate = 0.05
    decay_steps=int(1e4)

@ex.named_config
def two_class():
    seed = 4


@ex.named_config
def oneshot_5way():
    seed = 4
    K = 1
    N = 5
    num_proto = 2*N


@ex.named_config
def fiveshot_5way():
    seed = 4
    K = 5
    N = 5
    num_proto = 2*N


@ex.named_config
def oneshot_20way():
    seed = 4
    K = 1
    N = 20
    num_proto = 2*N


@ex.named_config
def fiveshot_20way():
    seed = 4
    K = 5
    N = 20
    num_proto = 2*N


@ex.named_config
def high_dim():
    seed=4
    ndim=4096

@ex.capture
def gen_data(extra_samples=100, N=2, K=5, std=0.01, comp=2, ndim=2):
    # K shot
    # N is Num of classes
    # m is number of samples
    m = (K+extra_samples)*N
    # comp is Number of components per class

    # Sample a training 2D dataset

    # Class means are sampled from a U(0,1). I want data to be positive
    # because it is suposed to come from a previous ReLU activation.
    Y_mean = np.random.rand(comp*N, ndim)
    Y = np.random.randint(low=0, high=comp*N, size=m)
    X = std*np.random.randn(m, ndim)
    for k in range(comp*N):
        points = np.ix_(Y==k)
        X[points,:] += Y_mean[k, :]
    Y = np.mod(Y, N)
    return X, Y

@ex.capture
def omniglot_inceptionV3_data(N, K, _run, run_dir):
    features = np.load('./Data/omniglot_inceptionV3_features.npy')
    n_classes, n_samples, n_features = features.shape
    classes = np.random.choice(np.arange(features.shape[0]), size=N, replace=False)
    X = np.reshape(features[classes, :, :], (N*n_samples, n_features))
    y = np.repeat(np.arange(N), n_samples)

    images = np.load('./Data/omniglot.npy')
    X_images = np.reshape(images[classes,:,:], (N*n_samples, 28, 28))
    imsave(os.path.join(run_dir, str(_run._id), 'data_sprite.png'),
               sprite(X_images))
    np.savetxt(os.path.join(run_dir, str(_run._id), 'data.tsv'), X,
               delimiter='\t')
    np.savetxt(os.path.join(run_dir, str(_run._id), 'data_labels.tsv'), y,
               delimiter='\t')
    return X, y


@ex.capture
def plot_data(X, Y, N, _run):
    # plot
    plt.figure()
    for k in range(N):
        points = np.ix_(Y == k)
        plt.plot(X[points, 0].squeeze(), X[points, 1].squeeze(),
                 label=str(k+1),
                 marker='+', linestyle='', ms=8)
    plt.legend(loc='best')



@ex.capture
def get_split(X, Y, n_splits, N, K, seed):
    m = X.shape[0]
    skf = StratifiedShuffleSplit(train_size=K*N,
                                 test_size=m-K*N, n_splits=n_splits,
                                 random_state=seed)
    for support, test in skf.split(X, Y):
        x, y = X[support,:], Y[support]
        xt, yt = X[test,:], Y[test]
        yield (x,y, xt,yt)


@ex.capture
def clusterNclassif(x, y, protos, input_proto_idx, num_classes, num_proto,
                    learning_rate,
                    decay_steps=0.0, T=1.0,
                    l1=0.03, one_proto=100, positive_wc=100,
                    random_seed=0, scope=None):

    with tf.variable_scope(scope) as scope:
        x_emb = x

        # Defining q(x) = prob(p|x) = softmax over -distance to prototypes
        proto_list = tf.unstack(protos, axis=0)
        distance_protos = tf.stack(
            [tf.reduce_sum(tf.squared_difference(x_emb, pi), axis=-1)
            for pi in proto_list], axis=-1)

        act_proto = slim.softmax(-distance_protos, scope="softmax_act_proto")

        batch_size, dim = tuple(x_emb.get_shape().as_list())
        # defining C_l(x) = prob(l|p)prob(p|x)
        # classification layer over q(x) : cl(x) = softmax (q(x) Wc)
        reg = slim.l1_regularizer(l1)
        logits = slim.fully_connected(
            act_proto, num_classes, activation_fn=None,
            weights_initializer=tf.random_uniform_initializer(0, 1),
            weights_regularizer=reg,
            variables_collections=['Wc'],
            scope='classes')

        predictions = slim.softmax(logits, scope='predictions')

        labels = tf.argmax(predictions, axis=-1, output_type=tf.int32,
                           name='labels')

        y_one_hot = slim.one_hot_encoding(y, num_classes)

        class_loss = tf.losses.softmax_cross_entropy(y_one_hot, logits, T,
                                                     scope='xentropy')

        #tf.losses.add_loss(class_loss)
        slim.summaries.add_scalar_summary(class_loss)

        accuracy = slim.metrics.accuracy(labels, y)
        slim.summaries.add_scalar_summary(accuracy)

        scope.reuse_variables()


        # Add penalty to force at least one proto per class
        Wc = tf.get_variable('classes/weights')
        slim.summaries.add_image_summary(
            tf.expand_dims(tf.expand_dims(Wc, axis=0), axis=-1), 'Wc')
        if one_proto:
            Wc_proto_sum = tf.reduce_sum(Wc, axis=0)
            one_proto_penalty = tf.losses.absolute_difference(
                Wc_proto_sum, tf.ones_like(Wc_proto_sum), weights=one_proto, scope='one_proto_penalty')
            slim.summaries.add_scalar_summary(one_proto_penalty)
            tf.losses.add_loss(one_proto_penalty)
        # Add penalty to force positive Wc
        if positive_wc:
            positive_wc_penalty = tf.multiply(positive_wc, tf.reduce_sum(-Wc, axis=[0,1]),
                                              name='positive_wc_penalty')
            tf.losses.add_loss(positive_wc_penalty)

    if decay_steps != 0:
        learning_rate = tf.train.exponential_decay(
            learning_rate, tf.train.get_or_create_global_step(),
            decay_rate=0.5, decay_steps=decay_steps, staircase=True)
        slim.summaries.add_scalar_summary(learning_rate)
    update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS)

    with tf.control_dependencies(update_ops):
        with tf.variable_scope('classification_optimizer'):
            trainable_vars = slim.get_trainable_variables(scope=scope)
            loss = tf.losses.get_total_loss()
            train_op = tf.train.AdamOptimizer(learning_rate).minimize(
                loss, var_list=trainable_vars,
                global_step=tf.train.get_or_create_global_step())


        return {'classif_train_op': train_op,
                'predictions' : predictions,
                'labels': labels}


@ex.capture
def single_run(x, y, xt, yt,
               _run,
               num_proto, N, epochs,
               learning_rate, decay_steps, T,
               one_proto, positive_wc, l1,
               run_dir, data):
    LOG_DIR = os.path.join(run_dir, str(_run._id))
    tf.reset_default_graph()
    sess = tf.InteractiveSession()

    x_tensor = tf.placeholder(tf.float32, shape=[None, x.shape[1]], name='x')
    y_tensor = tf.placeholder(tf.int32, name='y')


    with tf.variable_scope('clustering'):
        tfkmeans = KMeans(
            inputs=x_tensor, num_clusters=num_proto,
            initial_clusters='kmeans_plus_plus',
            # these down are default values, copied them here to know them
            distance_metric='squared_euclidean',
            use_mini_batch=False, mini_batch_steps_per_iteration=1,
            kmeans_plus_plus_num_retries=2)

        (cluster_all_scores, cluster_idx, scores,
         cluster_centers_initialized, cluster_centers_var,
         cluster_init_op, cluster_training_op) = tfkmeans.training_graph()

        clusters = cluster_centers_var

    protos = tf.reshape(clusters, [num_proto, x.shape[1]])
    slim.summaries.add_histogram_summary(protos, 'protos')
    slim.summaries.add_image_summary(
            tf.reshape(protos, [1, x.shape[1], num_proto,1]), 'Wp')
    input_proto_idx = tf.reshape(cluster_idx[0], [x.shape[0]])
    slim.summaries.add_histogram_summary(input_proto_idx, 'input_proto_idx')

    out = clusterNclassif(
        x_tensor, y_tensor, protos, input_proto_idx,
        num_classes=N, scope='classif')

    all_summaries = tf.summary.merge_all()

    summary_writer = tf.summary.FileWriter(
            LOG_DIR, tf.get_default_graph())

    saver = tf.train.Saver(max_to_keep=2)

    feed_dict = {x_tensor: x, y_tensor: y}
    sess.run(tf.global_variables_initializer())
    sess.run([cluster_init_op], feed_dict)
    for i in range(0, epochs):
        sess.run(cluster_training_op, feed_dict)
        _, summary = sess.run([out['classif_train_op'], all_summaries],
                              feed_dict)
        summary_writer.add_summary(summary, i)
        if (i % 100) == 0:
            saver.save(sess, os.path.join(LOG_DIR, 'model'),
                       global_step=tf.train.get_or_create_global_step())


    G = tf.get_default_graph()
    labels = sess.run(out['labels'], feed_dict)

    softmax_act_proto_tensor = G.get_tensor_by_name('classif/softmax_act_proto/Softmax:0')
    softmax_act_proto = sess.run(softmax_act_proto_tensor, feed_dict)
    with tf.variable_scope('classif', reuse=True):
        Wc = sess.run(tf.get_variable('classes/weights'), feed_dict)
    predictions = sess.run(out['predictions'], feed_dict)

    dim_proto = sess.run(protos, feed_dict).T

    accuracy = np.mean(labels==y)


    ## run forward path on test data
    feed_dict = {x_tensor: xt, y_tensor: yt}
    test_predictions, test_labels , test_softmax_act_proto = \
        sess.run([out['predictions'], out['labels'], softmax_act_proto_tensor],
                 feed_dict)

    test_accuracy = np.mean(test_labels==yt)

    sess.close()

    return (dim_proto, Wc,
            predictions, labels, accuracy, softmax_act_proto,
            test_predictions, test_labels, test_accuracy, test_softmax_act_proto)



@ex.capture
def plot_weights_activations(dim_proto,softmax_act_proto, Wc, predictions,
                             labels, _run):
    ## plot
    plt.figure(figsize=(20,6))
    plt.subplot( 2, 4, 1)
    plt.imshow(dim_proto, interpolation='none', cmap='viridis')
    plt.title('Wp, dimension x proto')
    plt.colorbar()
    plt.subplot( 2, 4, 2)
    plt.imshow(softmax_act_proto, interpolation='none', cmap='viridis')
    plt.title('softmax, sample x proto')
    plt.colorbar()
    plt.subplot( 2, 4,3)
    plt.imshow(Wc.T, interpolation='none', cmap='viridis')
    plt.title('Wc.T, class x proto')
    plt.colorbar()
    plt.subplot( 2, 4, 4)
    plt.imshow(predictions.T, interpolation='none', cmap='viridis')
    plt.title('softmax, class x sample')
    plt.colorbar()


@ex.capture
def plot_prototypes(x, y, xt, yt, Wc, dim_proto, N, _run):
    proto_class_label = np.argmax(Wc, axis=1)
    plt.figure(figsize=(8,6))
    plt.title('dims x prototypes')
    colors=[]
    for k in range(N):
        lines = plt.plot(x[np.ix_(y==k),0].squeeze(),x[np.ix_(y==k),1].squeeze(), label='support'+str(k+1),
                     marker='o',  linestyle='', ms=3)
        plt.plot(xt[np.ix_(yt==k),0].squeeze(),xt[np.ix_(yt==k),1].squeeze(), label='test'+str(k+1),
                     marker='+', linestyle='', ms=1, color=lines[0].get_color())
        colors.append(lines[0].get_color())
    for k in range(N):
        points = dim_proto[:, np.ix_(proto_class_label==k)]
        p_ix = np.ix_(proto_class_label==k)
        plt.plot(points[0, :].squeeze(), points[1,:].squeeze(), marker='d',
                 linewidth=0, label='proto'+str(k+1), color=colors[k], ms=5)
        for p, p_ix in zip(points.T,p_ix[0]):
            plt.annotate(xy=p[0,:], s=str(p_ix))
    plt.legend(loc='best', framealpha=0.5)


def accuracy_stat(result, set_name='train'):
    vals = [result[r][set_name]['accuracy'] for r in result]
    return {'avg': np.mean(vals),
            'std': np.std(vals)}


def plot_acc_bar(accuracy_stats):
    plt.figure()
    for x, set_name in enumerate(['train', 'test']):
        y = accuracy_stats[set_name]['avg']
        yerr = accuracy_stats[set_name]['std']
        plt.bar(x, y)
        plt.errorbar(x, y, yerr)

        plt.annotate('%0.2f (%0.2f)' % (y, yerr), xy=(x, y))

        plt.xticks([0, 1], ('train', 'test'))



@ex.automain
def main(N, K, n_splits, seed, ndim, _run, data, run_dir):
    # sacred initializes numpy RNG and saves the seed
    tf.set_random_seed(seed)
    if data == 'random':
        # Generate random data
        X, Y = gen_data()
        if ndim==2:
            plot_data(X, Y)
            plt.savefig(os.path.join(run_dir, str(_run._id), 'data.pdf'))
            plt.close()
    else:
        X, Y = omniglot_inceptionV3_data()

    # Generate validation splits
    split =  get_split(X, Y)

    # Run algo for each split
    result = {}
    for i in range(n_splits):
        x,y,xt,yt = next(split)
        (dim_proto, Wc,
         predictions, labels, accuracy, softmax_act_proto,
         test_predictions, test_labels, test_accuracy, test_softmax_act_proto) =\
             single_run(x, y, xt, yt)
        result[i] = {
              'prototypes': dim_proto,
              'Wc': Wc,
              'train' : {
                  'predictions': predictions,
                  'labels': labels,
                  'accuracy': accuracy},
              'test': {
                  'predictions': test_predictions,
                  'labels': test_labels,
                  'accuracy': test_accuracy}
              }

        plot_weights_activations(
                dim_proto, softmax_act_proto , Wc, predictions, labels)
        plt.savefig(os.path.join(run_dir, str(_run._id),
                                 'fold%d_weights_activations.pdf' % i))
        plt.close()
        if X.shape[1] == 2:
            plot_prototypes(x, y, xt, yt, Wc, dim_proto)
            plt.savefig(os.path.join(run_dir, str(_run._id),
                                     'fold%d_prototypes.pdf' % i))
            plt.close()

    return result