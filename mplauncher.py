#!/usr/bin/env python3
#SBATCH -N 1 -c 2 --partition=court_sirocco --gres=gpu:2
#SBATCH --time=3:00:00
# -*- coding: utf-8 -*-
"""
Created on Tue May  2 11:41:30 2017

@author: thalita

launcher 2
trying to create a python script launchable with sbatch


"""
from utils import expand_params
from experiment import ex
from multiprocessing_on_dill import Pool


from sacred.config.config_files import save_config_file


if __name__ == "__main__":
    param_grid, _ = expand_params(ex)

    def fn(run_id):
        path = ex.run('get_path', config_updates=param_grid[run_id],
                      options={'--unobserved': True}).info["model_path"]
        path += '/'
        cfg_file = path + 'config_upd_%d.json' % run_id
        save_config_file(param_grid[run_id], cfg_file)
        ex.run(command_name='launcher', named_configs=[cfg_file])

    p = Pool(3)
    p.map(fn, range(len(param_grid)))
