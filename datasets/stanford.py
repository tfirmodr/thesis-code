#! /usr/bin/python3
# -*- coding: utf-8 -*-
"""
Created on Thu Nov 10 11:25:35 2016

@author: thalita

"""

import urllib.request
import os
import tarfile
from scipy.misc import imread
import numpy as np
import tensorflow as tf
from skimage.transform import resize

from .base import BigNumpyDataset, Downloadable, Split

DATA_DIR = './Data/'


class Stanford_dataset(BigNumpyDataset, Downloadable):
    URL = 'http://dags.stanford.edu/data/iccv09Data.tar.gz'
    width = 320
    height = 240
    shape = (height, width)
    dict_n_classes = {'regions': 8+1, 'surfaces': 3+1, 'fgbg': 2}

    default_params = dict(
            n_samples=715,
            preload=True,
            label_type='regions')

    def __init__(self, train_size=0.9, test_size=0.1, seed=None,
                 n_samples=None, preload=True, label_type='regions',
                 color_mode='RGB'):
        self.WIDTH = Stanford_dataset.width
        self.HEIGHT = Stanford_dataset.height
        if n_samples is None:
            n_samples = 715
        self._n_images = n_samples
        self.label_type = label_type
        self._n_classes = Stanford_dataset.dict_n_classes[label_type]

        Split.__init__(self, train_size, test_size, seed)

        # color modes available in scipy.misc. FarabetLeCun use YUV (~YCbCr)
        self.color_mode = color_mode
        self.filepath = self._maybe_download(Stanford_dataset.URL, DATA_DIR)
        self._preloaded = preload
        with tarfile.open(self.filepath, 'r:gz') as file:
            self._index = [fname for fname in file.getnames()
                          if fname.count('/images/')][0:n_samples]
            if preload:
                self.images, self.labels = self.full_load()

    def _load_and_get(self, image_files, labels=True, **kwargs):
        return self.\
            _extract_data(image_files, labels=labels,
                          label_type=self.label_type, **kwargs)

    def _extract_data(self, image_files, labels=True,
                      label_type='regions', verbose=False):
        """
        Extract the images into a 4D tensor [image index, y, x, channels].
        Extract the labels into 4D tensor [image index, y, x, class indicator].
        """
        filepath = self.filepath
        with tarfile.open(filepath, 'r:gz') as file:
            if verbose:
                print('Extracting images', image_files[0],
                      ' until ', image_files[-1])
            images = []
            for imgfile in image_files:
                with file.extractfile(imgfile) as img:
                    shape = Stanford_dataset.shape
                    images.append(resize(imread(img, mode=self.color_mode),
                                         shape))
            images = np.array(images, dtype=np.float32)

            if verbose:
                print('Extracting labels', image_files[0],
                      ' until ', image_files[-1])
            if label_type == 'fgbg':
                label_type_name = 'regions'
            else:
                label_type_name = label_type
            label_files = [fname.replace('images', 'labels')
                           .replace('jpg', label_type_name + '.txt')
                           for fname in image_files]
            if labels is True:
                labels = []
                for lblfile in label_files:
                    with file.extractfile(lblfile) as label:
                        label = np.loadtxt(label, dtype=np.float32)
                        label = resize(label, Stanford_dataset.shape,
                                       order=0, clip=True, preserve_range=True)

                        if label_type == 'fgbg':
                            label = np.array(label == 7.0, dtype=np.float32)
                        else:
                            # -1 indicates unkwnown, rescale so 0 is unknown
                            # an all labels are positive
                            label += 1
                            label = label * (label > 0)
                        labels.append(label.astype(int))
                labels = np.array(labels, dtype=np.float32)

                return images, labels
            else:
                return images
