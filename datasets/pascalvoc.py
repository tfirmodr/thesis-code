#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr 28 08:38:43 2017

@author: thalita
"""
import tensorflow as tf
import os
import numpy as np
from scipy.misc import imread
from concurrent.futures import ThreadPoolExecutor

from .base import Split


root_dir = "./Data/VOC%d/"
fnames_file = os.path.join(root_dir, "ImageSets/Segmentation/trainval.txt")
img_dir = os.path.join(root_dir, "JPEGImages")
label_dir = os.path.join(root_dir, "SegmentationClass")
new_label_dir = os.path.join(root_dir, "SegmentationClassLabel")

# colour map
label_colors = [(0,0,0),
                # 0=background
                (128,0,0), (0,128,0), (128,128,0), (0,0,128), (128,0,128),
                # 1=aeroplane, 2=bicycle, 3=bird, 4=boat, 5=bottle
                (0,128,128), (128,128,128), (64,0,0), (192,0,0), (64,128,0),
                # 6=bus, 7=car, 8=cat, 9=chair, 10=cow
                (192,128,0), (64,0,128), (192,0,128), (64,128,128), (192,128,128),
                # 11=diningtable, 12=dog, 13=horse, 14=motorbike, 15=person
                (0,64,0), (128,64,0), (0,192,0), (128,192,0), (0,64,128)
                # 16=potted plant, 17=sheep, 18=sofa, 19=train, 20=tv/monitor
                ]


class PascalVOC(Split):
    default_params = {'year': 2012,
                      'shape': [500, 500]}

    def __init__(self, year=2012, shape=(500, 500),
                 train_size=0.9, test_size=0.1, seed=None):
        self.year = year
        self.height, self.width = shape
        with open(fnames_file % self.year, 'r') as f:
            lines = f.readlines()
            self.basenames = [l.strip('\n') for l in lines]
        self._n_images = len(lines)
        self._n_classes = len(label_colors)
        Split.__init__(self, train_size, test_size, seed)

    def image_reader(self, basename):
        """
        basename: tensor string
        """
        with tf.name_scope('image_reader'):
            prefix = tf.convert_to_tensor(img_dir % self.year + '/',
                                          dtype=tf.string)
            suffix = tf.convert_to_tensor('.jpg', dtype=tf.string)
            image_fname = tf.string_join([prefix, basename, suffix])
            img_contents = tf.read_file(image_fname)
            img = tf.image.decode_jpeg(img_contents, channels=3)
            # cast channels to float32
            img_r, img_g, img_b = tf.split(axis=2, num_or_size_splits=3,
                                           value=img)
            img = tf.cast(tf.concat(axis=2, values=[img_b, img_g, img_r]),
                          dtype=tf.float32)
            if tf.rank(img) != 4:
                img = tf.expand_dims(img, 0)
            img = tf.image.resize_bilinear(img, (self.height, self.width))
            return img

    def label_reader(self, basename):
        """
        basename: tensor string
        """
        with tf.name_scope('label_reader'):
            prefix = tf.convert_to_tensor(label_dir % self.year + '/',
                                          dtype=tf.string)
            suffix = tf.convert_to_tensor('.png', dtype=tf.string)
            label_fname = tf.string_join([prefix, basename, suffix])
            label = tf.image.decode_png(tf.read_file(label_fname),
                                        channels=3)
            # Add channel and batch dimensions
            label = tf.expand_dims(label, 0)
            # resize image
            label = tf.image.resize_nearest_neighbor(
                    label, (self.height, self.width))
            label = tf.squeeze(label)
        with tf.name_scope('label_converter'):
            limg = tf.to_float(label)
            new_labels = tf.zeros_like(limg)[:,:,0]
            for i in range(len(label_colors)):
                c = tf.constant([[label_colors[i]]], dtype=tf.float32,
                                shape=[1, 1, 3],
                                name='c%d' % i)

                l = tf.equal(limg, c, name='label%d' %i)
                l = tf.where(tf.reduce_all(l, axis=-1),
                             i + tf.zeros(new_labels.shape),
                             tf.zeros(new_labels.shape))
                new_labels += l
            label = new_labels
            label = tf.expand_dims(label, 0)
        return label

    def get_input_fn(self, partition='train', batch_size=1, **kwargs):
        indices = self.train_index if partition == 'train' else self.test_index
        def input_fn():
            with tf.name_scope('input_fn'):
                fnames = tf.convert_to_tensor(
                    [self.basenames[i] for i in indices],
                    dtype=tf.string)
                fnames = tf.train.string_input_producer(fnames)
                basename = fnames.dequeue()
                image_batch, label_batch = tf.train.batch(
                    [self.image_reader(basename), self.label_reader(basename)],
                    batch_size,
                    enqueue_many=True)

                return {'X': image_batch}, label_batch

        return input_fn


def convert_labels(year):
    with open(fnames_file % year, 'r') as f:
        lines = f.readlines()
    basenames = [l.strip('\n') for l in lines]

    def convert_one(bname):
        old_path = label_dir % year + '/' + bname + '.png'
        new_path = new_label_dir % year + '/' + bname + '.labels.csv'
        if not os.path.exists(new_path):
            limg = imread(old_path)
            new_labels = np.zeros(limg.shape[0:2], dtype=int)
            for i in range(len(label_colors)):
                c = np.array([[label_colors[i]]])
                l = np.where(np.all(limg == c, axis=2),
                             i + np.zeros(new_labels.shape, dtype=int),
                             np.zeros(new_labels.shape, dtype=int))
                new_labels += l
            # saving in a csv with header specifying shape
            # and a line with the flattened label image
            # ex:
            # 3,3\n
            # 0,0,0,3,3,3,0,0,0\n
            header = '%d,%d' % new_labels.shape
            np.savetxt(new_path, np.reshape(new_labels, [1, -1]), fmt='%d',
                       header=header, comments='', delimiter=',')

    # ensure csv labels are created
    os.makedirs(new_label_dir % year, exist_ok=True)
    p = ThreadPoolExecutor()
    for b in basenames:
        p.submit(convert_one, b)
    p.shutdown()

