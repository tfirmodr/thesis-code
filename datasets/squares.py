#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr 27 13:55:41 2017

@author: thalita
"""
import numpy as np

from .base import NumpyDataset, Split


class Squares_dataset(NumpyDataset):

    default_params = dict(
            n_samples=10,
            HEIGHT=20, WIDTH=30,
            n_classes=2,
            noise=0.1)

    def __init__(self, train_size=0.9, test_size=0.1, seed=None,
                 n_samples=10, HEIGHT=20, WIDTH=30, n_classes=2,
                 noise=0.1):

        self._n_images = n_samples
        self.HEIGHT = HEIGHT
        self.WIDTH = WIDTH
        self._n_classes = n_classes
        self.images, self.labels = random_inputs(n_samples, HEIGHT, WIDTH,
                                                 n_classes, noise)
        Split.__init__(self, train_size, test_size, seed)


def random_inputs(n_samples=10, HEIGHT=30, WIDTH=20, n_classes=2, noise=False):
    # dummy numpy random arrays as input images
    images = np.zeros((n_samples, HEIGHT, WIDTH, 3), dtype=np.float32)
    labels = np.zeros((n_samples, HEIGHT, WIDTH), dtype=np.float32)
    for i in range(n_samples):
        row = np.random.randint(0, HEIGHT, 2)
        row.sort()
        col = np.random.randint(0, WIDTH, 2)
        col.sort()
        images[i, row[0]:row[1], col[0]:col[1], :] = 1
        labels[i, row[0]:row[1], col[0]:col[1]] = 1
    if noise:
        the_noise = np.float32(np.random.randn(n_samples, HEIGHT, WIDTH, 3))
        images = images + noise * the_noise
    return images, labels
