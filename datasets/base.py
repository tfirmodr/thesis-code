#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr 27 15:18:38 2017

@author: thalita
"""
import tensorflow as tf
import tensorflow.contrib.slim as slim
import numpy as np
import os
import urllib
from collections import deque
from sklearn.cross_validation import ShuffleSplit

from .dataset_factory import get_dataset

numpy_input_fn = tf.estimator.inputs.numpy_input_fn


class DataInfo(object):
    @property
    def n_images(self):
        return self._n_images

    @property
    def n_classes(self):
        return self._n_classes


class Split(DataInfo):
    @property
    def train_index(self):
        return self._train_index

    @property
    def test_index(self):
        return self._test_index

    @property
    def n_images(self):
        return self._n_images

    def len_test(self):
        return len(self._test_index)

    def len_train(self):
        return len(self._train_index)

    def __init__(self, train_size=0.9, test_size=0.1, seed=None):
        split = ShuffleSplit(n=self._n_images, n_iter=1,
                             test_size=test_size,
                             train_size=train_size,
                             random_state=seed)
        self._train_index, self._test_index = next(iter(split))


class Downloadable(object):
    @property
    def url(self):
        return self._url

    def _maybe_download(self, url, DATA_DIR):
        """Download dataset, unless it's already here."""
        filename = url.split('/')[-1]
        if not tf.gfile.Exists(DATA_DIR):
            tf.gfile.MakeDirs(DATA_DIR)
        filepath = os.path.join(DATA_DIR, filename)
        if not tf.gfile.Exists(filepath):
            print('Downloading dataset', filename)
            filepath, _ = urllib.request.urlretrieve(url, filepath)
            with tf.gfile.GFile(filepath) as f:
                size = f.size()
            print('Successfully downloaded', filename, size, 'bytes.')
        return filepath


class NumpyDataset(Split):
    @property
    def images(self):
        return self._images

    @property
    def labels(self):
        return self._labels

    def get_batch_list(self, alist, **kwargs):
        return self.images[alist, :, :], self.labels[alist, :, :]

    def get_input_fn(self, partition='train', batch_size=1, **kwargs):
        indices = self.train_index if partition == 'train' else self.test_index
        X, y = self.get_batch_list(indices, **kwargs)
        input_fn = numpy_input_fn({'X': X}, y,
                                  batch_size=batch_size,
                                  num_epochs=None,
                                  shuffle=False,
                                  queue_capacity=1000,
                                  num_threads=1)
        return input_fn



class BigNumpyDataset(NumpyDataset):

    @property
    def preloaded(self):
        return self._preloaded

    @property
    def index(self):
        return self._index

    def _get_preloaded(self, indices, labels=True, **kwargs):
        if labels:
            return self.images[indices], self.labels[indices]
        else:
            return self.images[indices]

    @classmethod
    def _load_and_get(self, image_files, labels=True, **kwargs):
        pass

    def get_batch(self, begin, end, **kwargs):
        if self._preloaded:
            return self._get_preloaded(np.arange(begin, end), **kwargs)
        image_files = self._index[begin:end]
        return self._load_and_get(image_files, **kwargs)

    def get_batch_list(self, alist, **kwargs):
        if self._preloaded:
            return self._get_preloaded(alist, **kwargs)
        image_files = [self._index[f] for f in alist]
        return self._load_and_get(image_files, **kwargs)

    def full_load(self, **kwargs):
        return self._load_and_get(self._index, **kwargs)


    def get_input_fn(self, partition='train', batch_size=1):
        if self._preloaded:
            return NumpyDataset.get_input_fn(self, partition, batch_size)
        else:
            # XXX does not work
            indices = self.train_index if partition == 'train'\
                else self.test_index

            fnames = deque([self._index[f] for f in indices])
            def next_input_fn():
                batch_files = []
                preload_size = min(len(fnames), 100*batch_size)
                for i in range(preload_size):
                    f = fnames.pop()
                    fnames.appendleft(f)
                    batch_files.append(f)

                X, y = self._load_and_get(batch_files)
                input_fn = \
                    numpy_input_fn(
                            {'X': X}, y,
                            batch_size=batch_size,
                            num_epochs=1,
                            shuffle=False,
                            queue_capacity=1000,
                            num_threads=1)
                return input_fn

            def input_fn():
                try:
                    return input_fn._current_fn()
                except tf.errors.OutOfRangeError:
                    input_fn._current_fn = next_input_fn()
                    return input_fn._current_fn()
            input_fn._current_fn = next_input_fn()

            return input_fn


class SlimDataset(DataInfo):
    """
    Provides compatible interface for a dataset used in slim style
    """
    def __init__(name, dataset_dir):
        self._train = get_dataset(name, 'train', dataset_dir)
        self._test = get_dataset(name, 'test', dataset_dir)

    @property
    def train(self):
        return self.train

    @property
    def train(self):
        return self.test

    @property
    def name(self):
        return self._name

    @classmethod
    def _get_images_labels(self):
        pass

    def get_input_fn(self, partition='train', batch_size=1):
        dataset = self._train if partition == 'train' \
            else self._test

        def input_fn():
            with tf.name_scope(self.name + '_data_provider'):
                provider = slim.dataset_data_provider.DatasetDataProvider(
                    dataset,
                    num_readers=1,
                    common_queue_capacity=20 * batch_size,
                    common_queue_min=10 * batch_size,
                    shuffle=False)
                # Get : image, labels to enqueue
                X, y = self._get_images_labels(provider)
                # enqueue/dequeue with tf.train.batch
                dequeued_batch = tf.train.batch(
                        {'X' : X, 'labels':y },
                        batch_size=batch_size,
                        num_threads=1,
                        capacity = 5 * batch_size)
                return dequeued_batch

        return input_fn
