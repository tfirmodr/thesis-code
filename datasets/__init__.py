from .squares import Squares_dataset
from .stanford import Stanford_dataset
from .pascalvoc import PascalVOC
from .omniglot import OmniglotNShotDataset, OmniglotDataset

_dataset_classes={'stanford': Stanford_dataset,
                 'squares': Squares_dataset,
                 'pascalvoc': PascalVOC,
                 'omniglot': OmniglotDataset}

def get_dataset(name, **params):
    return _dataset_classes[name](**params)


def get_by_name(name):
    return _dataset_classes[name]
