"""
Test-tube example launcher for a hyperparameter search on SLURM.

This example shows how to use gpus on SLURM with PyTorch.

@thalita: This example comes from test-tube repo.
I made some changes and annotations to exemplify
plafrim usage.
I'm also proposing to use a modified version of their cluster class.
that tries to submit jobs until cluster acceptance
"""
import random
import time
import torch
from test_tube import Experiment, HyperOptArgumentParser
from test_tube_plafrim import PlafrimCluster, main_fn
import os

# declare the function you want to run using the @main_fn decorator
@main_fn
def train(hparams, exp, cluster):
    """
    Arguments will be provided via the decorator.
    :param hparams: The arguments to run the model with.
    :param hparams: Experiment instance to log info if you want
    :param cluster: Cluster object intance

    Their use is optional but the function signature must account for them.
    You can declare a function with no/less arguments as follows:
    ```
    @main_fn
    def train(hparams, *args):
        ...
    
    @main_fn
    def train(*args):
        ...
    ```
    """
    # This part of the code is simulating a training loop
    # you can call your onw training function and pass hparams to it
    # Pretend to train.
    device = 'cpu' #'cuda' if cluster.on_gpu else 'cpu'
    print(device)
    if 'last_step' not in exp.tags:
        exp.tag({'last_step': 0})
    init_step = int(exp.tags['last_step'])
    print('init_step', init_step)
    x = torch.rand((1, hparams.x_val), device=device)
    for train_step in range(init_step, 100):
        y = torch.rand((hparams.x_val, 1), device=device)
        out = x.mm(y)
        # using exp object to log info
        # optional
        exp.log({'fake_err': out.item()})
        exp.tag({'last_step': train_step})
        # spend time artificially here in order to test
        # the job save/rescheduling
        current_time = time.time()
        while (time.time() < current_time + 2):
                pass
    


if __name__ == '__main__':
    # you will set the hyperparameter search using test-tubes arg parser
    # it is based on argpars so syntax should be familiar
    # you can choose random_search or grid_search
    # you can also run a single experiment with fixed params
    # by giving them explicit values at the cmd line
    parser = HyperOptArgumentParser(strategy='random_search')
    # in this example the two fake hyperparameters are y_val and x_val
    # Set up our argparser and make the hparams tunable.
    parser.opt_list('--y_val',
        default=12, options=[1, 2, 3, 4, 5, 6], tunable=True, type=int)
    parser.opt_list('--x_val',
        default=12, options=[20, 12, 30, 45], tunable=True, type=int)
    # You can mix hyperparameters for your model and general parameters for
    # this script
    parser.add_argument('--n_trials', default=5, type=int)
    parser.add_argument('--seed', default=0, type=int)
    # The following arguments define where logging is saved
    # in the default case, results will be in
    # ./test_tube_example/test_tube_data/my_test/
    # each hparam set will get a version_xx folder inside this directory
    parser.add_argument('--test_tube_exp_name', default='my_test')
    parser.add_argument('--log_path', default='./test_tube_example/')
    hyperparams = parser.parse_args()

    # set seed for hyperparam set selection
    # it uses python random
    random.seed(hyperparams.seed)


    if hyperparams.hpc_exp_number is not None:
        print("I'm trial %d!" % hyperparams.hpc_exp_number)

    # To enable cluster training we declare a Slurm cluster object
    cluster = PlafrimCluster(
        #queue='court_sirocco',
        queue='defq',
        hyperparam_optimizer=hyperparams,
        log_path=hyperparams.log_path,
        python_cmd='-u python3',
        test_tube_exp_name=hyperparams.test_tube_exp_name
    )

    # Set job compute details (this will apply PER set of hyperparameters.)
    cluster.per_experiment_nb_cpus = 1
    cluster.per_experiment_nb_gpus = 0
    cluster.per_experiment_nb_nodes = 1
    cluster.memory_mb_per_node = 2000
    cluster.job_time = '06:00'
    # This sets how long before the time limit your job will be saved and
    # replaced by a new job (in case it is not finished),
    # renewing it's allocated time (default value is 5 min)
    cluster.minutes_to_checkpoint_before_walltime=5

    # Email results if your hpc supports it.
    # cluster.notify_job_status(
    #     email='some@email.com', on_done=True, on_fail=True)

    # You can add SLURM modules to be loaded before calling sbatch
    # cluster.load_modules([
    #     'slurm',
    #     'scm/git/2.18.0',
    #     'language/intelpython/3.5.2',
    #     'compiler/gcc/5.3.0',
    #     'compiler/intel/64/2017_update4',
    #     'mpi/openmpi/gcc/2.0.2',
    #     'cudnn/6.0',
    #     'compiler/cuda/8.0/toolkit/8.0.61',
    #      or
    #     'compiler/cuda/9.0/toolkit/9.0',
    #     etc...
    # ])

    # You can add bash commands to the non-SLURM portion.
    # for instance, activate your pyenv/conda env
    # cluster.add_command('source activate gpu-pytorch04')
    cluster.add_command('echo $SLURM_JOB_ID')
    cluster.add_command('source activate cpu-pytorch04')

    # You can add custom SLURM commands which show up as:
    # #comment
    # #SBATCH --cmd=value
    # ############
    # cluster.add_slurm_cmd(
    #    cmd='cpus-per-task', value='1', comment='CPUS per task.')


    # This call will generate one script per set of hparams
    # cluster.optimize_parallel_cluster_gpu(
    cluster.optimize_parallel_cluster_cpu(
        # Function to execute:
        train,
        # Number of hyperparameter combinations to search:
        nb_trials=hyperparams.n_trials,
        # This is what will display in the slurm queue:
        job_name='tt_job')
