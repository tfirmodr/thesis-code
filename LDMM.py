#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr 18 16:03:33 2018

@author: thalita
"""

import numpy as np
from sklearn.neighbors import NearestNeighbors
from sklearn.base import TransformerMixin, BaseEstimator, ClassifierMixin

from scipy import sparse
from tqdm import tqdm
from skorch_utils import TransformerNet, StopperNet, SaveWeights
from skorch.utils import to_tensor
from skorch.classifier import NeuralNetClassifier
from skorch.callbacks import Callback
import torch

from laplacian_utils import compute_W, compute_L


class LDMNet(object):
    """ Inherit this class and implement ThetaEstimator methods"""
    def __init__(self, mu=0.01, lambda_bar=0.01, n_neighbors=20,
                 tol=1e-4, max_iter=200, n_jobs=1,
                 concatenate_input=True,
                 epochs_alpha_update=2,
                 max_epochs=200,
                 *args, **kwargs):
        '''
        - mu: multiplier for the alternating direction method of multipliers (ADMM)
        - l or lambda_: regularization + temperature
        $\hat{\lambda} = t/2\lambda = (8 \lambda\gamma)^{-1}$
            - lambda: regularization strenth
            - t or gamma: heat kernel param, $\gamma=\frac{1}{4t}$
        - n_neighbors: for kNN graph
        - feature mask:  array with indices for features to work on
        '''
        super().__init__(*args, max_epochs=max_epochs, **kwargs)
        self.concatenate_input = concatenate_input

        self.max_epochs = max_epochs
        self.epochs_alpha_update = epochs_alpha_update
        self.mu = mu
        self.lambda_bar = lambda_bar
        self.n_neighbors = n_neighbors
        self.n_jobs = n_jobs
        self.tol = tol
        self.max_iter = max_iter

#    def initialize(self):
#        super().initialize()
#        self.alpha = None
#        self.Z = None
#        self.ksi = None
#        self.solver_info_ = []


    def _solve_lin_sys(self, W, L, ksij, Zj):
        c = self.mu/self.lambda_bar
        v = (ksij-Zj)
        # Solve Ax = b
        A = (L + c*W).tocsc()
        b = c * W * v
        x0 = ksij
        # preconditioning matrix M should approximate inv A
        # spilu returns SuperLU object with solve(b) that approx solves Ax=b
        # To improve the better approximation to the inverse, you may need to
        # increase `fill_factor` AND decrease `drop_tol`.
        M_approx = sparse.linalg.spilu(A, drop_tol=1e-5, fill_factor=50)
        M = sparse.linalg.LinearOperator(shape=A.shape, matvec=M_approx.solve)
        x, info = sparse.linalg.cg(A, b, x0, M=M, tol=self.tol, maxiter=self.max_iter)
        self.solver_info_.append(info)
        return x

    def _update_alpha(self, X, ksi, Z=None, X_imgs=None):
        '''
        X : samples
        ksi: features
        Z:  dual variable
        '''
        if self.concatenate_input:
            input = X if X_imgs is None else X_imgs
            p = lambda ksi: np.concatenate([ksi, input], axis=-1)
        else:
            p = lambda ksi: ksi

        self.W_ = compute_W(p(ksi), self.n_neighbors,
                            nn_radius=min(20,self.n_neighbors//2))
        self.L_ = compute_L(self.W_)

        alphas = []
        self.solver_info_ = []


        n_features = ksi.shape[1]

        if Z is  None:
            Z = np.zeros(X.shape[0], n_features)

        for j in range(n_features):
            alphaj = self._solve_lin_sys(self.W_, self.L_, ksi[:,j], Z[:,j])
            alphaj = alphaj.reshape(-1, 1)
            alphas.append(alphaj)
        alphas = np.concatenate(alphas, axis=-1)
        return alphas

    def fit(self, X, y, X_imgs=None):
        if not self.warm_start or not self.initialized_:
            self.initialize()
        ksi = self.transform(X)
        self.Z = np.zeros_like(ksi)
        self.ksi = ksi
        for i in range(0, self.max_epochs, self.epochs_alpha_update):
            # alpha update
            self.alpha = self._update_alpha(X, self.ksi, self.Z, X_imgs=X_imgs)
            # estimator update
            self.partial_fit(dict(X=X,
                                  Z=self.Z.astype(X.dtype),
                                  alpha=self.alpha.astype(X.dtype)),
                             y, epochs=self.epochs_alpha_update)
            if self.stop:
                break
            self.ksi = self.transform(X)

            # dual variable update
            dZ = self.alpha - self.ksi
            self.Z = self.Z + dZ
        return self


class ThetaEstimator(object):
    """
    Specification of class that should inherit LDMNet and implement
    """
    def __init__(self, *args, **kwargs):
#        super().__init__(*args, **kwargs)
        self.stop = False
        self.warm_start = False
        self.initialized_ = False

    def initialize(self):
        self.initialized_ = True
        pass

    def partial_fit(self, X, y, epochs=2):
        """  X is dict with X, alpha and Z"""
        return self

    def transform(self, X):
        return X


class LDMNetDummy(LDMNet, ThetaEstimator):
    def __init__(self, *args,**kwargs):
        super().__init__(*args, **kwargs)

class LDMNetReg(LDMNet, ThetaEstimator):
    def __init__(self, *args, lr=0.01, **kwargs):
        super().__init__(*args, **kwargs)
        self.lr = lr

    def initialize(self):
        super().initialize()
        self.last_ksi = None

    def partial_fit(self, X, y, epochs=2):
        if self.last_ksi is None:
            self.last_ksi = X['X']
        alpha = X['alpha']
        Z = X['Z']
        for _ in range(epochs):
            dX = self.mu*(self.last_ksi - Z - alpha)
            self.last_ksi -= self.lr * dX
        return self

    def transform(self,X):
        if self.last_ksi is None:
            return X
        else:
            return self.last_ksi

    def fit_transform(self, X, y):
        self.fit(X, y)
        return self.ksi


class SaveLDMNetVars(Callback):
    """ Callback to save ksi, alpha and Z"""
    def __init__(self, every_n_epochs=100):
        self.every_n_epochs = every_n_epochs

    def initialize(self):
        self.ksi = []
        self.Z = []
        self.alpha = []
        self.last_save = 0
        self.epochs = []
        return self

    def on_train_end(self, net, **kwargs):
        """
        Method notified after partial fit, when it is interesting to save
        ldmnet vars.
        """
        epochs = len(net.history)
        if epochs - self.last_save >= self.every_n_epochs:
            self.ksi.append(net.ksi)
            self.Z.append(net.Z)
            self.alpha.append(net.alpha)
            self.last_save = epochs
            self.epochs.append(epochs - 1)



class LDMNetSkorch(LDMNet, StopperNet, TransformerNet, NeuralNetClassifier,
                   ClassifierMixin):
    __doc__ = LDMNet.__doc__ + NeuralNetClassifier.__doc__
    def __init__(self, module, layer_name,
#                 lr=0.01,
#                 tol=1e-4, max_iter=200,
#                 max_epochs=200,
#                 verbose=0,
                 **kwargs):
        super().__init__(module=module,
#             lr=lr,
#             tol=tol, max_iter=max_iter,
#             max_epochs=max_epochs,
#             verbose=verbose,
             **kwargs)
        self.layer_name = layer_name

    def transform(self, X):
        return super().transform(X, name=self.layer_name)

    def get_loss(self, y_pred, y_true, X=None, training=False):
        if isinstance(X, dict):
            X_X = X['X']
        else:
            X_X = X
        loss = super().get_loss(y_pred, y_true, X_X, training)
        if X is not None and self.alpha is not None:
            ksi = self.infer(X['X'], name=self.layer_name)
            alpha = to_tensor(X['alpha'], device=ksi.device)
            Z = to_tensor(X['Z'], device=ksi.device)
            reg_loss = torch.norm(alpha - ksi + Z)
            reg_loss = reg_loss * self.mu * 0.5 * (1/ksi.shape[0])
            loss += reg_loss
        return loss



if __name__ == '__main__' :
    from sklearn.datasets import make_moons
    from prototype_utils import MLPClassifier
    from skorch_utils import EarlyStopping, NaNStopping, MLP, LRScheduler

    from scipy.stats import uniform, reciprocal
    from sklearn.model_selection import RandomizedSearchCV, StratifiedShuffleSplit

    random_state=0

    X, y = make_moons(
        n_samples=200, shuffle=True, noise=None, random_state=random_state)
    X = X.astype(np.float32)
    y = y.astype(np.int64)

#    dummy_model = LDMNetDummy(mu=0.01, lambda_bar=0.01)
#    dummy_model.fit(X,y)
    torch.manual_seed(random_state)
    module = MLP(n_in=X.shape[1], num_units=4, n_out=2)
    model = LDMNetSkorch(
        module=module,
        layer_name='hidden1',
        max_epochs=300,
        batch_size=200,
        lr=0.05,
        mu=0.01, lambda_bar=0.01,
        device='cuda',
        verbose=1,
        callbacks=[EarlyStopping(10, 'valid_loss'),
                   LRScheduler('ReduceLROnPlateau'),
                   SaveLDMNetVars(20),
                   NaNStopping()]
    )

#    model.fit(X,y)

    params = {
    'lr': uniform(1e-4, 1e-2),
    'mu': reciprocal(1e-4, 1e2),
    'lambda_bar': reciprocal(1e-4, 1e2)
    }

    search = RandomizedSearchCV(
        model, n_iter=5, n_jobs=1,
        cv=StratifiedShuffleSplit(1, test_size=0.2, random_state=random_state),
        refit=True, random_state=random_state,
        param_distributions=params, verbose=1)
    search.fit(X,y)
    model = search.best_estimator_

