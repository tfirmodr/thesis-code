#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May  1 17:24:07 2017

@author: thalita
"""
import tensorflow as tf
from collections import defaultdict

from .contextlibbackport import ContextDecorator
from .internal import ContextMethodDecorator


class LogEstimatorEval(ContextDecorator, ContextMethodDecorator):
    """
    Decorator to intercept calls to Estimator.evaluate
    """
    def __init__(self, experiment):
        self.experiment = experiment
        self.metrics = defaultdict(list)

        def decorator_fn(instance, original_method, original_args,
                                 original_kwargs):
            result = original_method(instance, *original_args,
                                     **original_kwargs)
            for key, value in result.items():
                metrics[key].append(items)

        ContextMethodDecorator.__init__(self,
                                        tf.estimator.Estimator,
                                        "evaluate",
                                        decorator_fn)