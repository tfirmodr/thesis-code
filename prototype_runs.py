#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jun  1 18:38:12 2018

@author: thalita
"""

import numpy as np
import pandas as pd
import os
from tqdm import tqdm
from shutil import copy
from prototype_models import (classify_prototypical, classify_softmax,
                              classify_cluster_softmax, classify_svm)

from prototype_data import get_dataset
from prototype_utils import load_results


class Experiment():
    def __init__(self, model_list=None, path=None,
                 dataset='mnist',
                 train_size=0.5,
                 n_classes=10,
                 n_splits=1, n_episodes=None,
                 n_jobs=2,
                 cv_repeats=5,
                 random_seed=0,
                 **cluster_classify_kwargs):
        if model_list is None:
            self.model_list = ['l1_plain', 'l2_plain',
                               'l1_cat', 'l2_cat',
                               'softmax', 'single_prototype', 'svm']
        else:
            self.model_list = model_list
        self.path = './prototype_results/'
        self.path += path if path is not None else 'default_exp'
        self.dataset = get_dataset(dataset, random_seed=random_seed)
        self.n_classes = n_classes
        self.n_episodes = n_episodes
        self.n_splits = n_splits
        self.random_seed = random_seed
        self.train_size = train_size
        self.skf, self.skf_cv = self.dataset.splitters(
                n_splits=n_splits,
                train_size=self.train_size,
                cv_repeats=cv_repeats)
        self.n_jobs = n_jobs
        # default values chosen from preliminary analysis
        default = dict(
            n_Cs=3, Cs_lim=(2, 6),
            n_n_clusters=3, n_clusters_lim=(1, 3),
            n_class_weights=3, class_weight_lim=(0.0, 2.0),
            n_shortcut=1, shortcut_lim=(0, 1), shortcut_scale='linear')
        if dataset == 'mnist':
            default.update(n_n_clusters=4, n_clusters_lim=(1, 4))
        if cluster_classify_kwargs is not None:
            default.update(cluster_classify_kwargs)
        self.cluster_classify_kwargs = default

        print(self.__dict__)

    def _get_or_load_data_random_states(self, path):
        """
        Generates `n_episodes` random states for numpy RNG and saves to
        `path`/data_random_states.pkl, or loads them if file exists
        """
        drs_file = os.path.join(path, 'data_random_states.pkl')
        if not os.path.exists(drs_file):
            data_random_states = []
            for _ in range(self.n_episodes):
                state = np.random.RandomState().get_state()
                data_random_states.append(state)
            # save data random states
            pd.to_pickle(data_random_states, drs_file)
        else:
            data_random_states = pd.read_pickle(drs_file)
        return data_random_states

    def _try_load_results(self, results_file, overwrite):
        print("Results at " + results_file)
        model_list = self.model_list
        results = dict()
        # if overwrite is 'all', return right away
        if isinstance(overwrite, str):
            if overwrite.lower() == 'all':
                print("Overwriting all")
                return results, model_list
            else:
                overwrite = None

        # if there is a file, load its results
        if os.path.exists(results_file):
            print('Loading existing results from ' + results_file)
            results = pd.read_pickle(results_file)['results']
            print("Already ran: ", list(results.keys()))
            # check which models to compute
            for model in results.keys():
                # episode based exp
                if (self.n_episodes is not None
                    and 'episode' in results[model].columns):
                    last = results[model].episode.values[-1]
                    if last >= self.n_episodes-1:
                        # all episodes computed for this model,
                        # do not recompute
                        model_list.remove(model)
                else:  # simple experiment
                    model_list = set(model_list) - set(results.keys())

            if overwrite is not None:
                model_list = list(model_list.union(overwrite))
                for item in overwrite:
                    print("Overwriting ", item )
                    del results[item]
            else:
                model_list = list(model_list)
        return results, model_list

    def _multiple_episodes(self, results, model_list, results_file,
                           data_random_states=None):
        N = self.n_classes
        if results != {}:
            init_ep = min([res_df.episode.values[-1]
                           for model, res_df in results.items()]) +1
            print('Continuing from ep ', init_ep)
        else:
            init_ep = 0
        # run episodes appending to existing dict
        for i in tqdm(range(init_ep, self.n_episodes), 'episodes',
                      initial=init_ep, total=self.n_episodes):
            drs = (None if data_random_states is None
                   else data_random_states[i])
            X_imgs, X, y = self.dataset.gen_data(
                    N=N, plot=False,
                    random_state=drs)
            res_dict = self._run_one(
                    model_list=model_list,
                    X=X, y=y)
            for k in res_dict:
                res = res_dict[k].assign(episode=[i]*len(res_dict[k]))
                if k not in results:
                    results[k] = res
                else:
                    results[k] = results[k].append(res, ignore_index=True)
            pd.to_pickle({'results': results,
                          'info': self.__dict__}, results_file)
            bk_file = results_file + '_%0.3d'
            if os.path.exists(bk_file % (i-1)):
                os.remove(bk_file % (i-1))
            copy(results_file, bk_file % (i))

        return results

    def _make_dataframe(res):
        """Make dataframe with best cv result + test accuracy for all
        train/test splits"""
        accs, models = res
        df = pd.DataFrame()
        for i in range(len(models)):
            model = models[i]
            dict_info = model.cv_results_
            info = pd.DataFrame(dict_info)
            df = df.append(info.loc[model.best_index_], ignore_index=True)
        df = df.assign(holdout_test_accuracy=accs)
        return df

    def _run_one(self, model_list, X, y, return_models=False):
        N = self.n_classes
        skf, skf_cv = self.skf, self.skf_cv

        # n Cs for softmax and svm
        if 'n_Cs' in self.cluster_classify_kwargs:
            n_Cs = self.cluster_classify_kwargs['n_Cs']
        else:
            n_Cs = 5

        result = dict()
        models = dict()
        if 'single_prototype' in model_list:
            res_proto = classify_prototypical(
                X, y, N, skf, skf_cv, random_state=self.random_seed,
                return_model=True, verbose=False)
            df_proto = pd.DataFrame(dict(holdout_test_accuracy=res_proto[0]))
            result.update({'single_prototype': df_proto})
            if return_models:
                models['single_prototype'] = res_proto[1]

        if 'softmax' in model_list:
            res_softmax = classify_softmax(
                X, y, N, skf, skf_cv, n_Cs=n_Cs,
                n_jobs=self.n_jobs,
                random_state=self.random_seed,
                return_model=True, verbose=False)
            result.update({'softmax': Experiment._make_dataframe(res_softmax)})
            if return_models:
                models['softmax'] = res_softmax[1]
        if 'svm' in model_list:
            res_svm = classify_svm(
                X, y, N, skf, skf_cv, n_Cs=n_Cs,
                n_jobs=self.n_jobs,
                random_state=self.random_seed,
                return_model=True, verbose=False)
            result.update({'svm': Experiment._make_dataframe(res_svm)})
            if return_models:
                models['svm'] = res_svm[1]

        model_list = list(set(model_list) -
                          {'single_prototype', 'softmax', 'svm'})
        res_cluster_classify = classify_cluster_softmax(
            X, y, N, skf, skf_cv,
            n_jobs=self.n_jobs,
            random_state=self.random_seed,
            models_to_test=model_list,
            return_model=True,
            verbose=False,
            **self.cluster_classify_kwargs)

        for k, v in res_cluster_classify.items():
            result[k] = Experiment._make_dataframe(v)
            if return_models:
                models[k] = v[1]

        if return_models:
            return result, models
        else:
            return result

    def run_multiple_class_subsets(self, overwrite=None):
        """
        Run for `self.n_episodes` class subsets with `self.num_classes` classes
        for models in `model_list`.
        Loads existing results file and appends to it.
        Overwrite is either None, 'all' or an iterable with models names
        to be re-run, overwriting existing results.
        """
        N = self.n_classes

        path = self.path
        path = os.path.join(path, "N{}classes_{}epi".format(N,
                            self.n_episodes))
        os.makedirs(path, exist_ok=True)

        np.random.seed(self.random_seed)
        # pick data_random_states for episodes
        data_random_states = self._get_or_load_data_random_states(path)

        # if there is a file, load its results
        results_file = os.path.join(path, 'results.pd.pkl')
        results, model_list = self._try_load_results(results_file, overwrite)

        if model_list != []:
            print('Running models:', model_list)
            # run episodes appending to existing dict
            self._multiple_episodes(
                    results, model_list, results_file,
                    data_random_states=data_random_states)
            # save results and data random states
            pd.to_pickle({'path': path,
                          'results': results,
                          'info': self.__dict__}, results_file)
        else:
            print('No new models to run')
            results = pd.read_pickle(results_file)['results']

        return results

    def run_multiple_splits(self, overwrite=None, return_models=False):
        """Run mutiple `n_splits` with `train_size` samples for
        models in `model_list`. Loads existing results file and appends to it.
        Overwrite is either None, 'all', or an iterable with models names
        to be re-run, overwriting existing results.
        """

        np.random.seed(self.random_seed)

        path = self.path
        path = os.path.join(path, "{}_splits_{}_train_size".format(
                self.n_splits, self.train_size))
        os.makedirs(path, exist_ok=True)

        results_file = os.path.join(path, 'results.pd.pkl')

        # if there is a file, load its results
        results, model_list = self._try_load_results(results_file, overwrite)

        if model_list != []:
            print('Running models:', model_list)
            # run splits appending to existing dict
            X_imgs, X, y = self.dataset.gen_data(plot=False)

            new_results = self._run_one(
                model_list=model_list,
                X=X, y=y, return_models=return_models)

            out = {'path': path,
                   'info': self.__dict__}
            if return_models:
                new_results, models = new_results
                out.update({'models': models})

            results.update(new_results)
            out.update({'results': results})
            # save results and data random states
            pd.to_pickle(out, results_file)
        else:
            print('No new models to run')
            out = pd.read_pickle(results_file)
        return out


def test():
    ex = Experiment(
            model_list = ['svm','single_prototype'],
            path='test',
            dataset='mnist',
            train_size=110,
            n_splits=2
    )
    results = ex.run_multiple_splits()
    file = './prototype_results/test/2_splits_110_train_size/results.pd.pkl'
    lresults, lex = load_results(file)
    os.remove(file)
    os.removedirs('./prototype_results/test/2_splits_110_train_size/')
    return ex, results, lex, lresults



if __name__ == '__main__':
    ex, results, lex, lresults = test()