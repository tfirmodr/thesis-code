#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jun 11 11:11:57 2018

@author: thalita
"""

import numpy as np
from sklearn.neighbors import NearestNeighbors
from sklearn.base import TransformerMixin, BaseEstimator, ClassifierMixin

from scipy import sparse
from tqdm import tqdm
from skorch_utils import TransformerNet, StopperNet, NNClassifier, SaveWeights
from skorch.utils import to_tensor
from skorch.callbacks import Callback
import torch

from prototype_models  import MyKMeans


class ProtoRegNet(StopperNet, NNClassifier):
    __doc__ = """ NN with regularization based on prototype reconstruction. """

    def __init__(self, module, layer_name,
                 reg=0.01, n_proto=3,
                 tol=1e-4, max_iter=200, n_jobs=1,
                 concatenate_input=True,
                 epochs_alpha_update=2,
                 max_epochs=200,
                 **kwargs):
        super().__init__(module=module, max_epochs=max_epochs, **kwargs)
        self.layer_name = layer_name
        self.concatenate_input = concatenate_input
        self.max_epochs = max_epochs
        self.epochs_alpha_update = epochs_alpha_update
        self.reg = reg
        self.n_proto = n_proto
        self.n_jobs = n_jobs
        self.tol = tol
        self.max_iter = max_iter

    def _update_alpha(self, X, ksi,
                      Z,
                      X_imgs=None):
        if self.concatenate_input:
            input = X if X_imgs is None else X_imgs
            p = lambda ksi: np.concatenate([ksi, input], axis=-1)
        else:
            p = lambda ksi: ksi
        # Reuse previous protos if this is not the first update
        if self.protos is None:
            init = 'k-means++'
            n_init = 10
        else:
            init = self.protos
            n_init = 1
        cluster = MyKMeans(
                self.n_proto,
                init=init,
                n_init=n_init,
                max_iter=self.max_iter,
                tol=self.tol,
                n_jobs=self.n_jobs)
        out = cluster.fit_transform(p(ksi))
        # MyKmeans returns dict with X (transformed) X_old and protos
        self.q = out['X']
        self.protos = out['protos']
        alphas = np.matmul(self.q, self.protos)[:, :ksi.shape[1]]
        return alphas

    def initialize_module(self):
        super().initialize_module()
        self.module_


    def fit(self, X, y, X_imgs=None):
        if not self.warm_start or not self.initialized_:
            self.initialize()
        ksi = self.transform(X)
#        self.Z = np.zeros_like(ksi)
        self.ksi = ksi
        self.protos = None
        for i in range(0, self.max_epochs, self.epochs_alpha_update):
            # alpha update
            self.alpha = self._update_alpha(X, self.ksi,
                                            None,  # self.Z,
                                            X_imgs=X_imgs)
            # estimator update
            self.partial_fit(dict(X=X,
                                  # Z=self.Z.astype(X.dtype),
                                  alpha=self.alpha.astype(X.dtype)),
                             y, epochs=self.epochs_alpha_update)
            if self.stop:
                break
            self.ksi = self.transform(X)

            # dual variable update
            # dZ = self.alpha - self.ksi
            # self.Z = self.Z + dZ
        return self

    def transform(self, X):
        return super().transform(X, name=self.layer_name)

    def get_loss(self, y_pred, y_true, X=None, training=False):
        if isinstance(X, dict):
            X_X = X['X']
        else:
            X_X = X
        loss = super().get_loss(y_pred, y_true, X_X, training)
        if X is not None and self.alpha is not None:
            ksi = self.infer(X['X'], name=self.layer_name)
            alpha = to_tensor(X['alpha'], device=ksi.device)
            # Z = to_tensor(X['Z'], device=ksi.device)
            # reg_loss = torch.norm(alpha - ksi + Z)
            reg_loss = torch.norm(alpha - ksi)
            reg_loss = reg_loss * self.reg * 0.5 * (1/ksi.shape[0])
            loss += reg_loss
        return loss


class SaveProtoRegNetVars(Callback):
    """ Callback to save ksi, alpha and Z"""
    def __init__(self, every_n_epochs=100):
        self.every_n_epochs = every_n_epochs

    def initialize(self):
        self.ksi = []
#        self.Z = []
        self.alpha = []
        self.last_save = 0
        self.epochs = []
        return self

    def on_train_end(self, net, **kwargs):
        """
        Method notified after partial fit, when it is interesting to save
        ldmnet vars.
        """
        epochs = len(net.history)
        if epochs - self.last_save >= self.every_n_epochs:
            self.ksi.append(net.ksi)
#            self.Z.append(net.Z)
            self.alpha.append(net.alpha)
            self.last_save = epochs
            self.epochs.append(epochs - 1)

if __name__ == '__main__' :
    from sklearn.datasets import make_moons
    from prototype_utils import MLPClassifier
    from skorch_utils import EarlyStopping, NaNStopping, MLP, LRScheduler

    from scipy.stats import uniform, reciprocal, randint
    from sklearn.model_selection import RandomizedSearchCV, StratifiedShuffleSplit

    random_state=0

    X, y = make_moons(
        n_samples=200, shuffle=True, noise=None, random_state=random_state)
    X = X.astype(np.float32)
    y = y.astype(np.int64)


    # default optimizer is torch.optim.SGD
    # default params are momentum=0, dampening=0, weight_decay=0,
    torch.manual_seed(random_state)
    module = MLP(n_in=X.shape[1], num_units=4, n_out=2)
    model = ProtoRegNet(
        module=module,
        layer_name='hidden1',
        n_proto=10,
        max_epochs=300,
        batch_size=200,
        optimizer=torch.optim.SGD,
        lr=0.05,
        reg=0.01,
        use_cuda=False,
        verbose=1,
        callbacks=[EarlyStopping(10, 'valid_loss'),
                   LRScheduler('ReduceLROnPlateau'),
                   SaveProtoRegNetVars(20),
                   NaNStopping()]
    )

#    model.fit(X,y)


    params = {
    'lr': uniform(1e-4, 1e-2),
    'reg': reciprocal(1e-5, 1e5),
    'n_proto': randint(2,50)
    }

    search = RandomizedSearchCV(
        model, n_iter=5, n_jobs=1,
        cv=StratifiedShuffleSplit(1, test_size=0.2,
                                  random_state=random_state),
        refit=True, random_state=random_state,
        param_distributions=params, verbose=1)
    search.fit(X,y)
    model = search.best_estimator_