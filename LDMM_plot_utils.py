#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Apr 23 17:13:00 2018

@author: thalita
"""
from sklearn.decomposition import PCA
import matplotlib.pyplot as plt
import numpy as np
import scipy as sp
import re

def get_LDMNet_var(model, var_name):
    """
    for retro compatibility reasons,
    try to look both in the model of for the callback
    """
    model_var = model.__getattribute__(var_name)
    if isinstance(model_var, list):
        return model_var, np.arange(len(model_var))
    else:
        try:
            d_callbacks = dict(model.callbacks_)
            r = re.compile('Save.*Vars')
            key = next(filter(r.match, d_callbacks.keys()))
            saver_callback = d_callbacks[key]
            var = saver_callback.__getattribute__(var_name)
            epochs = saver_callback.__getattribute__('epochs')
            return var, epochs
        except KeyError:
            raise RuntimeError("Callback 'SaveLDMNetVars' not found in model")

def plot_ksi_alpha_time(model, every_n=3, ncols=5, y=None, **kwargs):
    fitted_projs = plot_over_time(
            model, which_var='alpha', every_n=every_n, ncols=ncols, y=y,
            fitted_projs=None, **kwargs)
    plt.suptitle('alpha')
    plt.tight_layout()
    plot_over_time(
        model, which_var='ksi', every_n=every_n, ncols=ncols, y=y,
        fitted_projs=fitted_projs, **kwargs)
    plt.suptitle('ksi')
    plt.tight_layout()


def plot_sv_over_time(model, which_var, every_n=3, ncols=5, y=None,
                   fitted_projs=None, n_components=None, **kwargs):
    ksi, epochs = get_LDMNet_var(model, which_var)
    nrows = len(ksi)//(ncols*every_n) + 1*(len(ksi)%(ncols*every_n) > 0)
    fitted_projs_list = []
    plt.figure(figsize=(4*ncols,3*nrows))
    for plotn, i in enumerate(range(0,len(ksi),every_n)):
        plt.subplot(nrows, ncols, plotn+1)
        title = "epoch=%d " % epochs[i]
        if fitted_projs is None:
            if n_components is None:
                n = min(ksi[i].shape)
            else:
                n = n_components
            proj = PCA(n_components=n, random_state=0).fit(ksi[i])
            fitted_projs_list.append(proj)
        else:
            proj = fitted_projs[plotn]
        plt.title(title)
        plt.plot(proj.singular_values_/proj.singular_values_[0], 'o:')
        plt.xlabel('principal components', fontsize='small')
#        plt.xticks(fontsize='small')
#        plt.yticks(fontsize='small')
    plt.tight_layout()
    return fitted_projs_list


def plot_over_time(model, which_var, every_n=3, ncols=5, y=None,
                   fitted_projs=None, **kwargs):
    ksi, epochs = get_LDMNet_var(model, which_var)
    nrows = len(ksi)//(ncols*every_n) + 1*(len(ksi)%(ncols*every_n) > 0)
    fitted_projs_list = []
    plt.figure(figsize=(3.8*ncols,2.5*nrows))
    for plotn, i in enumerate(range(0,len(ksi),every_n)):
        plt.subplot(nrows, ncols, plotn+1)
        title = "epoch=%d " % epochs[i]
        if ksi[i].shape[1] > 2:
            if fitted_projs is None:
                proj = PCA(n_components=2, random_state=0).fit(ksi[i])
                fitted_projs_list.append(proj)
                plt.xlabel('principal component 1')
                plt.ylabel('principal component 2')
            else:
                proj = fitted_projs[plotn]
            ksi2d = proj.transform(ksi[i])
            title += " var: %0.1f %0.1f" % tuple(proj.explained_variance_ratio_[0:2])
        else:
            ksi2d = ksi[i]
            plt.xlabel('coordinate 1')
            plt.ylabel('coordinate 2')
        plt.title(title, fontsize='small')
        if y is not None:
            plt.scatter(*ksi2d.T, c=y, **kwargs)
        else:
            plt.scatter(*ksi2d.T, **kwargs)
        plt.xticks(fontsize='small')
        plt.yticks(fontsize='small')
    return fitted_projs_list


def plot_f_df(Z, title, smooth_delta=None, time=None, **kwargs):
    plt.figure(figsize=(10,4))
    plt.subplot(1,2,1)
    plt.title(title)
    for feature in Z:
        if feature.ndim > 1:
            # mean over samples
            feature = feature.mean(axis=0)
        if time is None:
            plt.plot(feature, **kwargs)
        else:
            plt.plot(time, feature)

    Z = np.diff(Z, axis=-1)
    plt.subplot(1,2,2)
    plt.title("delta "+ title)
    for feature in Z:
        if feature.ndim > 1:
            feature = feature.mean(axis=0)
        if smooth_delta is not None and smooth_delta != 0:
            if type(smooth_delta) is float:
                smooth_delta = int(smooth_delta*feature.size)
            window = np.ones(smooth_delta)
            feature = np.convolve(feature, window/window.sum(), mode='same')
        if time is None:
            plt.plot(feature, **kwargs)
        else:
            plt.plot(time[:-1], feature)


def plot_ldmm_vars(model, var_name='Z', **kwargs):
    Z, epochs = get_LDMNet_var(model, var_name)
    # Z is samples x features x time
    Z = np.stack(Z, axis=-1)
    # now it is features x samples x time
    Z = Z.transpose([1,0,2])
    plot_f_df(Z, var_name, time=epochs, **kwargs)

def plot_net_weights(model, par_name, **kwargs):
    """
    model should have a SaveWeights callback
    Par name is layer name
    """
    saver = dict(model.callbacks_)['SaveWeights']
    params = saver.params
    epochs = saver.epochs
    par_names = [par_name + suffix for suffix in ['.weight', '.bias']]
    for par_name in par_names:
        if par_name in params[0]:
            W = []
            for p_t in params:
                W.append(p_t[par_name])
            # W is out x in x time
            W = np.stack(W, axis=-1)
            plot_f_df(W, 'W ' + par_name, time=epochs, **kwargs)
