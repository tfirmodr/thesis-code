#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Oct 25 14:29:40 2018

@author: thalita
"""

import numpy as np
from sklearn.cluster.spectral import discretize
from sklearn.cluster import KMeans
from scipy.stats import mode
from skorch_utils import NNClassifier, SaveWeights
from skorch.utils import to_tensor
from skorch.callbacks import Callback
import torch
import torch.nn as nn

import matplotlib.pyplot as plt

from laplacian_utils import compute_W, compute_L, smalestSVD, gap, plot_graph


class SProtoRegNet(NNClassifier):
    def __init__(self,
                 module,
                 reg=0.001,
                 criterion=torch.nn.CrossEntropyLoss,
                 layer_name='hidden1',
                 **kwargs):
        super().__init__(module, criterion=criterion, **kwargs)
        self.reg = reg
        self.layer_name = layer_name

    @property
    def _default_callbacks(self):
        return super()._default_callbacks + \
            [('ProtoUpdate', ProtoUpdate(layer_name=self.layer_name))]

    def initialize(self):
        super().initialize()
        self.protos = None
        self.protos_class = None
        return self

    def initialize_module(self):
        super().initialize_module()
        for name, param in self.module_.named_parameters():
            if name.endswith(self.layer_name+'.weight'):
                nn.init.eye_(param)
                nn.init.xavier_normal_(param)
            elif name.endswith('weight'):
                nn.init.xavier_normal_(param)
            elif name.endswith('bias'):
                nn.init.zeros_(param)

    def get_loss(self, y_pred, y_true, X=None, training=False):
        loss = super().get_loss(y_pred, y_true, X, training)
        # testing margin loss
        # y_pred = nn.functional.softmax(y_pred, dim=1)
        # loss = nn.functional.multi_margin_loss(y_pred, y_true)
        if X is not None and self.reg:
            # n_classes = self.module_.output.weight.size(0)
            ksi = self.infer(X, name=self.layer_name)
            n_protos, n_features = self.protos.size()
            n_samples, _ = ksi.size()

            # create similarity tensor
            dist = torch.empty(n_samples, n_protos)
            for ix, p in enumerate(self.protos):
                dist[:, ix] = torch.sum((ksi-p)**2, dim=1)

            # sim = torch.nn.functional.softmax(-dist, dim=1)
            # proto2class = torch.zeros((n_protos, n_classes),
            #                           device=self.device)
            # proto2class.scatter_(1, self.protos_class.view(-1,1), 1)
            # proto_class_pred = torch.mm(sim, proto2class)
            # reg_loss = torch.nn.CrossEntropyLoss()
            # reg_loss = reg_loss(proto_class_pred, y_true)

            # version with sorting in case i need it later
            # sorted_dist, idx = torch.sort(dist, dim=1)
            #
            # # gather classes for protos in order,
            # # from the closest to the farthest
            # # for each mini batch sample
            # samples_proto_class = torch.empty(n_samples, n_protos)
            # for sample in range(n_samples):
            #     samples_proto_class[sample, :] = \
            #         torch.gather(input=self.protos_class, dim=0,
            #                      index=idx[sample])
            # # only distances to wrong protos will enter the loss
            # bad_proto_mask = samples_proto_class.to(
            #     torch.long) != y_true.view(-1, 1)
            # dist2good = sorted_dist[:, 0]

            # version without sorting
            # gather classes for protos in order,
            # from the closest to the farthest
            # for each mini batch sample
            samples_proto_class = torch.empty(n_samples, n_protos)
            for sample in range(n_samples):
                samples_proto_class[sample, :] = self.protos_class

            # only distances to wrong protos will enter the loss
            bad_proto_mask = samples_proto_class.to(
                torch.long) != y_true.view(-1, 1)

            dist2bad = torch.mean(dist * bad_proto_mask.to(torch.float), dim=1)
            reg_loss = - (dist2bad).mean()  # dist2good.mean()
            loss = (1-self.reg) * loss + self.reg * reg_loss
           # loss = reg_loss

        return loss


class ProtoUpdateBase(object):
    def _cat_ksi(self, ksi,  X, X_imgs=None):
        '''
        X : samples (images or features)
        X_imgs : input images (in case X contains pre-extracted features)
        '''
        if self.concatenate_input:
            input = X if X_imgs is None else X_imgs
            return np.concatenate([ksi, input.view(input.shape[0],-1)],
                                   axis=-1)
        else:
            return ksi

    def _update_W_L(self, ksi, X, X_imgs=None):
        cat_ksi = self._cat_ksi(ksi, X, X_imgs=X_imgs)
        self.W_ = compute_W(cat_ksi, self.n_neighbors,
                            nn_radius=self.nn_radius)
        self.Ln_ = compute_L(self.W_, normalized=True, return_diag=False)

    def _update_protos(self, net, ksi, y,  X=None):
        # Reuse previous protos if this is not the first update
        # valid only if using k-means
        if self.emb_protos is None:
            kmeans_init = 'k-means++'
            kmeans_n_init = 10
        else:
            # XXX problematic if the number of protos changes
            # from one iter to the other
            kmeans_init = self.emb_protos
            kmeans_n_init = 1
        # Compute neighbohhood graph and Laplacian
        W = self.W_
        Ln = self.Ln_
        # Smalest eigen vectors of Laplacian matrix
        if self.last_svd is None:
            svdLn = smalestSVD(Ln, k=self.n_components,
                               method='arpack',
                               random_seed=self.random_seed)
        else:
            _, vec0 = tuple(self.last_svd)
            svdLn = smalestSVD(
                Ln, k=self.n_components,
                vec0=vec0,
                method=self.svd_method,
                maxiter=self.max_iter if self.svd_method == 'lobpcg' else None,
                random_seed=self.random_seed)
        self.last_svd = svdLn
        # XXX gap sometimes gives a very large number of clusters
        #     which is not intersting and causes problems
        if self.n_clusters == 'gap':
            n_clusters = gap(svdLn.singular_values_, self.gap_lim)
        else:
            n_clusters = self.n_clusters
        # use first n_clusters components for clustering
        embedding = svdLn.singular_vectors_[:, :n_clusters]
        if self.cluster_assignment == 'discretize':
            clusters = discretize(embedding, copy=False,
                                  random_state=self.random_seed)
        elif self.cluster_assignment == 'kmeans':
            kmeans = KMeans(n_clusters, init=kmeans_init, n_init=kmeans_n_init,
                            random_state=self.random_seed)
            kmeans.fit(embedding)
            clusters = kmeans.labels_

        # find center in the embedding space, take closest sample as prototype
        protos = []
        protos_class = []
        self.emb_protos = []
        for i in range(n_clusters):
            cluster_mask = (clusters == i)
            if np.any(cluster_mask):
                # get cluster classes
                y_masked = y[cluster_mask]
                cluster_classes, class_count = mode(y_masked)
                class_freq = class_count/class_count.max()
                most_frequent = class_freq >= self.split_proto_threshold
                cluster_classes = cluster_classes[most_frequent]
                for subclass in cluster_classes:
                    protos_class.append(subclass)
                    class_mask = y_masked == subclass
                    # get embedding points from this cluster and from this class
                    emb_masked = embedding[cluster_mask][class_mask]
                    if self.cluster_assignment == 'discretize':
                        emb_center = emb_masked.mean(axis=0)
                    elif self.cluster_assignment == 'kmeans':
                        emb_center = kmeans.cluster_centers_[i]
                    emb_dist = np.linalg.norm(emb_masked-emb_center, axis=1)
                    self.emb_protos.append(emb_center)
                    # take closest sample of the most voted class as protoype
                    center_id = np.argmin(emb_dist)
                    center = ksi[cluster_mask][class_mask][center_id]
                    protos.append(center)

        self.emb_protos = np.array(self.emb_protos)
        protos = np.array(protos)
        protos_class = np.array(protos_class)
        net.protos = to_tensor(protos.astype(X.dtype),
                               device=net.device)
        net.protos_class = to_tensor(protos_class.astype(y.dtype),
                                     device=net.device)
        plots = 4
        plt.subplot(1, plots, 1)
        plot_graph(X, y, W)
        plt.xlabel('X')
        plt.subplot(1, plots, 2)
        plot_graph(ksi, y, W)
        plt.xlabel('ksi\n%d prototypes' % protos.shape[0])
        plt.scatter(*protos.T, s=50, c=protos_class,
                    marker='d', edgecolors='r')
        plt.subplot(1, plots, 3)
        plt.ylabel('Lnormalized eigenvalues')
        plt.plot(svdLn.singular_values_[:2*n_clusters], 'o:')
        plt.xlabel('n_clusters = %d' % n_clusters)
        plt.title('eigval sum: %0.2f' % np.abs(svdLn.singular_values_).sum())
        plt.subplot(1, plots, 4)
        plot_graph(embedding[:, :2], y, W)
        plt.scatter(*self.emb_protos[:, :2].T, s=50,
                    c=protos_class, marker='d', edgecolors='r')
        plt.xlabel('laplacian embedding\n%d prototypes' % protos.shape[0])
        plt.gcf().set_size_inches(15, 4)
        plt.show()
        diff_class_mask = np.zeros(W.shape, dtype=bool)
        for i, line_i in enumerate(W):
            for j in line_i.indices:
                diff_class_mask[i, j] = diff_class_mask[j, i] = (y[i] != y[j])
                if j + 1 >= i:
                    break

        print("similarities",
              "max btw diff class:",
              W[diff_class_mask].max() if diff_class_mask.any() else '--',
              "min:", W.data.min(), "max:", W.max())

class ProtoUpdate(Callback, ProtoUpdateBase):
    def __init__(self, layer_name='hidden1',
                 split_proto_threshold=0.4,
                 n_neighbors=20, nn_radius=10, n_components=50,
                 n_clusters='gap', cluster_assigmnent='discretize',
                 svd_method='arpack',
                 gap_lim=0.01,
                 tol=1e-4, max_iter=200, n_jobs=1,
                 concatenate_input=True,
                 random_seed=None,
                 epochs_update=2,
                 **kwargs):
        self.svd_method = svd_method
        self.random_seed = random_seed
        self.layer_name = layer_name
        self.concatenate_input = concatenate_input
        self.epochs_update = epochs_update
        self.split_proto_threshold = split_proto_threshold
        self.n_clusters = n_clusters
        self.gap_lim = gap_lim
        self.n_jobs = n_jobs
        self.tol = tol
        self.max_iter = max_iter
        self.n_neighbors = n_neighbors
        self.nn_radius = nn_radius
        self.n_components = n_components
        self.cluster_assignment = cluster_assigmnent

    def initialize(self):
        super().initialize()
        self.emb_protos = None
        self.last_svd = None
        self.last_update_ = 0
        return self

    def on_epoch_begin(self, net,
                       dataset_train=None, dataset_valid=None, **kwargs):
        epochs = len(net.history)
        if not (epochs-1) % self.epochs_update:
            X, y = dataset_train[:]
            ksi = net.transform(X, name=self.layer_name)
            self._update_W_L(ksi, X, X_imgs=None)
            self._update_protos(net, ksi, y, X)

class SaveVars(Callback):
    """ Callback to save ksi, alpha and Z"""

    def __init__(self, every_n_epochs=100):
        self.every_n_epochs = every_n_epochs

    def initialize(self):
        # self.ksi = []
        self.protos = []
        self.protos_class = []
        self.last_save = 0
        self.epochs = []
        return self

    def on_epoch_end(self, net, **kwargs):
        """
        save vars
        """
        epochs = len(net.history)
        if epochs - self.last_save >= self.every_n_epochs:
            # self.ksi.append(net.ksi)
            self.protos.append(net.protos.cpu().numpy())
            self.protos_class.append(net.protos_class.cpu().numpy())
            self.last_save = epochs
            self.epochs.append(epochs - 1)

# if __name__ == '__main__':
#    from skorch_utils import MLP, NNClassifier
#    from sklearn.datasets import make_moons
#    random_seed = 0
#    X, y = make_moons(n_samples=500, noise=0.1, random_state=random_seed)
#    X, y = X.astype(np.float32), y.astype(np.int64)
#    callbacks = [SaveVars(1)]
#    torch.manual_seed(random_seed)
#    net = SProtoRegNet(
#            MLP,
#            module__num_units=[2],
#            module__n_out=2,
#            module__drop_proba=0,
#            lr=1e-2,
#            callbacks=callbacks,
#            callbacks__ProtoUpdate__epochs_update=0)
#    net.fit(X, y)
