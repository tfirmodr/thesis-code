
# coding: utf-8

# # Cifar 10

# In[1]:


from Cifar10InceptionV3ClusterAndClassify import gen_cifar10, spliters
import numpy as np
import matplotlib.pyplot as plt

from skorch_utils import MLP, LRScheduler, SaveWeights, EarlyStopping, NaNStopping
from LDMM import LDMNetSkorch, SaveLDMNetVars

import torch
import os
import pandas as pd

from sklearn.model_selection import RandomizedSearchCV, StratifiedShuffleSplit
from scipy.stats import uniform, randint, reciprocal, norm


import argparse

parser = argparse.ArgumentParser()
parser.add_argument('--n_hparams', default=100, type=int)
parser.add_argument('--max_epochs', default=500, type=int)
parser.add_argument('--cuda', action='store_true', default=False)
parser.add_argument('-o', '--overwrite', action='store_true', default=False)
parser.add_argument('--n_jobs', default=3, type=int)
parser.add_argument('--train_size', nargs='+', default=[200,500], type=int)

args = parser.parse_args()
n_hparams = args.n_hparams
max_epochs= args.max_epochs


random_state=0
np.random.seed(random_state)


# In[2]:


X_imgs, X, y = gen_cifar10(plot=False)
X = X.astype(np.float32)
y = y.astype(np.int64)


# In[3]:


X.shape, y.shape, X_imgs.shape


# In[4]:


torch.manual_seed(random_state)
module = MLP(n_in=X.shape[1], num_units=(2048), n_out=10, drop_proba=0)
model = LDMNetSkorch(
    module,
    layer_name='hidden1',
    batch_size=200,
    max_epochs=max_epochs,
    lr=0.005,
    mu=1e-3,
    lambda_bar=0.01,
    use_cuda=args.cuda,
    verbose=0,
    callbacks=[
        EarlyStopping(20, 'valid_loss'),
        NaNStopping(),
        LRScheduler('ReduceLROnPlateau'),
        SaveWeights(every_n_epochs=20),
        SaveLDMNetVars(every_n_epochs=20)]
)


# In[5]:



params = {
    'lr': norm(7e-3, 1e-3),
    'mu': uniform(1e-5, 1),
    'lambda_bar': uniform(1e-3, 10)
}


BASE_DIR = 'Cifar10LDMNetHparams/%d_hparams_%d_max_epochs_train_size_%d/'
FILE = 'save.pkl'

saves = []
for train_size in args.train_size:
    skf, skf_cv = spliters(random_state, train_size=train_size)
    search = RandomizedSearchCV(
        model, n_iter=n_hparams, n_jobs=args.n_jobs, refit=True,
        cv=StratifiedShuffleSplit(1, test_size=0.2, random_state=random_state),
        param_distributions=params, verbose=1)
    DIR = BASE_DIR % (n_hparams, max_epochs, train_size)
    os.makedirs(DIR, exist_ok=True)
    fname = os.path.join(DIR, FILE)
    if not os.path.exists(fname) or args.overwrite:
        train, test = next(skf.split(X,y))
        search.fit(X[train], y[train], X_imgs[train].reshape(train.size,-1))
        saved = {'search': search, 'split' :(train,test)}
        pd.to_pickle(saved, fname)
        print('saved!')
    else:
        saved = pd.read_pickle(fname)
    saves.append(saved)


# In[6]:


for saved in saves:
    train, test = saved['split']
    search = saved['search']
    model = search.best_estimator_
    print("best params ", search.best_params_)
    print("test accuracy @ ", train.size, ": ",model.score(X[test],y[test]))

