#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Jun  2 11:02:35 2018

@author: thalita
"""

from prototype_runs import Experiment
import argparse

def parser():
    desc = "mains script to run experiments for the prototypes model"
    parser = argparse.ArgumentParser(description=desc)
    parser.add_argument('--exp', default='custom',
                        help='train_size or episodes')
    parser.add_argument('--overwrite', nargs='+', help='models to re-run')
    parser.add_argument('--model_list', nargs='+', help='model name list')
    parser.add_argument('--dataset', help='dataset name')
    parser.add_argument('--path')
    parser.add_argument('--train_size', type=int)
    parser.add_argument('--n_splits', type=int, default=1)
    parser.add_argument('--n_classes', type=int, default=10)
    parser.add_argument('--n_episodes', type=int)
    parser.add_argument('--random_seed', type=int, default=0)
    parser.add_argument('--n_jobs', type=int, default=2,
                        help='n_jobs for grid search')

    return parser

if __name__ == '__main__':
    p = parser()
    args = p.parse_args()
    kwargs = dict(vars(args))
    del kwargs['exp']
    del kwargs['overwrite']

    if args.exp == 'train_size':
        assert args.dataset is not None
        del kwargs['path']
        ex = Experiment(
                path=args.dataset, **kwargs)
        ex.run_multiple_splits(args.overwrite)

    elif args.exp == 'episodes':
        assert args.n_episodes is not None
        assert args.n_classes is not None
        del kwargs['path']
        del kwargs['dataset']
        del kwargs['train_size']
        ex = Experiment(
                path='omniglot_episodes',
                dataset='omniglot',
                train_size=0.5,
                **kwargs)
        ex.run_multiple_class_subsets(args.overwrite)
    else:  # custom
        ex = Experiment(**kwargs)
        if args.dataset == 'omniglot' and args.n_episodes is not None:
            ex.run_multiple_class_subsets(args.overwrite)
        elif args.dataset is not None and args.train_size is not None:
            ex.run_multiple_splits(args.overwrite)
