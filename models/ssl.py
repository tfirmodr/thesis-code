#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun 22 16:45:49 2017

@author: thalita

SSL

"""

import tensorflow as tf
import tensorflow.contrib.layers as layers


def group_l1_regularizer(axis_group, strengh, scope=None):
    """
    axis_groups: list contains a set of axis which will be
    grouped through an l2 norm before applying l1

    strengh: l1 penaly weight
    """
    scope = scope or 'grouped_l1'
    reg = layers.l1_regularizer(strengh, scope=scope)

    def group_l1(weights, name=None):
        group = axis_group
        # calculate L2 norm of each group
        # reduce sum of squares on all but "dim" axis
        grouped = tf.sqrt(tf.reduce_sum(weights**2, axis=group))
        # L1 criteria over axis "dim"
        return reg(grouped)

    return group_l1


def ssl_regularizer(
        filter_wise=0,
        channel_wise=0,
        shape_wise=0,
        depth_wise=0):
    """
    Implementation of SSL regularization (group L1)
    Arguments are: option=strenght
    options are: fitler_wise, channel_wise, shape_wise, depth_wise
    """
    regularizers = []

    if shape_wise:
        # group in channels, height and width
        reg = group_l1_regularizer([0, 1, 2], shape_wise)
        regularizers.append(reg)
    if filter_wise:
        # number of output filters
        reg = group_l1_regularizer([3], filter_wise)
        regularizers.append(reg)
    if channel_wise:
        # number of input channels used for an output feature
        reg = group_l1_regularizer([2], channel_wise)
        regularizers.append(reg)
    if depth_wise:
        reg = group_l1_regularizer([0, 1, 2, 3], channel_wise)
        regularizers.append(reg)

    return layers.sum_regularizer(regularizers)
