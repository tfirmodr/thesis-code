#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Feb 24 18:23:22 2017

@author: thalita

Compute invariants on a batch of images

"""

import tensorflow as tf
import numpy as np
from skimage.filters.edges \
    import (EROSION_SELEM, HSOBEL_WEIGHTS, VSOBEL_WEIGHTS, HSCHARR_WEIGHTS,
            VSCHARR_WEIGHTS, HPREWITT_WEIGHTS, VPREWITT_WEIGHTS,
            ROBERTS_PD_WEIGHTS, ROBERTS_ND_WEIGHTS)
from skimage.filters import gabor_kernel
from skimage.restoration.uft import laplacian as laplacian_k

from .view import view_activations

GRAD_FLAVORS = ['sobel', 'scharr', 'prewitt']


def get_n_invariants():
    global gradient_funcs, other_funcs
    # image, 3 types of gradients * (1s and 2nd gradients + grad_based funcs)
    # + other funcs
    return 1 + (len(gradient_funcs)) + len(other_funcs)


def _kwargs_dict(**kwargs):
    return kwargs


def squared_gradients(images):
    Gx = grad_x(images)
    Gy = grad_y(images)
    G2 = norm_grad(images, 'sobel', precomp_grads={'x':Gx, 'y':Gy})
    Gx2 = tf.square(Gx, name='Gx2')
    Gy2 = tf.square(Gy, name='Gy2')
    GxGy = tf.multiply(Gx, Gy, name='GxGy')
    return G2, Gx2, Gy2, GxGy

def compute_invariants(scope_name, images):
    """
    Return tensor with computed invariants for images
    Shape batch_size, h, w, channels*n_invariants
    """
    global gradient_funcs, other_funcs
    with tf.variable_scope(scope_name):
        invariants = [images]
        for flavor in ['sobel']:
            # since gradients are used for all functions
            # precompute them only once
            G_x, G_y, G_xx, G_yy, G_xy = all_grads(images, flavor)
            precomp_grads = _kwargs_dict(x=G_x, y=G_y,
                                         xx=G_xx, yy=G_yy, xy=G_xy)
            #invariants += list(precomp_grads.values())
            out = [f(images, precomp_grads=precomp_grads, flavor=flavor)
                   for f in gradient_funcs]
            invariants += out
        invariants += [f(images) for f in other_funcs]
        invariants = tf.concat(invariants, axis=-1, name='invariants')
        tf.assert_rank(invariants, 4, data=invariants.get_shape())
        return invariants

def _kernel2constant(op_k):
    # convert np.ndarray kernel to tf constant
    op_k = op_k.reshape(list(op_k.shape) + [1, 1])
    op_k = tf.constant(op_k, dtype=tf.float32)
    return op_k

def _apply_kernel(images, op_k, name):
    # convert np.ndarray kernel to tf constant
    op_k = _kernel2constant(op_k)
    # get number of channels in images
    in_channels = images.get_shape()[-1].value
    # create filter 4D tensor repeating the 2D kernel
    # for all channels  (channel_multiplier=1)
    tiling_size = [1, 1, in_channels, 1]
    op_k = tf.tile(op_k, tiling_size)
    # channel separated convolution (hence the depthwise version of conv2d)
    output = \
        tf.nn.depthwise_conv2d(images, op_k,
                               strides=[1, 1, 1, 1], padding='SAME', name=name)
    tf.assert_rank(output, 4, data=output.get_shape())
    return output


def _set_grad_flavor(flavor='sobel'):
    if flavor == 'scharr':
        g_x = HSCHARR_WEIGHTS
        g_y = VSCHARR_WEIGHTS
    elif flavor == 'prewitt':
        g_x = HPREWITT_WEIGHTS
        g_y = VPREWITT_WEIGHTS
    else:  # default to sobel
        g_x = HSOBEL_WEIGHTS
        g_y = VSOBEL_WEIGHTS
    return g_x, g_y


def grad_x(images, flavor=None):
    g_x, g_y = _set_grad_flavor(flavor)
    return _apply_kernel(images, g_x, name='G_x')


def grad_y(images, flavor=None):
    g_x, g_y = _set_grad_flavor(flavor)
    return _apply_kernel(images, g_y, name='G_y')


def second_grads(images, flavor=None):
    g_x, g_y = _set_grad_flavor(flavor)
    g_xx = g_x**2
    g_yy = g_y**2
    g_xy = g_x*g_y
    G_xx = _apply_kernel(images, g_xx, name='G_xx')
    G_yy = _apply_kernel(images, g_yy, name='G_yy')
    G_xy = _apply_kernel(images, g_xy, name='G_xy')
    return G_xx, G_yy, G_xy


def all_grads(images, flavor='sobel'):
    G_x = grad_x(images, flavor)
    G_y = grad_y(images, flavor)
    G_xx, G_yy, G_xy = second_grads(images, flavor)
    return G_x, G_y, G_xx, G_yy, G_xy


def laplacian(images, **kwargs):
    """
    Compute the 2D laplacian of a batch of images
    (Discrete aproximation)
    """
    # get a 2D kernel from skimage.restoration.utf.laplacian,
    # with ksize=(3,3)
    _, op_k = laplacian_k(2, (3, 3))
    return _apply_kernel(images, op_k, name='laplacian')


def selem_erosion(images):
    return _apply_kernel(images, EROSION_SELEM,
                         name='erosion')


def roberts_pos(images):
    return _apply_kernel(images, ROBERTS_PD_WEIGHTS,
                         name='pos_diag')


def roberts_neg(images):
    return _apply_kernel(images, ROBERTS_ND_WEIGHTS,
                         name='neg_diag')


def norm_grad(images, flavor=None, precomp_grads=None):
    """
    Compute the 2D  gradient of a batch of images
    (Discrete gradient aproximation)
    If flavor is none, defaults to Sobel-Feldman operator
    In case the image has multiiple channels, each one is treated independently
    """
    if precomp_grads is None:
        G_x = grad_x(images, flavor)
        G_y = grad_y(images, flavor)
    else:
        G = precomp_grads
        G_x, G_y = G['x'], G['y']

    return tf.add(tf.square(G_x), tf.square(G_y), name=flavor)


def local_unflatness(images, precomp_grads=None, flavor=None):
    if precomp_grads is None:
        G_xx, G_yy, G_xy = second_grads(images, flavor)
    else:
        G = precomp_grads
        G_xx, G_yy, G_xy = G['xx'], G['yy'], G['xy']
    return tf.add_n([tf.square(G_xx), tf.square(G_yy),
                     2 * G_xy], name='unflatness')


def curvature(images, precomp_grads=None, flavor=None):
    if precomp_grads is None:
        G_x, G_y, G_xx, G_yy, G_xy = all_grads(images, flavor)
    else:
        G = precomp_grads
        G_x, G_y, G_xx, G_yy, G_xy = G['x'], G['y'], G['xx'], G['yy'], G['xy']
    return tf.add_n([tf.square(G_x) * G_xx, tf.square(G_y) * G_yy,
                     2 * G_x * G_y * G_xy], name='curvature')


other_funcs = [laplacian, selem_erosion, roberts_pos, roberts_neg]
gradient_funcs = [norm_grad, local_unflatness, curvature]
