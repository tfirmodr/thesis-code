"""

@author: thalita

Variable summaries, view activations, kernels and parameters

"""

try:
    import matplotlib.pyplot as plt
except:
    pass
import numpy as np
import tensorflow as tf


def variable_summaries(var):
    """
    Copied from TF doc
    Attach a lot of summaries to a Tensor (for TensorBoard visualization).
    """
    mean = tf.reduce_mean(var)
    tf.summary.scalar('mean', mean)
    with tf.name_scope('stddev'):
        stddev = tf.sqrt(tf.reduce_mean(tf.square(var - mean)))
    tf.summary.scalar('stddev', stddev)
    tf.summary.scalar('max', tf.reduce_max(var))
    tf.summary.scalar('min', tf.reduce_min(var))
    tf.summary.histogram('histogram', var)


def plot_sample_images(images, labels):
    plt.figure()
    plt.subplot(1,2,1)
    plt.title('sample image')
    plt.imshow(images[0, :, :, :], interpolation='none')
    plt.subplot(1,2,2)
    plt.title('sample labels')
    plt.imshow(labels[0, :, :], interpolation='none')


def view_activations(conv, name='activation'):
    with tf.name_scope("summary"):
        # for summary visualization
        _, height, width, n_filters = conv.get_shape().as_list()
        # slice first image
        view = tf.slice(conv, (0, 0, 0, 0), (1, -1, -1, -1))  # V[0,...]
        # eliminate batch dimension
        view = tf.reshape(view, (height, width, n_filters))
        # add a black border
        border = max(int(0.1 * width), 1)
        height += border
        width += border
        view = tf.image.resize_image_with_crop_or_pad(view,
                                                      height, width)
        # define number of rows and columns for display grid
        # start with sqrt then subtract until we get an integer divider
        rows = int(np.sqrt(n_filters))
        while n_filters % rows:
            rows -= 1
        cols = n_filters // rows
        # redistribute filters in the two new dimensions
        view = tf.reshape(view, (height, width, rows, cols))
        # swap axis order to rows, height, cols, width
        view = tf.transpose(view, (2, 0, 3, 1))
        # reshape to 2D
        view = tf.reshape(view, [1, rows * height, width * cols, 1])
    tf.summary.image(name, view)


def view_kernel(kernel):
    """
    View kernels for first output channel.
    Kernel is shape Height, width, in_filters, out_filters
    """
    with tf.name_scope("summary"):
        view = tf.transpose(kernel, [3, 0, 1, 2])
    view_activations(view, 'first_output_kernels')


def view_param_k(K):
    """
    K is a tensor [ batch, HEIGH, WIDTH, n_in, n_out, 3*3]
    build image [height*(3+border), width*(3+border)] from
    kernels connecting 1st output to 1st channel of 1st image
    """

    K = K[0,:,:,0,0,:]
    height, width, _ = K.get_shape().as_list()
    # add a black border
    K = tf.reshape(K, [height*width, 3, 3])
    K = tf.transpose(K, [1 ,2, 0])
    border = 1
    k_size = 3 + border
    K = tf.image.resize_image_with_crop_or_pad(K, k_size, k_size)
    # back to height*width, k_size, k_size
    K = tf.transpose(K, [2, 0 ,1])
    K = tf.reshape(K, [height, width, k_size, k_size])
    # swap axis order to height, k_size, width, k_size,
    K = tf.transpose(K, [0, 2, 1, 3])
    K = tf.reshape(K, [1, height * k_size, width * k_size, 1])
    tf.summary.image('param_k', K)

