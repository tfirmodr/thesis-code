from .multiscale_cnn import MultiscaleConvNet
from .vgg import VGG
from .multiconv import MultiConv
from .fullmulticonv import FullMultiConv
from .resnet import ResNet
from .few_shot_prototypes import FewShotPrototypes
from .few_shot_episodical_cnn import EpisodicalCNN

_models_dict = dict(
        multiscale_cnn=MultiscaleConvNet,
        vgg=VGG,
        multiconv=MultiConv,
        fullmulticonv=FullMultiConv,
        resnet=ResNet,
        fewshotprototypes=FewShotPrototypes,
        fewshotcnn=EpisodicalCNN)


def get_by_name(name):
    return _models_dict[name]
