#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May 10 15:59:02 2017

@author: thalita

Multi convolution + vgg

"""
import tensorflow as tf
import tensorflow.contrib.layers as layers
from tensorflow.contrib.learn import Estimator, ModelFnOps
import tensorflow.contrib.slim as slim
from tensorflow.contrib.slim import nets
import numpy as np

from .vgg import VGG, RestoreHook
from .training import set_training
from .view import view_activations, view_kernel


vgg = nets.vgg


class MultiConv(VGG):
    default_params = dict(VGG.default_params)
    default_params.update(
            weights='imagenet',
            regL1=1e-3)
    default_params['optimizer_params'].update(learning_rate=1e-4, momentum=0.9)


    def __init__(self, model_dir, params=None, config=None):
        Estimator.__init__(self, model_fn=MultiConv._model_fn,
                           model_dir=model_dir,
                           config=config,
                           params=params,
                           feature_engineering_fn=VGG._feature_engineering_fn)

    def _model_fn(X, y, mode, params, config=None):
        if params is None:
            params = MultiConv.default_params()

        is_training = False if mode == 'predict' else True
        with slim.arg_scope(vgg.vgg_arg_scope(params['regL2'])):
            kwargs = dict(params)
            del kwargs['regL2']
            del kwargs['weights']
            del kwargs['optimizer']
            del kwargs['optimizer_params']
            del kwargs['tv_loss']

            net, _ = MultiConv._vgg_net(
                inputs=X['X'],
                is_training=is_training,
                **kwargs)
            # check whether to reload imagenet weights
            # (available for VGG16 an 19 only)
            hooks = None
            if params['weights'] == 'imagenet':
                if params['variant'] in {16, 19}:
                    end_point = int(params['vgg_end_point'][-1:])
                    init_fn = MultiConv._get_weights(params['variant'],
                                                     up_to_layer=end_point)
                    hooks = [RestoreHook(init_fn)]
                else:
                    tf.logging.warning(
                        "This VGG variant has no pre-trained weights, "+
                        "falling back to random initialization.")


        y_pred_logits = tf.image.resize_images(net, y.get_shape()[1:])

        train_step, y_pred_labels = set_training(
                y, y_pred_logits,
                optimizer=params['optimizer'],
                optimizer_params=params['optimizer_params'])

        return ModelFnOps(mode=mode,
                          predictions=y_pred_labels,
                          loss=tf.losses.get_total_loss(),
                          train_op=train_step,
                          eval_metric_ops=None,
                          output_alternatives=None,
                          training_chief_hooks=None,
                          training_hooks=hooks,
                          scaffold=None)

    def _vgg_net(
            inputs,
            n_classes=1000,
            is_training=True,
            dropout_keep_prob=0.5,
            pooling_window=[2, 2],
            top='pixel-fc',
            vgg_end_point='conv5',
            regL1=1e-3,
            variant='a'):
        """Oxford Net VGG adapted to produce pixelwise classification.

        Based on tf.contrib.slim implementation

        Args:
            inputs: a tensor of size [batch_size, height, width, channels].
            num_classes: number of predicted classes.
            is_training: whether or not the model is being trained.
            dropout_keep_prob: the probability that activations are kept in
                the dropout layers during training.

            scope: Optional scope for the variables.
        Returns:
              the last op containing the log predictions and end_points dict.
        """
        scope = model_name = 'vgg_' + str(variant)

        # quantity of chained conv layers for each VGG variant
        repeats = {'a': [1, 1, 2, 2, 2],
                   16: [2, 2, 3, 3, 3],
                   19: [2, 2, 4, 4, 4]}
        channels = [64, 128, 256, 512, 512]

        with tf.variable_scope(scope, model_name, [inputs]) as sc:
            end_points_collection = sc.original_name_scope + '_end_points'
            # Collect outputs for conv2d, fully_connected and max_pool2d.
            with slim.arg_scope(
                    [layers.conv2d, layers.max_pool2d],
                    outputs_collections=end_points_collection):
                # iteratively build layers of vgg and stop at given end point
                rp = repeats[variant]
                net = inputs
                for l, ch in enumerate(channels):
                    scope = 'conv%d' % (l + 1)
                    net = layers.repeat(
                            net, rp[l], multiconv2d, ch, [3, 3],
                            scope=scope)

                    if l == 0:
                        view_activations(net, 'conv1')
                        for k in slim.get_variables_by_suffix('weights:0'):
                            with tf.name_scope('conv1'):
                                view_kernel(k)
                    if vgg_end_point == scope:
                        break

                    scope = 'pool%d' % (l + 1)
                    net = layers.max_pool2d(
                            net, pooling_window,
                            scope=scope)

                    if vgg_end_point == scope:
                        break


                kernel_sizes = [[1, 1], [3, 3], [5, 5]]

                net = multiconv2d(net, 1024, kernel_sizes, scope='multiconv6')
                net = sparse_combination(net, regL1, scope='sparse_combination')

                view_activations(net, 'multiconv6')

                net = multiconv2d(net, 1024, kernel_sizes, scope='multiconv7')
                net = sparse_combination(net, regL1, scope='sparse_combination',
                                         reuse=True)

                view_activations(net, 'multiconv7')

                if top == 'pixel-fc':
                    conv_shape = net.get_shape().as_list()[1:3]
                elif top == 'channel-fc':
                    conv_shape = [1, 1]

                def get_noise_shape(net):
                    """
                    noise_shape for dropout
                    noise_shape = [batch size, 1, 1, n_channels],
                    each batch and channel component will be kept independently
                    and each row and column will be kept or not kept together.
                    """
                    noise_shape = net.get_shape().as_list()
                    noise_shape[1] = noise_shape[2] = 1
                    return noise_shape

                if dropout_keep_prob > 0:
                    net = layers.dropout(
                        net, dropout_keep_prob, get_noise_shape(net),
                        is_training=is_training, scope='dropout7')

                net = layers.conv2d(
                      net,
                      n_classes, conv_shape,
                      activation_fn=None,
                      normalizer_fn=None,
                      scope='fc8')

                # Convert end_points_collection into a end_point dict.
                su = slim.utils
                end_points = su.convert_collection_to_dict(end_points_collection)

        return net, end_points


def multiconv2d(inputs,
                num_outputs,
                kernel_sizes=[[1, 1], [3, 3], [5, 5]],
                scope=None,
                **kwargs):
    """
    returns: 5-D tensor of 4-D feature maps stacked in the last dim
    """
    kernel_sizes = np.array(kernel_sizes).squeeze()
    if kernel_sizes.ndim == 1:
        return layers.conv2d(inputs, num_outputs, kernel_sizes,
                             scope=scope, **kwargs)
    else:
        convs = []
        for ix, ks in enumerate(kernel_sizes):
            bscope = scope + '/branch%d' % (ix + 1)
            branch = layers.conv2d(inputs, num_outputs, ks,
                                   scope=bscope, **kwargs)
            weights = slim.get_variables_by_suffix(bscope + '/weights:0')[0]
            with tf.name_scope(bscope):
                view_kernel(weights)
            convs.append(branch)
        convs = tf.stack(convs, axis=-1)
        return convs


def sparse_combination(convs,
                       regL1=1e-5,
                       reuse=False, scope=None):
    with tf.name_scope(scope + "/shape_pre_proc"):
        n, h, w, c, br = convs.get_shape().as_list()
        # transpose to n, ch, h, w, br
        convs = tf.transpose(convs, [0, 3, 1, 2, 4])
        convs = tf.reshape(convs, [n*c, h, w, br])

    init = tf.constant_initializer(1.0/br)
    comb = layers.conv2d(convs, 1, [1, 1], reuse=reuse,
                         biases_initializer=None,
                         weights_initializer=init,
                         weights_regularizer=layers.l1_regularizer(regL1),
                         scope=scope)
    with tf.name_scope(scope + "/shape_post_proc"):
        comb = tf.reshape(comb, [n, c, h, w])
        comb = tf.transpose(comb, [0, 2, 3, 1])

    # Only create summaries if instantiating new weights
    # (i.e. when called with reuse=false)
    if not reuse:
        weights = slim.get_variables_by_suffix(scope + '/weights:0')[0]
        with tf.name_scope(scope + "/summary"):
            branch_weights = tf.unstack(
                tf.squeeze(weights), name='branch_weights')
            for ix, b in enumerate(branch_weights):
                slim.summarize_tensor(b, 'branch%d/weight' % ix)
    return comb
