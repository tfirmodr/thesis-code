#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jun 23 14:04:27 2017

@author: thalita
"""
import tensorflow as tf
import tensorflow.contrib.framework as framework
import urllib
import os
import tarfile


def maybe_download_extract(filename, url, DATA_DIR):
    filepath = maybe_download(url, DATA_DIR)
    with tarfile.open(filepath, 'r:gz') as file:
        file.extract(filename, DATA_DIR)


def maybe_download(url, DATA_DIR):
    filename = url.split('/')[-1]

    if not tf.gfile.Exists(DATA_DIR):
        tf.gfile.MakeDirs(DATA_DIR)

    filepath = os.path.join(DATA_DIR, filename)
    if not tf.gfile.Exists(filepath):
        print('Downloading weights', filename)
        filepath, _ = urllib.request.urlretrieve(url, filepath)
        with tf.gfile.GFile(filepath) as f:
            size = f.size()
        print('Successfully downloaded', filename, size, 'bytes.')

    return filepath


class RestoreHook(tf.train.SessionRunHook):
    def __init__(self, init_fn):
        self.init_fn = init_fn

    def after_create_session(self, session, coord=None):
        if session.run(framework.get_or_create_global_step()) == 0:
            tf.logging.info('Restoring imagenet weights.')
            self.init_fn(session)
