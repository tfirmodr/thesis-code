#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Dec  9 17:26:07 2016

@author: thalita

TF functions instantiating the elements of the model
"""
from skimage.segmentation import felzenszwalb
import numpy as np
import tensorflow as tf
from .invariants import (get_n_invariants, compute_invariants, laplacian,
                        squared_gradients)
from .view import view_activations, view_kernel, view_param_k

activation_functions = dict(
    tanh=tf.nn.tanh,
    sigmoid=tf.nn.sigmoid,
    relu=tf.nn.relu,
    crelu=tf.nn.crelu,
    elu=tf.nn.elu,
    softplus=tf.nn.softplus,
    softsign=tf.nn.softsign)

def image_pyramid(images, image_shape, with_laplacian=False):
    """
    Constructs a set of pyramids from a given set of images
    Currently using a bilinear interpolation (tf.resize_mages)
    """
    HEIGHT, WIDTH = image_shape
    tf.summary.image('input_images', images)
    images = tf.nn.l2_normalize(images, [1, 2])
    T = lambda img : laplacian(img) if with_laplacian else img
    pyramid = [T(images)]
    for i in range(1, 3):
        image = tf.image.resize_images(images, [HEIGHT//(2**i), WIDTH//(2**i)],
            method=tf.image.ResizeMethod.BILINEAR)
        image = T(image)
        pyramid.append(image)
    return pyramid


def create_k_params(scope_name, in_filters, out_filters):
    """Instantiate params for variational kernels
    For now I fixed a quantity #out_filters of sets of params that is the same
    for every input channel
    In the traditional case we have in_channel * out_channel kernels
    """
    with tf.variable_scope(scope_name):
        params = tf.get_variable(
                'k_params', shape=[1, out_filters, 5],
                dtype=tf.float32,
                initializer=tf.random_normal_initializer(mean=0.5,
                                                         stddev=1e-1))
        tf.summary.histogram('k_params', params)

        bias = tf.get_variable(
                name='biases', shape=[out_filters],
                dtype=tf.float32, initializer=tf.constant_initializer(0.0))
        return params, bias


def create_inv_weights_bias(scope_name, in_filters, out_filters):
    with tf.variable_scope(scope_name):
        inv_weights = tf.get_variable(name='inv_weights',
            shape=[1, 1, in_filters, out_filters], dtype=tf.float32,
            initializer=tf.random_normal_initializer(stddev=1e-1),
                regularizer=None)
        bias = tf.get_variable(name='biases', shape=[out_filters],
            dtype=tf.float32, initializer=tf.constant_initializer(0.0))
        return inv_weights, bias


def create_kernel_bias(scope_name, in_filters, out_filters, half_feat=True):
    with tf.variable_scope(scope_name):
        kernel = tf.get_variable(name='weights',
            shape=[7, 7, in_filters, out_filters], dtype=tf.float32,
            initializer=tf.random_normal_initializer(stddev=1e-1))
        bias = tf.get_variable(name='biases', shape=[out_filters],
            dtype=tf.float32, initializer=tf.constant_initializer(0.0))
        if half_feat and not (scope_name.find('conv1') > -1 ):
            """
            To compose each feature map in the next layer,
            only 50% of the input feature maps are used.
            This is not acctual dropout, it is a fixed ramdomly picked wiring
            (or so it seems from the paper, section 5.1).
            """
            keep = []
            for _ in range(out_filters):
                choice = np.random.choice(in_filters, in_filters//2,
                                          replace=False)
                keep.append([1 if i in choice else 0 for i in range(in_filters)])
            keep = np.array(np.array(keep, dtype=np.float32).T, ndmin=4)
            keep = tf.constant(keep)
            keep = tf.get_variable('keep', initializer=keep,
                                   trainable=False, dtype=tf.float32)
        view_kernel(kernel)
    return kernel, bias


def shared_parameters(with_inv=False, with_param_k=False, half_feat=True,
                      **kwargs):
    parameters = []

    metaparam = [
        {'scope_name': 'conv1y',
         'in_filters': 1,
         'out_filters': 10},
        {'scope_name': 'conv1uv',
         'in_filters': 2,
         'out_filters': 6},
        {'scope_name': 'conv2',
         'in_filters': 16,
         'out_filters': 64}]
    metaparam.append(
        {'scope_name': 'conv3',
         'in_filters': metaparam[-1]['out_filters'],
         'out_filters': 256})

    if with_param_k:
        out_filters = metaparam[0]['out_filters'] + metaparam[1]['out_filters']
        in_filters = 3
        param, bias = \
            create_k_params('conv1', in_filters, out_filters)
        parameters.append(param)
        parameters.append(bias)

        for meta in metaparam[2:]:
            kernel, bias = create_kernel_bias(**meta, half_feat=half_feat)
            parameters.append(kernel)
            parameters.append(bias)

    elif with_inv:
        layers = ['conv%d' % i for i in range(1, 4)]
        out_filters = [16, 64, 256]
        in_filters = [3] + out_filters
        n_inv = get_n_invariants()
        for i, scope_name in enumerate(layers):
            inv_weights, bias = \
                create_inv_weights_bias(scope_name, in_filters[i] * n_inv,
                                        out_filters[i])
            in_filters = in_filters * n_inv
            parameters.append(inv_weights)
            parameters.append(bias)
    else:
        for meta in metaparam:
            kernel, bias = create_kernel_bias(**meta, half_feat=half_feat)
            parameters.append(kernel)
            parameters.append(bias)

    return parameters


def _kij(i, j, I, W):
    with tf.name_scope("kernels/%d/%d" % (i, j)):
        # Ws are [in|1, out, 3, 3]
        n_in = W['G2'].get_shape()[0].value
        n_out = W['G2'].get_shape()[1].value
        # Ws need to be [1, in|1, out, 9]
        w_shape = [1, n_in, n_out, 9]
        G2, Gx2, Gy2, GxGy = squared_gradients(I)
        # Gs are [batch, height, width, in]
        n_in_G = G2.get_shape()[-1].value
        # G[:, i ,j, :]  is [batch, in]
        # G[:, i ,j, :] needs to be expanded to [batch, in, 1, 1]
        G_4D = [-1, n_in_G, 1, 1]
        # kij is [batch, in, out, 9]
        kij = tf.reshape(G2[:, i, j, :], G_4D)*tf.reshape(W['G2'], w_shape) \
            + tf.reshape(Gx2[:, i, j, :], G_4D)*tf.reshape(W['Gx2'], w_shape) \
            + tf.reshape(Gy2[:, i, j, :], G_4D)*tf.reshape(W['Gy2'], w_shape) \
            + tf.reshape(GxGy[:, i, j, :], G_4D)*tf.reshape(W['GxGy'], w_shape)
        return kij


def _W(params):
    # TODO it might be more memory efficient to have Ws as sparse tensors
    with tf.name_scope("compute_param_masks"):
        # params is shape [n_in|1, n_out, 5]
        n_in = params.get_shape()[0].value
        n_out = params.get_shape()[1].value
        # construct bases to multiply a,b,c,d,e
        sides = tf.constant([[0, 0, 0], [1, 0, 1], [0, 0, 0]],
                            dtype=tf.float32, name='sides')
        vertical = -tf.transpose(sides, [1, 0], name='vertical')
        main_corner = tf.constant([[1, 0, 0], [0, 0, 0], [0, 0, 1]],
                                  dtype=tf.float32, name='main_corner')
        second_corner = tf.constant([[0, 0, 1], [0, 0, 0], [1, 0, 0]],
                                    dtype=tf.float32, name='second_corner')
        center = tf.constant([[0, 0, 0], [0, 1, 0], [0, 0, 0]],
                             dtype=tf.float32, name='center')
        # reshape to match params [in|1, out, 3*3]
        # then stack at 3rd rank position creating [in|1, out, 5, 3*3]
        base_shape = [1, 1, 3*3]
        base = tf.stack([  # a, b, c, d, e
                tf.reshape(main_corner + second_corner, base_shape, name='a'),
                tf.reshape(main_corner - second_corner, base_shape, name='b'),
                tf.reshape(sides, base_shape, name='c'),
                tf.reshape(vertical, base_shape, name='d'),
                tf.reshape(center, base_shape, name='e')], axis=2)
        scaled_base = tf.reshape(tf.expand_dims(params, -1) * base,
                                 [n_in, n_out, 5, 3, 3])
        # transpose to [abcde, in|1, out, 3, 3]
        scaled_base = tf.transpose(scaled_base, [2, 0, 1, 3, 4],
                                   name='param_masks')
    with tf.name_scope("compute_grad_weight_masks"):
        a, b, c, d, e = range(5)
        # Ws are [in|1, out, 3, 3]
        W = {'G2': tf.reduce_sum(tf.gather(scaled_base, [a, e]),
                                 axis=[0], name='G2_mask'),
             'Gx2': tf.reduce_sum(tf.gather(scaled_base, [c, d]),
                                  axis=[0], name='Gx2_mask'),
             'GxGy': tf.reduce_sum(tf.gather(scaled_base, [b]),
                                   axis=[0], name='GxGy_mask')}
        W['Gy2'] = tf.transpose(-W['Gx2'], [0, 1, 3, 2], name='Gy2_mask')
        return W


def conv_parameterized_kernels(images, params):
    '''
    params should be is shape [n_in|1, n_out, 5]
    images should be standard batch, HEIGHT, WIDTH, n_in]
    '''
    n_in = images.get_shape()[-1].value
    n_out = params.get_shape()[1].value
    # Ws are [in|1, out, 3, 3]
    W = _W(params)
    # I is [batch, HEIGHT, WIDTH, n_in]
    I = images
    HEIGHT = I.get_shape()[1].value
    WIDTH = I.get_shape()[2].value
    with tf.name_scope("compute_local_kernels"):
        # build 3*3 kernels for every pixel i,j
        # K is a tensor [ batch, HEIGH, WIDTH, n_in, n_out, 3*3]
        K = tf.stack([tf.stack([_kij(i, j, I, W) for j in range(WIDTH)], axis=1)
                      for i in range(HEIGHT)], axis=1, name='join_kernels')
        view_param_k(K)
        # Tranpose and reshape K to match [batch, HEIGHT, WIDTH, n_out, n_in*3*3]
        K = tf.transpose(K, [0, 1, 2, 4, 3, 5])
        K = tf.reshape(K, [-1, HEIGHT, WIDTH, n_out, n_in*3*3])
    with tf.name_scope("compute_image_patches"):
        # To calculate the masking, extract image patches corresponding
        # to each output pixel i,j
        I = tf.extract_image_patches(I, ksizes=[1, 3, 3, 1],
                                     strides=[1, 1, 1, 1],
                                     rates=[1, 1, 1, 1],
                                     padding='SAME', name=None)
        # I is [batch, output HEIGHT, output WIDTH, n_in*ksizeH*ksizeW]
        # output_size = (input_size + stride - 1) / stride
        # in this case output_size == input_size
        I = tf.expand_dims(I, axis=-2)
    with tf.name_scope("apply_local_kernels"):
        # Apply the masking, getting output feture maps
        # with shape [batch, HEIGHT, WIDTH, n_out]
        return tf.reduce_sum(I * K, axis=[-1])


def conv_layer(images, scope_name, idx,
               activation='tanh',
               dropout=False,
               half_feat=True,
               with_inv=False,
               with_param_k=False):
    with tf.variable_scope(scope_name, reuse=True) as scope:
        bias = tf.get_variable('biases')
        if with_param_k:
            params = params = tf.get_variable('k_params')
            conv = conv_parameterized_kernels(images, params)
        elif with_inv:
            inv = compute_invariants(scope_name, images)
            inv_weights = tf.get_variable(name='inv_weights')
            conv = tf.nn.conv2d(inv, filter=inv_weights,
                                strides=[1, 1, 1, 1], padding='SAME')
        else:
            if half_feat:
                kernel = tf.get_variable('weights')
                if not (scope_name.find('conv1') >= 0):
                    keep = tf.get_variable('keep')
                    mask = tf.tile(keep, [7, 7, 1, 1])
                    conv_kernel = kernel * mask
                else:
                    conv_kernel = kernel
            conv = tf.nn.conv2d(images, conv_kernel,
                                strides=[1, 1, 1, 1], padding='SAME')
        bias = tf.nn.bias_add(conv, bias)
        activation = activation_functions[activation.lower()]
        conv = activation(bias, name=scope.name + '_' + str(idx))
        tf.assert_rank(conv, 4, data=conv.get_shape())
        view_activations(conv)
        if dropout:
            # from tf.nn.dropout doc
            # noise_shape = [batch size, 1, 1, n_channels],
            # each batch and channel component will be kept independently and
            # each row and column will be kept or not kept together.
            noise_shape = conv.get_shape().as_list()
            noise_shape[1] = noise_shape[2] = 1
            conv = tf.nn.dropout(conv, dropout, noise_shape)
        return conv


def single_network(idx, images,
                   pooling='MAX',
                   pooling_stride=[2, 2],
                   pooling_window=[2, 2],
                   **kwargs):
    with_inv = kwargs['with_inv']
    with_param_k = kwargs['with_param_k']

    if with_inv or with_param_k:
        conv1 = conv_layer(images, 'conv1', idx, **kwargs)
    else:
        imagesy = tf.slice(images, [0, 0, 0, 0], [-1, -1, -1, 1])
        imagesuv = tf.slice(images, [0, 0, 0, 0], [-1, -1, -1, 2])
        # conv1
        conv1y = conv_layer(imagesy, 'conv1y', idx)
        conv1uv = conv_layer(imagesuv, 'conv1uv', idx)
        # cat conv1_y with conv1_uv
        conv1 = tf.concat([conv1y, conv1uv], axis=3, name='conv1_' + str(idx))
    # pool1
    pool1 = tf.nn.pool(conv1,
                       pooling_type=pooling,
                       window_shape=pooling_stride,
                       strides=pooling_stride,
                       padding='VALID',
                       name='pool1_' + str(idx))
    # Use parameterized kernels only on the first layer for now
    del kwargs['with_param_k']
    # conv2
    conv2 = conv_layer(pool1, 'conv2', idx, **kwargs)

    # pool2
    pool2 = tf.nn.pool(conv2,
                       pooling_type=pooling,
                       window_shape=pooling_stride,
                       strides=pooling_stride,
                       padding='VALID',
                       name='pool2_' + str(idx))

    # conv3
    conv3 = conv_layer(pool2, 'conv3', idx, **kwargs)
    return conv3


def pyramid_network(image_pyramid, **kwargs):
    """
    instantiate multiscale network
    """
    with tf.name_scope('pyramid_net'):
        # instantiate shared params
        shared_parameters(**kwargs)
        # instantiate multi scale convolutions
        outputs = []
        for idx, imgs in enumerate(image_pyramid):
            out = single_network(idx, imgs, **kwargs)
            # store output
            outputs.append(out)
        # do upsampling
        # TODO check what is done in the paper, doing bilinear interpolation
        for i, out in enumerate(outputs):
            if i > 0:
                outputs[i] = tf.image.resize_images(
                    out, [outputs[0].get_shape()[1].value,
                          outputs[0].get_shape()[2].value],
                    method=tf.image.ResizeMethod.BILINEAR)
        return tf.concat(outputs, axis=3)


def mlp_classif(features, image_shape, activation, n_hidden=1024, n_classes=2):
    """
    Final mlp for classification
    Used in the "superpixels" aproach, described in section 4.1
    """
    with tf.name_scope('final_mlp'):
        # get shape of features as they came from the previous multiscale
        # network
        batch_size, height, width, n_features = \
            tuple(features.get_shape().as_list())
        # reshape properly into feature vectors
        features = tf.reshape(features, [-1, n_features])
        # hidden fully connected layer
        activation = activation_functions[activation.lower()]
        hidden = tf.layers.dense(
            inputs=features, units=n_hidden,
            kernel_initializer=tf.random_normal_initializer(stddev=1e-1),
            activation=activation)
        # final linear layer (softmax done by the loss function)
        out = tf.layers.dense(
            inputs=hidden, units=n_classes,
            kernel_initializer=tf.random_normal_initializer(stddev=1e-1))

        out = tf.reshape(out, [-1, height, width, n_classes])
        HEIGHT, WIDTH = image_shape
        out = tf.image.resize_images(out, [HEIGHT, WIDTH],
                                     method=tf.image.ResizeMethod.BILINEAR)
        labels_out = tf.argmax(tf.nn.softmax(out), 3)
        return out, labels_out


def batch_segmentation(batch_images, **kwargs):
    """
    Gets a batch of images with shape (batch size, height, width, channels)
    and outputs felzenszwalb segmentation maps (batch size, height, width)
    """
    batch_size = batch_images.shape[0]
    segmentation = []
    for i in range(batch_size):
        seg = felzenszwalb(batch_images[i, :, :, :], **kwargs)
        segmentation.append(seg)
    segmentation = np.expand_dims(np.array(segmentation), -1)
    return segmentation


def avg_segments(y_pred, segmentation):
    """
    y_pred: tensor with the class predictions for every pixel
        shape (batch, height, width, classes)
    segmentation: tensor with segmentation maps
        shape (batch, height, width)

    - reshape to have H*W as 2nd dim
    - unpack batches into lists
    - treat each element of the batch and then repack
    """
    batch_size, height, width, n_classes = y_pred.get_shape().as_list()
    classes = tf.unstack(
        tf.reshape(y_pred, shape=[batch_size, height*width, n_classes]))
    segment_ids = tf.unpack(
        tf.reshape(segmentation, shape=[batch_size, height*width]))
    _avg_segments = []
    for d, s in zip(classes, segment_ids):
        num_segments = tf.reduce_max(s) - 1
        # get a count of each segment for computing mean
        # (returned in the order they appear)
        seg_ids, _, counts = tf.unique_with_counts(s)
        # need to sort count in the order of the segment ids to align with the
        # output of unsorted_segment_sum
        # tf has no sort op, using top_k function to do it
        _, sorted_seg_idx = tf.nn.top_k(seg_ids, k=num_segments)
        # top_k sorting is descending, want it to be ascending
        sorted_seg_idx = tf.reverse(sorted_seg_idx, [True])
        # getting counts according to the indices defined in sorted_seg_idx
        # convert to float to allow div operation
        counts = tf.to_float(tf.gather(counts, sorted_seg_idx))
        means_seg = tf.div(tf.unsorted_segment_sum(d, s, num_segments), counts)
        # use gather to get the mean values for each segment in the right place
        # then reshaping into a regular per pixel class distribution
        avg_class = tf.reshape(tf.gather(means_seg, s),
                               shape=[height, width, -1])
        _avg_segments.append(avg_class)

    return tf.stack(_avg_segments, name='y_pred')
