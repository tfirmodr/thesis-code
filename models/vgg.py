#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr 21 17:57:16 2017

@author: thalita

VGG

Wrapping VGG net from slim into a tf Estimator

"""

import tensorflow as tf
from tensorflow.contrib.learn import Estimator, ModelFnOps
import tensorflow.contrib.slim as slim
from tensorflow.contrib.slim import nets
import tensorflow.contrib.framework as framework
import tensorflow.contrib.layers as layers
from tensorflow.contrib.framework import assign_from_checkpoint_fn
import os
import urllib
import tarfile

from .training import set_training
from .view import view_activations, view_kernel
from .fullmulticonv import sparse_combination

vgg = nets.vgg


class RestoreHook(tf.train.SessionRunHook):
    def __init__(self, init_fn):
        self.init_fn = init_fn

    def after_create_session(self, session, coord=None):
        if session.run(framework.get_or_create_global_step()) == 0:
            tf.logging.info('Restoring imagenet weights for VGG net')
            self.init_fn(session)


class VGG(Estimator):

    default_params = dict(
        variant=16,  # a, 16 or 19
        weights=None,
        n_classes=1000,
        fc_size=4096,
        fc6_shape=[1, 1],
        vgg_end_point='pool5',
        dropout_keep_prob=0,
        pooling_window=[2, 2],
        tv_loss=0,
        optimizer='Momentum',
        optimizer_params=dict(learning_rate=1e-2, momentum=0.9),
        shortcutsL1=None,
        shortcutsL1_steps=None,
        shortcutsL1_final=None,
        shortcuts_init=1e-5,
        regL2=5e-4)

    def __init__(self, model_dir, params=None, config=None):
        Estimator.__init__(self, model_fn=VGG._model_fn,
                           model_dir=model_dir,
                           config=config,
                           params=params,
                           feature_engineering_fn=VGG._feature_engineering_fn)

    def _feature_engineering_fn(X, y):
        """
        VGG assumes images are at least 224x224 pixels
        """
        h, w = X['X'].get_shape().as_list()[1:3]
        # reshape smallest side to be 224
        # and the other is rescaled proportionally (will be >= 224)
        new_size = None
        if w > h:
            if h < 224:
                new_size = [224, w*224//h]
        else:
            if w < 224:
                new_size = [h*224//w, 224]
        if new_size is not None:
            X['X'] = tf.image.resize_images(X['X'], new_size)
            tf.summary.image('input_image', X['X'])
        return X, y

    def _model_fn(X, y, mode, params, config=None):
        if params is None:
            params = VGG.default_params()

        is_training = False if mode == 'predict' else True
        with slim.arg_scope(vgg.vgg_arg_scope(params['regL2'])):
            kwargs = dict(params)
            del kwargs['regL2']
            del kwargs['weights']
            del kwargs['optimizer']
            del kwargs['optimizer_params']
            del kwargs['tv_loss']
            del kwargs['shortcutsL1_steps']
            del kwargs['shortcutsL1_final']

            # preparing shortcut weight growth
            if (params['shortcutsL1'] is not None
                and params['shortcutsL1_final'] is not None
                and params['shortcutsL1_steps'] is not None):
                kwargs['shortcutsL1'] = tf.train.polynomial_decay(
                    params['shortcutsL1'],
                    tf.train.get_or_create_global_step(),
                    params['shortcutsL1_steps'],
                    params['shortcutsL1_final'])

            net, _ = VGG._vgg_net(
                inputs=X['X'],
                is_training=is_training,
                **kwargs)
            # check whether to reload imagenet weights
            # (available for VGG16 an 19 only)
            hooks = None
            if params['weights'] == 'imagenet':
                if params['variant'] in {16, 19}:
                    end_point = int(params['vgg_end_point'][-1:])
                    init_fn = VGG._get_weights(params['variant'],
                                               up_to_layer=end_point)
                    hooks = [RestoreHook(init_fn)]
                else:
                    tf.logging.warning(
                        "This VGG variant has no pre-trained weights, "+
                        "falling back to random initialization.")


        y_pred_logits = tf.image.resize_images(net, y.get_shape()[1:])

        train_step, y_pred_labels = set_training(
                y, y_pred_logits,
                tv_loss=params['tv_loss'],
                optimizer=params['optimizer'],
                optimizer_params=params['optimizer_params'])

        return ModelFnOps(mode=mode,
                          predictions=y_pred_labels,
                          loss=tf.losses.get_total_loss(),
                          train_op=train_step,
                          eval_metric_ops=None,
                          output_alternatives=None,
                          training_chief_hooks=None,
                          training_hooks=hooks,
                          scaffold=None)


    def _get_weights(variant, up_to_layer=5):
        url = ("http://download.tensorflow.org/models/vgg_%s_2016_08_28.tar.gz"
               % str(variant))
        DATA_DIR = 'Data'
        filename = url.split('/')[-1]
        if not tf.gfile.Exists(DATA_DIR):
            tf.gfile.MakeDirs(DATA_DIR)
        filepath = os.path.join(DATA_DIR, filename)
        if not tf.gfile.Exists(filepath):
            print('Downloading weights', filename)
            filepath, _ = urllib.request.urlretrieve(url, filepath)
            with tf.gfile.GFile(filepath) as f:
                size = f.size()
            print('Successfully downloaded', filename, size, 'bytes.')

        model_name = 'vgg_' + str(variant)
        ckpt_fname = '%s.ckpt' % model_name
        with tarfile.open(filepath, 'r:gz') as file:
            file.extract(ckpt_fname, DATA_DIR)

        var_list = ['/'.join((model_name, l)) for l in ['fc6', 'fc7', 'fc8']]
        var_list.append('global_step')

        include_list = ['conv%d' % i for i in range(1, up_to_layer + 1)]
        include_list = ['/'.join((model_name, l)) for l in include_list]
        init_fn = assign_from_checkpoint_fn(
            os.path.join(DATA_DIR, ckpt_fname),
            slim.get_variables_to_restore(include=include_list,
                                          exclude=var_list),
            ignore_missing_vars=True)

        return init_fn

    def _vgg_net(
            inputs,
            n_classes=1000,
            is_training=True,
            dropout_keep_prob=0.5,
            pooling_window=[2, 2],
            vgg_end_point='pool5',
            variant='a',
            shortcutsL1=1e-2,
            shortcuts_init=1e-5,
            fc_size=4096,
            fc6_shape=[7, 7]):
        """Oxford Net VGG adapted to produce pixelwise classification.

        Based on tf.contrib.slim implementation

        Args:
            inputs: a tensor of size [batch_size, height, width, channels].
            num_classes: number of predicted classes.
            is_training: whether or not the model is being trained.
            dropout_keep_prob: the probability that activations are kept in
                the dropout layers during training.

            scope: Optional scope for the variables.
        Returns:
              the last op containing the log predictions and end_points dict.
        """
        scope = model_name = 'vgg_' + str(variant)

        # quantity of chained conv layers for each VGG variant
        repeats = {'a': [1, 1, 2, 2, 2],
                   16: [2, 2, 3, 3, 3],
                   19: [2, 2, 4, 4, 4]}
        channels = [64, 128, 256, 512, 512]

        with tf.variable_scope(scope, model_name, [inputs]) as sc:
            end_points_collection = sc.original_name_scope + '_end_points'
            # Collect outputs for conv2d, fully_connected and max_pool2d.
            tf.add_to_collection(end_points_collection, inputs)
            with slim.arg_scope(
                    [layers.conv2d, sparse_combination],
                    outputs_collections=end_points_collection):
                # iteratively build layers of vgg and stop at given end point
                rep = repeats[variant]
                net = inputs
                for l, ch in enumerate(channels):
                    scope = 'conv%d' % (l + 1)
#                    net = layers.repeat(
#                            net, rep[l], layers.conv2d, ch, [3, 3],
#                            scope=scope)
                    for l_i in range(rep[l]):

                        net = layers.conv2d(
                                net, ch, [3, 3],
                                scope=scope + '/conv%d_%d' % (l + 1, l_i + 1))


                        if shortcutsL1 is not None:
                            net = sparse_combination(
                                    regL1=shortcutsL1,
                                    init_scale=shortcuts_init,
                                    scope='comb%d_%d' % (l + 1, l_i + 1))

                    if l == 0:
                        view_activations(net, 'conv%d_%d' % (l + 1, l_i + 1))
                        for k in slim.get_variables_by_suffix('weights:0'):
                            with tf.name_scope('conv1'):
                                view_kernel(k)


                    if vgg_end_point == scope:
                        break

                    scope = 'pool%d' % (l + 1)
                    net = layers.max_pool2d(
                            net, pooling_window,
                            scope=scope)

                    if vgg_end_point == scope:
                        break

                # Use conv2d instead of fully_connected layers.

                net = layers.conv2d(net, fc_size, fc6_shape,
                                    padding='VALID', scope='fc6')

                if shortcutsL1 is not None:
                    net = sparse_combination(
                            regL1=shortcutsL1,
                            init_scale=shortcuts_init,
                            scope='combfc6')

                if dropout_keep_prob:
                    net = layers.dropout(
                            net, dropout_keep_prob,
                            is_training=is_training, scope='dropout6')

                net = layers.conv2d(net, fc_size, [1, 1], scope='fc7')

                if dropout_keep_prob:
                    net = layers.dropout(
                            net, dropout_keep_prob,
                            is_training=is_training, scope='dropout7')

                if shortcutsL1 is not None:
                    net = sparse_combination(
                            regL1=shortcutsL1,
                            init_scale=shortcuts_init,
                            scope='combfc7')

                net = layers.conv2d(
                      net,
                      n_classes, [1, 1],
                      activation_fn=None,
                      normalizer_fn=None,
                      scope='fc8')

                # Convert end_points_collection into a end_point dict.
                su = slim.utils
                end_points = su.convert_collection_to_dict(end_points_collection)

        return net, end_points
