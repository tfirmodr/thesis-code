#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jun 23 10:05:26 2017

@author: thalita

ResNet for dense prediction

Wraping implem from tf slim into estimator

"""

import tensorflow as tf
from tensorflow.contrib.learn import Estimator, ModelFnOps
import tensorflow.contrib.slim as slim
from tensorflow.contrib.slim import nets
import tensorflow.contrib.layers as layers
from tensorflow.contrib.framework import assign_from_checkpoint_fn
import os


from .training import set_training
from .view import view_activations, view_kernel
from .vgg import VGG
from .utils import maybe_download_extract, RestoreHook
from .ssl import ssl_regularizer


resnet_utils = nets.resnet_utils

resnet_fns = {
    'v1_50': nets.resnet_v1.resnet_v1_50,
    'v2_50': nets.resnet_v2.resnet_v2_50,
    'v1_101': nets.resnet_v1.resnet_v1_101,
    'v2_101': nets.resnet_v2.resnet_v2_101,
    'v1_152': nets.resnet_v1.resnet_v1_152,
    'v2_152': nets.resnet_v2.resnet_v2_152,
    'v1_200': nets.resnet_v1.resnet_v1_200,
    'v2_200': nets.resnet_v2.resnet_v2_200}

v1_url = 'http://download.tensorflow.org/models/resnet_v1_%s_2016_08_28.tar.gz'
v2_url = 'http://download.tensorflow.org/models/resnet_v2_%s_2017_04_14.tar.gz'

resnet_urls = dict([(k, v1_url % k[-2:])
                    for k in resnet_fns.keys()
                    if k[0:2] == 'v1' and k[-3:] != '200'] +
                   [(k, v2_url % k[-2:])
                    for k in resnet_fns.keys()
                    if k[0:2] == 'v2' and k[-3:] != '200'])



class ResNet(VGG):

    default_params = dict(
        version='v2_50',
        top='channel-fc',  # pixel-fc, channel-fc
        weights=None,
        n_classes=1000,
        tv_loss=0,
        output_stride=16,
        optimizer='Momentum',
        optimizer_params=dict(learning_rate=1e-2, momentum=0.9),
        regL2=1e-5,
        depth_wise=0,
        channel_wise=0,
        filter_wise=0,
        batch_norm_decay=0.997,
        batch_norm_epsilon=1e-5,
        batch_norm_scale=True)

    def __init__(self, model_dir, params=None, config=None):
        Estimator.__init__(self, model_fn=ResNet._model_fn,
                           model_dir=model_dir,
                           config=config,
                           params=params,
                           feature_engineering_fn=VGG._feature_engineering_fn)

    def _model_fn(X, y, mode, params, config=None):
        if params is None:
            params = ResNet.default_params()

        is_training = False if mode == 'predict' else True
        with slim.arg_scope(resnet_utils.resnet_arg_scope(
                is_training=is_training,
                weight_decay=params['regL2'],
                batch_norm_decay=params['batch_norm_decay'],
                batch_norm_epsilon=params['batch_norm_epsilon'],
                batch_norm_scale=params['batch_norm_scale'])):
            with slim.arg_scope(regularizer_arg_scope(
                    params['regL2'],
                    params['channel_wise'],
                    params['filter_wise'],
                    params['depth_wise'])):

                net = _res_net(
                    inputs=X['X'],
                    top=params['top'],
                    version=params['version'],
                    num_classes=params['n_classes'],
                    output_stride=params['output_stride'])

            # check whether to reload imagenet weights
            hooks = None
            if params['weights'] == 'imagenet':
                if params['version'] in resnet_urls:
                    init_fn = ResNet._get_weights(params['version'])
                    hooks = [RestoreHook(init_fn)]
                else:
                    tf.logging.warning(
                        "This Resnet variant has no pre-trained weights, " +
                        "falling back to random initialization.")

        y_pred_logits = tf.image.resize_images(net, y.get_shape()[1:])

        train_step, y_pred_labels = set_training(
                y, y_pred_logits,
                tv_loss=params['tv_loss'],
                optimizer=params['optimizer'],
                optimizer_params=params['optimizer_params'])

        return ModelFnOps(mode=mode,
                          predictions=y_pred_labels,
                          loss=tf.losses.get_total_loss(),
                          train_op=train_step,
                          eval_metric_ops=None,
                          output_alternatives=None,
                          training_chief_hooks=None,
                          training_hooks=hooks,
                          scaffold=None)

    def _get_weights(version):
        url = resnet_urls[version]
        DATA_DIR = 'Data'
        model_name = 'resnet_' + version
        ckpt_fname = model_name + '.ckpt'
        maybe_download_extract(ckpt_fname, url, DATA_DIR)

        exclude = ['logits/weights', 'logits/biases', 'global_step']

        init_fn = assign_from_checkpoint_fn(
            os.path.join(DATA_DIR, ckpt_fname),
            slim.get_variables_to_restore(exclude=exclude))
        return init_fn


def _res_net(
        inputs,
        top='channel-fc',
        version='v1_152',
        num_classes=10,
        output_stride=None):

    model_name = 'resnet_'+version
    end_points_collection = model_name + '_end_points'

    net, end_points = resnet_fns[version](
            inputs,
            num_classes=None,
            global_pool=False,
            output_stride=output_stride)

    if top == 'pixel-fc':
        conv_shape = net.get_shape().as_list()[1:3]
    elif top == 'channel-fc':
        conv_shape = [1, 1]

    net = layers.conv2d(
            net,
            num_classes, conv_shape,
            activation_fn=None,
            normalizer_fn=None,
            scope='logits',
            outputs_collections=end_points_collection)

    return net


def regularizer_arg_scope(
        regL2=0,
        depth_wise=1e-5,
        channel_wise=1e-5,
        filter_wise=1e-5):
    regs = []
    if regL2 != 0:
        regs.append(layers.l2_regularizer(regL2))
    if depth_wise or channel_wise or filter_wise:
        regs.append(ssl_regularizer(
                filter_wise, channel_wise, shape_wise, depth_wise))

    with slim.arg_scope(
      [layers.conv2d],
      weights_regularizer=layers.sum_regularizer(regs)) as arg_sc:
          return arg_sc