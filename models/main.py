#! /usr/bin/python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov  7 14:24:11 2016

@author: thalita

Implementation of the scene labeling CNN model from Farabet et al 2013

"""

import numpy as np
import tensorflow as tf
from tensorflow.tensorboard.tensorboard import main as launch_tb
import traceback
import os
import shutil
import argparse
from threading import Thread
from sklearn.cross_validation import KFold, ShuffleSplit
import data
from model import image_pyramid, pyramid_network, mlp_classif, avg_segments,\
    batch_segmentation
from view import plot_sample_images

from sacred import Experiment
#from sacred.observers import SqlObserver

ex = Experiment('testing_sacred')
#ex.observers.append(SqlObserver.create('sqlite:///Data/experiments.db'))


@ex.config
def base():
    """
    Configuration parameters used in the experiment
    can be changed in the command line by adding <with param=value>
        ex: with invariants=True dataset_name='stanford'
    """
    n_hidden = 1024  # Final MLP hidden units
    # Regularization
    regL2 = 1e-5
    # Optimization params
    batch_size = 1
    learning_rate = 1e-3
    max_epoch = 1e9

    invariants = False
    param_k = False
    dataset_name = 'squares'
    preload = True
    REAL_DATA = True if dataset_name == 'stanford' else False
    noise=None
    label_type = None
    if REAL_DATA:
        # Stanford dataset dimensions
        HEIGHT = data.Stanford_dataset.height
        WIDTH = data.Stanford_dataset.width
        # Set n_samples to an integer to use only part of the images (max 715)
        n_samples = 715
        label_type = 'regions'
        n_classes = data.Stanford_dataset.n_classes[label_type]
        if label_type != 'fgbg':
            weights = [0.0] + [1.0] * n_classes
        else:
            weights = 1.0

    else:
        # dummy numpy random arrays as input images
        HEIGHT = 60
        WIDTH = 80
        n_samples = 700
        n_classes = 2
        max_epoch = 100
        weights = 1.0
        noise = 0.1
    log_dir = 'log'

@ex.capture
def build_net(graph, REAL_DATA, n_hidden, regL2, batch_size, learning_rate, HEIGHT, WIDTH, n_classes, n_samples, invariants, param_k):

    with graph.as_default():
        # placeholders for inputs
        X = tf.placeholder(tf.float32,
                           shape=(None, HEIGHT, WIDTH, 3), name='X')
        y = tf.placeholder(tf.float32,
                           shape=(None, HEIGHT, WIDTH), name='y')

        segmentation = tf.placeholder(tf.int32,
                                      shape=(None, HEIGHT, WIDTH, 1),
                                      name='segmentation')
        # %% instantiate network
        output = pyramid_network(image_pyramid(X, (HEIGHT, WIDTH)),
                                 with_inv=invariants,
                                 with_param_k=param_k)
        y_pred = mlp_classif(output, (HEIGHT, WIDTH),
                                         n_hidden, n_classes=n_classes)

        # y_pred = avg_segments(y_pred, segmentation)
        # generate objective function
        tf.summary.image('input_labels', tf.expand_dims(y, -1))
        # cross entropy
        xentropy = tf.losses.sparse_softmax_cross_entropy(
                logits=y_pred, labels=tf.to_int32(y))
        tf.summary.scalar('xentropy', xentropy)
        # regularization
        variables = tf.get_collection('trainable_variables')
        regularizer = tf.contrib.layers.l2_regularizer(regL2)
        L2reg = tf.contrib.layers.apply_regularization(regularizer, variables)
        tf.summary.scalar('L2reg', L2reg)
        # objective
        objective = tf.losses.get_total_loss()  # xentropy + L2reg
        tf.summary.scalar('objective', objective)
        # Create a variable to hold the global_step.
        global_step_tensor = tf.contrib.framework.get_or_create_global_step()
        # Instantiate training and accuracy calculation
        # Original paper indicates using Stochastic Gradient descent
        # Calling minimize() takes care of both
        # computing the gradients and applying them to the variables.
        train_step = tf.train.GradientDescentOptimizer(learning_rate)\
            .minimize(objective, global_step=global_step_tensor)
#        accuracy, _ = tf.metrics.accuracy(tf.to_int32(y),
#                                          tf.to_int32(tf.argmax(y_pred, -1)),
#                                          name='accuracy')

        correct_prediction = tf.equal(tf.to_int32(tf.argmax(y_pred, -1)),
                                      tf.to_int32(y))
        accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32),
                                  name='accuracy')
        tf.summary.scalar('accuracy', accuracy)
        init = tf.global_variables_initializer()
        merged_summaries = tf.summary.merge_all()

        # Save checkpoint
        saver = tf.train.Saver()
        return init, X, y, segmentation, train_step, accuracy,\
            merged_summaries, saver, global_step_tensor

@ex.automain
def main(REAL_DATA, n_hidden, regL2, batch_size, learning_rate, HEIGHT, WIDTH, n_classes, n_samples, log_dir, max_epoch, preload, label_type, weights, noise):
    if REAL_DATA:
        dataset = data.Stanford_dataset(n_samples=n_samples,
                                        preload=preload,
                                        label_type=label_type)

    else:
        # dummy numpy random arrays as input images
        images, labels = data.random_inputs(n_samples, HEIGHT, WIDTH,
                                            n_classes, noise)
        plot_sample_images(images, labels)

    tf.logging.set_verbosity(tf.logging.WARN)

    # split train test data (5 fold)
    split = KFold(n=n_samples, n_folds=5)
    # XXX using single split for now
    split = ShuffleSplit(n=n_samples, n_iter=1, test_size=0.1)
    print('Insantiating graph')
    train_graph = tf.Graph()
    init, X, y, segmentation, train_step, accuracy, merged_summaries, saver,\
        global_step_tensor = build_net(train_graph)
    print('... Done!')
    sess = tf.Session(graph=train_graph)
    # Check for previous session
    restoring = False
    if os.path.exists('/tmp/graph.checkpoint.index'):
        ans = input('Reload previous session? (y/n)')
        if ans.lower() == 'y':
            print('Loading session')
            saver.restore(sess, '/tmp/graph.checkpoint')
            restoring = True
    if not restoring :
        print('Initializing variables')
        sess.run(init)
        try:
            if os.path.exists(log_dir):
                shutil.rmtree(log_dir)
        except:
            pass

    # instantiate summary writers for tensorboard
    train_writer = tf.summary.FileWriter(log_dir+'/train/', sess.graph)
    # Launch tb
    tf.flags.FLAGS.logdir = log_dir
    tb_thread = Thread(target=launch_tb, name='tensorboard')
    tb_thread.start()

    for train_index, test_index in split:
        try:
            # run optimzation
            with sess.as_default():
                global_step = sess.run(global_step_tensor)
                while global_step//split.n_train < max_epoch:
                    # update local global_step
                    global_step = sess.run(global_step_tensor)
                    # get epoch number
                    epoch = global_step//split.n_train
                    # Train with batches
                    batch = global_step % split.n_train
                    n_batches = split.n_train//batch_size
                    # load batch
                    if REAL_DATA:
                        X_batch, y_batch = \
                            dataset.get_batch_list(
                                train_index[batch:batch_size+batch])
                    else:
                        X_batch = images[
                            train_index[batch:batch_size+batch], :, :]
                        y_batch = labels[
                            train_index[batch:batch_size+batch], :, :]
                    seg_batch = batch_segmentation(X_batch)
                    # create feed_dict for placeholders with current batch
                    feed_dict = {X: X_batch, y: y_batch,
                                 segmentation: seg_batch}
                    # run train step, accuracy and summaries
                    _, train_accuracy, summary = \
                        sess.run([train_step, accuracy, merged_summaries],
                                 feed_dict=feed_dict)
                    train_writer.add_summary(summary, global_step)

                    # save every 100 global iters
                    status_tup = ('global count', global_step,
                                  ', epoch', epoch,
                                  ', batch (0-indexed):', batch,
                                  'out of', n_batches,
                                  ', training acc', train_accuracy)
                    if global_step % 100 == 0:
                        saver.save(sess, '/tmp/graph.checkpoint')
                        print(*status_tup, ' saved checkpoint',
                              end='\r', flush=True)
                    else:
                        print(*status_tup, end='\r', flush=True)

                    # Evaluate on test set
                    if batch == n_batches - 1:
                        test_accuracy = 0.0
                        for i in range(len(test_index)):
                            if REAL_DATA:
                                X_batch, y_batch = \
                                    dataset.get_batch_list(test_index[i:i+1])
                            else:
                                X_batch = images[test_index[i:i+1], :, :]
                                y_batch = labels[test_index[i:i+1], :, :]
                            seg_batch = batch_segmentation(X_batch)
                            feed_dict = {X: X_batch, y: y_batch,
                                         segmentation: seg_batch}
                            test_accuracy += sess.run(accuracy,
                                                      feed_dict=feed_dict)
                        test_accuracy /= len(test_index)
    #                    test_writer.add_summary(summary, global_step)
                        print("\nglobal count %d, epoch %d, test acc %g"
                              % (global_step, epoch, test_accuracy))

                    global_step = sess.run(global_step_tensor)

            print('ENDED WELL !')
        except:
            traceback.print_exc()
            tb_thread.join(0.0)
            exit(-1)

#if __name__ == "__main__":
#    ex.run()
