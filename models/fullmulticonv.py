#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jun  2 16:45:16 2017

@author: thalita


"""

import tensorflow as tf
import tensorflow.contrib.layers as layers
from tensorflow.contrib.learn import Estimator, ModelFnOps
import tensorflow.contrib.slim as slim
import numpy as np

from .training import set_training

from .view import view_activations, view_kernel


class FullMultiConv(Estimator):
    default_params = dict(
            pooling_window=[2, 2],
            channels=[32, 32, 64, 64],
            kernel_sizes=[[3, 3], [5, 5]],
            regL2=1e-5,
            regL1=1e-3,
            tv_loss=0,
            optimizer='Momentum',
            optimizer_params=dict(learning_rate=1e-3, momentum=0.9)
    )

    def __init__(self, model_dir, params=None, config=None):
        Estimator.__init__(self, model_fn=_model_fn,
                           model_dir=model_dir,
                           config=config,
                           params=params,
                           feature_engineering_fn=None)


def _model_fn(X, y, mode, params, config=None):
        if params is None:
            params = FullMultiConv.default_params()

        with slim.arg_scope(
                [layers.conv2d],
                activation_fn=tf.nn.relu,
                weights_regularizer=(layers.
                                     l2_regularizer(params['regL2'])),
                biases_initializer=tf.zeros_initializer(),
                padding='SAME'):

            kwargs = dict(params)
            del kwargs['regL2']
            del kwargs['optimizer']
            del kwargs['optimizer_params']
            del kwargs['tv_loss']

            net, _ = _net_fn(inputs=X['X'], **kwargs)

        y_pred_logits = tf.image.resize_images(net, y.get_shape()[1:])

        train_step, y_pred_labels = set_training(
                y, y_pred_logits,
                optimizer=params['optimizer'],
                optimizer_params=params['optimizer_params'])

        return ModelFnOps(mode=mode,
                          predictions=y_pred_labels,
                          loss=tf.losses.get_total_loss(),
                          train_op=train_step,
                          eval_metric_ops=None,
                          output_alternatives=None,
                          training_chief_hooks=None,
                          training_hooks=None,
                          scaffold=None)


def _net_fn(inputs,
            n_classes=1000,
            pooling_window=[2, 2],
            regL1=1e-3,
            channels=[64, 64, 128, 256, 512],
            kernel_sizes=[[3, 3], [5, 5], [7, 7]]):

    scope = model_name = 'multiconv'

    with tf.variable_scope(scope, model_name, [inputs]) as sc:
        end_points_collection = sc.original_name_scope + '_end_points'
        # Collect outputs for conv2d, fully_connected and max_pool2d.
        tf.add_to_collection(end_points_collection, inputs)
        with slim.arg_scope(
                [multiconv2d, layers.max_pool2d, sparse_combination],
                outputs_collections=end_points_collection):
            for i, ch in enumerate(channels):
                net = multiconv2d(inputs, ch, kernel_sizes, regL1=regL1,
                                  scope='conv%d' % (i+1))
                net = layers.max_pool2d(net, pooling_window,
                                        scope='pool%d' % (i+1))
                net = sparse_combination(regL1=regL1,
                                         scope='comb%d' % (i+1))
                view_activations(net, 'comb%d' % (i+1))

            fc_shape = net.get_shape().as_list()[1:3]
            net = layers.conv2d(
                  net,
                  n_classes, fc_shape,
                  activation_fn=None,
                  normalizer_fn=None,
                  scope='fc_end')

            # Convert end_points_collection into a end_point dict.
            su = slim.utils
            end_points = su.convert_collection_to_dict(end_points_collection)

    return net, end_points


@tf.contrib.framework.add_arg_scope
def multiconv2d(inputs,
                num_outputs,
                kernel_sizes=[[1, 1], [3, 3], [5, 5]],
                regL1=1,
                scope=None, outputs_collections=None,
                **kwargs):
    """
    returns: list of  4-D feature maps
    """
    kernel_sizes = np.array(kernel_sizes).squeeze()
    if kernel_sizes.ndim == 1:
        return layers.conv2d(inputs, num_outputs, kernel_sizes,
                             scope=scope, **kwargs)
    else:
        convs = []
        for ix, ks in enumerate(kernel_sizes):
            bscope = scope + '/branch%d' % (ix + 1)
            branch = layers.conv2d(inputs, num_outputs, ks,
                                   scope=bscope, **kwargs,
                                   outputs_collections=None)
            weights = slim.get_variables_by_suffix(bscope + '/weights:0')[0]
            with tf.name_scope(bscope):
                view_kernel(weights)
            convs.append(branch)
        convs = select_combination(convs, regL1, scope + '/select')
        tf.add_to_collection(outputs_collections, convs)
        return convs


def select_combination(previous_outputs, regL1=1, scope=None):
    with tf.variable_scope(scope) as scope:
        br = branches = len(previous_outputs)
        n, h, w, c = previous_outputs[-1].get_shape().as_list()

        lim = 1.0/br
        init = tf.random_uniform_initializer(minval=-lim, maxval=lim)

        w_lis = []
        for i, out in enumerate(previous_outputs):
            if i < br-1:
                w_lis += [tf.get_variable(
                    'w_l%d' % i, shape=[1],  dtype=tf.float32,
                    initializer=init,
                    regularizer=layers.l1_regularizer(regL1))]
                slim.summarize_tensor(w_lis[i], 'w_l%d(%s)' % (i, out.name))
            else:
                w_lis += [1-tf.add_n(w_lis)]

        w_lis = tf.squeeze(tf.stack(w_lis))
        # compute threshold
        # here we want only one element
        # tf top K order is decrasing
        w_lis2 = tf.square(w_lis)
        w_thresh, _ = tf.nn.top_k(w_lis2, k=1)
        w_lis_eff = tf.nn.sigmoid(w_lis2 - w_thresh) * w_thresh

        comb = tf.reduce_sum(
                tf.reshape(w_lis_eff, [1, 1, 1, 1, br]) *
                tf.stack(previous_outputs, axis=-1),
                axis=-1)
        return comb


@tf.contrib.framework.add_arg_scope
def sparse_combination(outputs_collections=None, regL1=1e-5,
                       thresholding=False, init_scale=1e-5, scope=None):
    """
    use end_points collection to get previous outputs and combine them in a
    sparse normalized sum
    """
    with tf.variable_scope(scope) as scope:
        if isinstance(outputs_collections, str):
            previous_outputs = tf.get_collection(outputs_collections)
        else:
            previous_outputs = outputs_collections

        br = branches = len(previous_outputs)
        n, h, w, c = previous_outputs[-1].get_shape().as_list()

        init = tf.constant_initializer(init_scale/(br-1.0))

        w_lis = []
        for i, out in enumerate(previous_outputs):
            # previous outputs but last
            if i < br-1:
                # if previous layer is bigger, resize feature map before
                # combination
                if out.get_shape().as_list()[1:3] != [h, w]:
                    previous_outputs[i] = \
                        tf.image.resize_images(out, (h, w))
                # if previous layer has more channels, project with a 1x1
                # convolution
                if out.get_shape().as_list()[-1] != c:
                    previous_outputs[i] = layers.conv2d(
                            previous_outputs[i],
                            c, [1, 1],
                            activation_fn=None,
                            scope='channel_projection%d' % (i+1),
                            outputs_collections=None)
                # create weight variable
                w_lis += [tf.get_variable(
                    'w_l%d' % i, shape=[], dtype=tf.float32,
                    initializer=init,
                    regularizer=layers.l1_regularizer(regL1))]
                summary_name = 'w_l%d_%s_' % (i, out.name.split(':')[0])
                slim.summarize_tensor(tf.squeeze(w_lis[i]), summary_name)
            # last output will be passed directly if shortcut weights are low
            # enough
            else:
                w_lis += [1-tf.add_n(w_lis)]

        w_lis = tf.squeeze(tf.stack(w_lis))

        if thresholding:
            # compute threshold
            # sort, compute diferences and pick max difference
            # tf top K order is decrasing
            w_lis2 = tf.square(w_lis)
            w_sort, _ = tf.nn.top_k(w_lis2, k=br)
            thresh_ix = tf.to_int32(
                    tf.arg_max(w_sort[0:-2] - w_sort[1:-1], -1)) + 1
            w_thresh = w_sort[thresh_ix]

            w_lis_eff = tf.nn.sigmoid(w_lis2 - w_thresh) * w_lis
        else:
            w_lis_eff = w_lis

        comb = tf.reduce_sum(
                tf.reshape(w_lis_eff, [1, 1, 1, 1, br]) *
                tf.stack(previous_outputs, axis=-1),
                axis=-1)
#    if isinstance(outputs_collections, str):
#        tf.add_to_collection(outputs_collections, comb)

    return comb
