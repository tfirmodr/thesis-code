
"""

@author: thalita

Metric functions
"""
import tensorflow as tf


def confusion_matrix(labels, predictions, n_classes):
    confusion_matrix = tf.confusion_matrix(
        tf.reshape(tf.to_int32(predictions), [-1]),
        tf.reshape(tf.to_int32(labels), [-1]),
        num_classes=n_classes,
        name='confusion_matrix')
    return confusion_matrix


def pr_common_calc(confusion_matrix, name):
    true_positives = tf.matrix_diag_part(confusion_matrix, 'true_positives')
    total_predictions = tf.reduce_sum(confusion_matrix, axis=0)
    total_examples = tf.reduce_sum(confusion_matrix, axis=1)
    false_positives=total_predictions-true_positives
    if name=='precision':
        denominator = total_predictions
    elif name=='recall':
        denominator = total_examples
    else:
        denominator = total_examples+false_positives
    p = tf.to_float(true_positives / denominator)
    # fix nan cases (0/0 division)
    if name == 'precision':
        # if no examples, then a 0 total_predicion is ok (give 1),
        # else give 0
        correction = tf.where(total_examples == 0,
                              tf.ones_like(p),
                              tf.zeros_like(p))
    else:
        # for recall or mean iu give 1 (should return 0 of 0)
        correction = tf.ones_like(p)

    p = tf.where(tf.is_nan(p),
                 correction,  # if is nan
                 p)  # else
    p = tf.reduce_mean(p, name=name)

    return p


def get_pr_function(n_classes, name):
    def func(labels, predictions):
        with tf.name_scope(name):
            cf_mat = confusion_matrix(labels, predictions, n_classes)
            return pr_common_calc(cf_mat, name)
    return func


def precision(labels, predictions, n_classes):
    func = get_pr_function(n_classes, 'precision')
    return func(labels, predictions)


def recall(labels, predictions, n_classes):
    func = get_pr_function(n_classes, 'recall')
    return func(labels, predictions)


def mean_iu(labels, predictions, n_classes):
    func = get_pr_function(n_classes, 'mean_iu')
    return func(labels, predictions)
