#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Nov 16 10:02:58 2017

@author: thalita
"""

import tensorflow as tf
import tensorflow.contrib.slim as slim

from .few_shot_cnn import Classifier


class EpisodicalCNN(tf.estimator.Estimator):
    default_params = dict(
        keep_prob=0.5,
        layer_sizes=[64, 64, 64, 64],
        n_classes=20,
        fc_layer_sizes=[],  # empty list or list with sizes
        learning_rate=1e-3,
        metalearning=True,
        decay_steps=2000,
        decay_rate=0.5,
        weights=None,
        l1=0.0,
        l2=0.0)

    def __init__(self, model_dir=None, config=None, params=None):
        tf.estimator.Estimator.__init__(
                self,
                model_fn=model_fn(params),
                model_dir=model_dir,
                params=params,
                config=config)


    def metatrain_step(self, input_fn, steps=None, max_steps=None, hooks=None):
        self._model_fn.meta_mode = tf.estimator.ModeKeys.TRAIN
        self.train(input_fn, steps=steps, max_steps=steps, hooks=hooks)


class model_fn():
    def __init__(self, params):
        for name, value in params.items():
            self.__setattr__(name, value)
        self._meta_mode = None


    @property
    def meta_mode(self):
        return self._meta_mode

    @meta_mode.setter
    def meta_mode(self, val):
        self._meta_mode = val

    def __call__(self,
                 features,
                 labels,
                 mode,
                 params):
        tf.logging.info("model_fn caled"+str(self._meta_mode))
        for name, value in params.items():
            self.__setattr__(name, value)

        is_training = (mode == tf.estimator.ModeKeys.TRAIN)

        x = features['x_support']
        y = tf.reshape(features['y_support'], [-1])
        xt = features['x_target']
        yt = tf.reshape(labels, [-1])

        full_x = tf.concat([x,xt], axis=0)

        batch_size, height, width, num_channels = x.get_shape().as_list()

        with tf.variable_scope('embedding'):
            cnn = Classifier(batch_size, self.layer_sizes, num_channels)
            # cnn will return vectors for each image
            x_emb = cnn(full_x, is_training, self.keep_prob)

        with tf.variable_scope('classification'):
            # declare fc  layers
            fc_net = x_emb
            if len(self.fc_layer_sizes) >  0:
                for i, size in enumerate(fc_layer_sizes):
                    fc_net = slim.fully_connected(
                            fc_net, size, activation_fn=tf.nn.relu,
                            scope='fc%d' % (i+5))

            logits = slim.fully_connected(
                fc_net, self.n_classes, activation_fn=None,
                weights_regularizer=slim.regularizers.l1_l2_regularizer(self.l1, self.l2),
                scope='classes')
            predictions = slim.softmax(logits, scope='predictions')
            labels = tf.argmax(predictions, axis=-1, output_type=tf.int32,
                           name='labels')

            logits_support, logits_target = tf.split(logits, 2, axis=0)

            pred_support, pred_target = tf.split(predictions, 2, axis=0)

            labels_support, labels_target = tf.split(labels, 2, axis=0)

        # optimization
        update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS)
        with tf.control_dependencies(update_ops):
            learning_rate = self.decay_learning_rate()
            optimizer = tf.train.AdamOptimizer(learning_rate)
            y_one_hot = slim.one_hot_encoding(tf.squeeze(y), self.n_classes)

            loss = tf.losses.softmax_cross_entropy(
                    y_one_hot, logits_support, scope='xentropy_support')

            train_op = optimizer.minimize(
                    tf.losses.get_total_loss(),
                    var_list=slim.get_trainable_variables(scope='classification'),
                    global_step=tf.train.get_or_create_global_step())

            # optimize loss on target (when meta_training)
            yt_one_hot = slim.one_hot_encoding(tf.squeeze(yt), self.n_classes)
            loss_target = tf.losses.softmax_cross_entropy(
                    yt_one_hot, logits_target, scope='xentropy_target',
                    loss_collection=None)
            if self._meta_mode == tf.estimator.ModeKeys.TRAIN:
                cnn_train_op = optimizer.minimize(
                        loss,#_target,
                        var_list=slim.get_trainable_variables(scope='embedding'))
                train_op = tf.group(train_op, cnn_train_op)

        # if metatraining or on eval/predict modes, return loss and predictions
        # on target set
        if (self._meta_mode == tf.estimator.ModeKeys.TRAIN or
                mode != tf.estimator.ModeKeys.TRAIN):
            loss = loss#_target
            labels = labels_support#_target
        else:
            loss = loss
            labels = labels_support

        # evaluation is always on target
        accuracy = tf.metrics.accuracy(yt, labels_target)
        class_accuracy = tf.metrics.mean_per_class_accuracy(
                yt, labels_target, num_classes=self.n_classes)
        if self._meta_mode == tf.estimator.ModeKeys.TRAIN:
            tf.add_to_collection(tf.GraphKeys.UPDATE_OPS, accuracy[1])
            tf.add_to_collection(tf.GraphKeys.UPDATE_OPS, class_accuracy[1])
        slim.summaries.add_scalar_summary(class_accuracy[0])
        slim.summaries.add_scalar_summary(accuracy[0])

        eval_metric_ops = {'accuracy': accuracy,
                           'class_accuracy': class_accuracy}

        training_hooks = []
        if self.weights == 'omniglot':
            training_hooks += [cnn.get_restore_hook()]

        return tf.estimator.EstimatorSpec(
                mode,
                predictions=labels,
                loss=loss,
                train_op=train_op,
                training_hooks=training_hooks,
                eval_metric_ops=eval_metric_ops)

    def decay_learning_rate(self):
        # do learning rate decay
        if self.decay_steps != 0:
            global_step = tf.train.get_or_create_global_step()
            learning_rate = tf.train.exponential_decay(
                    self.learning_rate, global_step,
                    decay_rate=self.decay_rate, decay_steps=self.decay_steps,
                    staircase=True)
            slim.summaries.add_scalar_summary(learning_rate)
        else:
            learning_rate = self.learning_rate
        return learning_rate
