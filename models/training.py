#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Feb 24 11:42:31 2017

@author: thalita

optimizers and set training
"""

import tensorflow as tf
from tensorflow.contrib.layers import summaries as summ

from .metrics import precision, recall, mean_iu

optimizers = dict(
    GradientDescent=tf.train.GradientDescentOptimizer,
    Adadelta=tf.train.AdadeltaOptimizer,
    Adagrad=tf.train.AdagradOptimizer,
    Adam=tf.train.AdamOptimizer,
    Momentum=tf.train.MomentumOptimizer)


def default_optimizer_params(name):
    params = dict()
    params.update(learning_rate=1e-5)
    if name == 'Adadelta':
        params.update(rho=0.95, epsilon=1e-8)
    elif name == 'Adagrad':
        params.update(initial_accumulator_value=0.1)
    elif name == 'Adam':
        params.update(beta1=0.9, beta2=0.999, epsilon=1e-8)
    elif name == 'Momentum':
        params.update(momentum=1e-5, use_nesterov=False)

    return params


def set_training(y, y_pred,
                 regL2=0, regL1=0, tv_loss=0,
                 optimizer='GradientDescent', optimizer_params=None):
    """
    y_pred :  tensor with logits, [batch, height, width, n classes]
    y: tensor with class ids, [batch, height, width]
    """
    n_classes = y_pred.get_shape()[-1]
    y_pred_labels = tf.argmax(tf.nn.softmax(y_pred), 3)
    tf.summary.image('input_labels', tf.to_float(tf.expand_dims(y, -1)))
    tf.summary.image('pred_labels',
                     tf.to_float(tf.expand_dims(y_pred_labels, -1)))

    # generate objective function
    # cross entropy
    xentropy = tf.losses.sparse_softmax_cross_entropy(
            logits=y_pred, labels=tf.to_int32(y))
    summ.summarize_tensor(xentropy)
    # regularization
    variables = tf.get_collection('trainable_variables')
    if regL2:
        regularizer = tf.contrib.layers.l2_regularizer(regL2)
        L2reg = tf.contrib.layers.apply_regularization(regularizer, variables)
        summ.summarize_tensor(L2reg, 'L2_regularization')
    if regL1:
        regularizer = tf.contrib.layers.l1_regularizer(regL1)
        L1reg = tf.contrib.layers.apply_regularization(regularizer, variables)
        summ.summarize_tensor(L1reg, 'L1_regularization')
    # total variation over output
    total_variation = tf.reduce_sum(tf.image.total_variation(y_pred_labels),
                                    name='total_variation')
    if tv_loss != 0:
        tf.losses.add_loss(tv_loss * tf.to_float(total_variation))
    summ.summarize_tensor(total_variation, 'total_variation')
    # objective
    objective = tf.losses.get_total_loss()  # xentropy + L2reg
    summ.summarize_tensor(objective, 'loss')

    # Get/Create a variable to hold the global_step.
    global_step_tensor = tf.contrib.framework.get_or_create_global_step()
    # Instantiate training and accuracy calculation
    default = default_optimizer_params(optimizer)
    # deal with learning rate annealing (decay)
    if 'decay_rate' in optimizer_params:
        if 'decay_steps' not in optimizer_params:
            raise ValueError("Learning rate decay requires decay_steps"
                             " option.")
        learning_rate = default['learning_rate']
        default['learning_rate'] = tf.train.natural_exp_decay(
                learning_rate, global_step_tensor,
                decay_steps=optimizer_params['decay_steps'],
                decay_rate=optimizer_params['decay_rate'])
        del optimizer_params['decay_steps']
        del optimizer_params['decay_rate']

    default.update(optimizer_params)  # update with passed params

    optimizer = optimizers[optimizer](**default)
    # computing the gradients and applying them to the variables.
    grads = optimizer.compute_gradients(objective)
    # monitor gradients
    #summ.summarize_tensors([grad for grad, var in grads])
    train_step = optimizer.apply_gradients(grads,
                                           global_step=global_step_tensor)

    accuracy = tf.contrib.metrics.accuracy(
        tf.to_int32(y_pred_labels), tf.to_int32(y))
    summ.summarize_tensor(accuracy)
    summ.summarize_tensor(y_pred_labels, 'y_pred_labels')
    summ.summarize_tensor(y,'y')

    p = precision(tf.to_int32(y), tf.to_int32(y_pred_labels), n_classes)
    summ.summarize_tensor(p)
    r = recall(tf.to_int32(y), tf.to_int32(y_pred_labels), n_classes)
    summ.summarize_tensor(r)
    m = mean_iu(tf.to_int32(y), tf.to_int32(y_pred_labels), n_classes)
    summ.summarize_tensor(m)

    return train_step, y_pred_labels
