#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar  3 12:18:11 2017

@author: thalita
"""

import tensorflow as tf
from tensorflow.contrib.learn import Estimator, ModelFnOps
import tensorflow.contrib.layers
from tensorflow.contrib.layers import summaries as summ

from .model import image_pyramid, pyramid_network, mlp_classif
from .training import optimizers, default_optimizer_params, set_training

class MultiscaleConvNet(Estimator):

    default_params = dict(
            n_hidden=1024,
            half_feat=True,
            dropout=False,
            activation='tanh',
            pooling='MAX',
            pooling_stride = [2, 2],
            pooling_window = [2, 2],
            regL2=1e-5,
            regL1=0,
            tv_loss=0,
            optimizer='GradientDescent',
            optimizer_params=dict(learning_rate=1e-3),
            with_laplacian=False,
            with_inv=False,
            with_param_k=False)

    def __init__(self, model_dir, params=None, config=None):
        Estimator.__init__(self, model_fn=_model_fn,
                           model_dir=model_dir,
                           config=config,
                           params=params,
                           feature_engineering_fn=feature_engineering_fn)


def feature_engineering_fn(X, y):
    """"Check and ensure expected shapes for X and y"""
    def _ensure_rank(X, rank):
        shape = X.get_shape().as_list()
        if len(shape) == rank-1:
            shape = [-1] + shape
            X = tf.reshape(X, shape)
        return X
    X = _ensure_rank(X['X'], 4)
    y = _ensure_rank(y, 3)
    return X, y


def _model_fn(X, y, mode, params, config=None):
    if params is None:
        params = MultiscaleConvNet.default_params()
    y_pred_labels, train_step = _multiscale_net(X, y, **params)
    return ModelFnOps(mode=mode,
                      predictions=y_pred_labels,
                      loss=tf.losses.get_total_loss(),
                      train_op=train_step,
                      eval_metric_ops=None,
                      output_alternatives=None,
                      training_chief_hooks=None,
                      training_hooks=None)


def _multiscale_net(X, y,
                    n_classes=None,
                    n_hidden=1024,
                    half_feat=True,
                    dropout=False,
                    activation='tanh',
                    pooling='MAX',
                    pooling_stride = [2, 2],
                    pooling_window = [2, 2],
                    regL2=1e-5,
                    regL1=0,
                    tv_loss=0,
                    optimizer='GradientDescent',
                    optimizer_params=dict(learning_rate=1e-3),
                    with_laplacian=False,
                    with_inv=False,
                    with_param_k=False):
    '''
    X: 4D tensor
    y: 3D tensor with numeric labels
    '''
    HEIGHT, WIDTH = tuple(X.get_shape().as_list()[1:3])

    pyramid = image_pyramid(X, (HEIGHT, WIDTH), with_laplacian)
    output = pyramid_network(pyramid,
                             activation=activation,
                             dropout=dropout,
                             pooling=pooling,
                             pooling_stride=pooling_stride,
                             pooling_window=pooling_window,
                             with_inv=with_inv,
                             with_param_k=with_param_k)
    y_pred, y_pred_labels = mlp_classif(output, image_shape=(HEIGHT, WIDTH),
                                        activation=activation,
                                        n_hidden=n_hidden,
                                        n_classes=n_classes)
    # y_pred = avg_segments(y_pred, segmentation)
    train_step, y_pred_labels = set_training(
            y, y_pred,
            regL2, regL1, tv_loss,
            optimizer, optimizer_params)


    return y_pred_labels, train_step
