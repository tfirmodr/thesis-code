
# coding: utf-8


import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Circle
import os
from tqdm import tqdm
from sklearn.linear_model import LogisticRegressionCV
from sklearn.model_selection import (RepeatedStratifiedKFold, StratifiedShuffleSplit,
                                     PredefinedSplit)
from sklearn.base import BaseEstimator, ClassifierMixin, TransformerMixin
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from scipy.stats import ttest_ind, wilcoxon
from sklearn.cluster import KMeans
from sklearn.metrics import accuracy_score

from sklearn.neighbors import NearestCentroid
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import StratifiedKFold
from sklearn.model_selection import GridSearchCV
from sklearn.pipeline import Pipeline, FeatureUnion
from tempfile import mkdtemp
from shutil import rmtree
from sklearn.externals.joblib import Memory

import pandas as pd
import itertools
from sklearn.decomposition import PCA
from sklearn.manifold import TSNE, Isomap
from itertools import combinations
from scipy.stats import ttest_ind, wilcoxon
from scipy.stats import friedmanchisquare
from stac import friedman_test

from keras.datasets import cifar10

from sklearn.preprocessing import StandardScaler
from prototype_plots import plot_montage

random_state=10
N=10


def plot2D(X, y, y_pred=None, method='pca', n_components=2,
           plot_n_samples=None,**method_kwargs):
    global N
    if method == 'pca':
        estim = PCA(n_components, **method_kwargs)
    elif method == 'tsne':
        estim = TSNE(n_components=2,**method_kwargs)
        if plot_n_samples is None:
            plot_n_samples = 100
    elif method == 'lda':
        estim = LinearDiscriminantAnalysis(n_components=2,**method_kwargs)
    elif method == 'isomap':
        estim = Isomap(n_components=2, **method_kwargs)

    # subsample points for plots so it doesn't take forever
    if plot_n_samples is not None:
        subsample = np.random.choice(np.arange(X.shape[0]),
                                 size=plot_n_samples,
                                 replace=False)
        X, y = X[subsample], y[subsample]
        if y_pred is not None:
            y_pred = y_pred[subsample]


    if X.shape[-1] > 2:
        if method == 'lda':
             X_new = estim.fit_transform(X, y)
        else:
            X_new = estim.fit_transform(X)
        if method == 'pca':
            print("explained variance ", estim.explained_variance_)
    else:
        X_new = X
    def subplot(i, j):
        colors = []
        for k in range(N):
            if y_pred is not None:
                ms=10
            else:
                ms=7
            points = np.ix_(y==k)
            lines = plt.plot(X_new[points,i].squeeze(),X_new[points,j].squeeze(), label=str(k+1),
                     marker='o', linestyle='', ms=ms)
            colors.append(lines[0].get_color())
        if y_pred is not None:
            for k in range(N):
                points = np.ix_((y_pred==k) * (y_pred != y))
                plt.plot(X_new[points,i].squeeze(),X_new[points,j].squeeze(),
                         marker='X', linestyle='', ms=7, color=colors[k],
                         markeredgecolor='k', markeredgewidth=0.2)
        plt.legend(loc='best', fontsize='small', title='classes')
    plt.figure()
    dim_pairs = list(combinations(range(n_components), 2))
    nplots = len(dim_pairs)
    nrows = (nplots-1) // 3 + 1
    ncols = min(nplots, 3)
    for p in range(nplots):
        plt.subplot(nrows, ncols, p+1)
        subplot(*dim_pairs[p])
        if nplots > 1:
            plt.title('components %d x %d' % dim_pairs[p])

    if X.shape[-1] > 2:
        return estim


def circle(x, y, radius=0.15):
    from matplotlib.patches import Circle
    from matplotlib.patheffects import withStroke
    circle = Circle((x, y), radius, clip_on=False, zorder=10, linewidth=1,
                    edgecolor='black', facecolor=(0, 0, 0, .0125), linestyle=':',
                    path_effects=[withStroke(linewidth=5, foreground='w')])
    plt.gca().add_artist(circle)


def sparsity(mat, tol=1e-6):
    return 1-np.sum(np.abs(mat)>tol)/mat.size


def gen_cifar10(N=10, plot=True, random_seed=None, random_state=None):
    if random_seed is not None:
        np.random.seed(random_seed)
    elif random_state is not None:
        np.random.set_state(random_state)

    (x_train, y_train), (x_test, y_test) = cifar10.load_data()
    X_imgs = images = np.concatenate([x_train,x_test])
    X = np.load('./Data/cifar10_inceptionV3_features.npy')
    y = np.concatenate([y_train, y_test]).squeeze()
#    if N <10:
#        class_subset_mask = np.isin(y, classes)
#        X = X[class_subset_mask]
#        X_imgs = X_imgs[class_subset_mask]
#        y = y[class_subset_mask]

    if plot:
        plot_montage(images)

    return X_imgs, X, y



class StratifiedShufflePredefinedTestSplit(StratifiedShuffleSplit, PredefinedSplit):
    def __init__(self, test_fold, train_size=0.1, n_splits=1, random_state=None):
        StratifiedShuffleSplit.__init__(self, n_splits=1,
                                        train_size=train_size,
                                        random_state=random_state)
        PredefinedSplit.__init__(self, test_fold)

    def split(self, X, y):
        train, test = next(PredefinedSplit.split(self, X, y))
        train, _ = next(StratifiedShuffleSplit.split(self, X[train], y[train]))
        yield train, test


def spliters(random_state, train_size=100, n_splits=1, cv_folds=2, cv_repeats=1):
    global skf, skf_cv
    test_fold = np.array(50000*[-1]+10000*[0])
    skf = StratifiedShufflePredefinedTestSplit(test_fold, train_size,
                                               n_splits, random_state)
    skf_cv = RepeatedStratifiedKFold(n_splits=cv_folds, n_repeats=cv_repeats,
                                     random_state=None)
    return skf, skf_cv

#%%
def softmax(x, axis=0):
    """Compute softmax values for each sets of scores in x."""
    return np.exp(x) / np.sum(np.exp(x), axis=axis)

class MyKMeans(KMeans):
    """
    Scikit learn Pipeline will call fit_transform for this one.
    fit_transform in kmeans calls fit(X)._transform(X)
    Calling predict(X) still returns clusters based on simple distances.
    """
    def __init__(self, n_clusters=3, class_weight=0.0,
                 init='k-means++', n_init=10,
                 max_iter=300, tol=1e-4, precompute_distances='auto',
                 verbose=0, random_state=None, copy_x=True,
                 n_jobs=1, algorithm='auto'):
        KMeans.__init__(self, n_clusters, init, n_init,
                 max_iter, tol, precompute_distances,
                 verbose, random_state, copy_x,
                 n_jobs, algorithm)
        self.class_weight = class_weight

    def fit(self, X, y):
        if self.class_weight > 0.0:
            n_classes = np.max(y) + 1
            n_dim = X.shape[1]
            y_one_hot = self.class_weight * np.eye(n_classes)[y]
            X = np.concatenate([X,y_one_hot], axis=1)
            KMeans.fit(self, X)
            self.cluster_centers_class_ = self.cluster_centers_[:, n_dim:]
            self.cluster_centers_ = self.cluster_centers_[:, :n_dim]
        else:
            KMeans.fit(self, X)
        return self

    def fit_transform(self, X, y):
        return self.fit(X,y)._transform(X)

    def distances(self, X):
        return KMeans._transform(self, X)

    def predict(self, X):
#         if self.class_weight > 0.0:
#             X = np.concat([X,np.zeros([X.shape[0],self.num_clusters])], axis=1)
        return KMeans.predict(self, X)

    def _transform(self, X):
#         if self.class_weight > 0.0:
#             X = np.concat([X,np.zeros([X.shape[0],self.num_clusters])], axis=1)
        X_new = KMeans._transform(self, X)
        X_new = np.array(softmax(-X_new), ndmin=2)
        return_dict = dict(X_old=X, X=X_new, protos=self.cluster_centers_)
        if self.class_weight > 0.0:
            cluster_classes = np.argmax(self.cluster_centers_class_, axis=1)
            return_dict.update(cluster_classes=cluster_classes)
        return return_dict

    def transformX(self, X):
        self.transform(X)['X']


class TopClassifier(LogisticRegression):
    def __init__(self, use_shortcut=False, shortcut=0.0,
                 predict_from_cluster=False,
                 penalty='l2', dual=False, tol=1e-4, C=1.0,
                 fit_intercept=True, intercept_scaling=1, class_weight=None,
                 random_state=None, solver='liblinear', max_iter=100,
                 multi_class='ovr', verbose=0, warm_start=False, n_jobs=1):
        LogisticRegression.__init__(
                self, penalty, dual, tol, C,
                 fit_intercept, intercept_scaling, class_weight,
                 random_state, solver, max_iter,
                 multi_class, verbose, warm_start, n_jobs)
        self.shortcut = shortcut
        self.use_shortcut = use_shortcut
        self.predict_from_cluster = predict_from_cluster

    def _read_features(self, features):
        X = features['X']
        if self.use_shortcut:
            # weight for shortcut is alpha * X_old + (1-alpha) X_tilde
            # X_tilde is X*protos, because X is acctualy q(x)
            X_shortcut = self.shortcut * features['X_old']
            X_tilde = np.matmul(X, features['protos'])
            X = (1-self.shortcut) * X_tilde
            X = X + X_shortcut
        return X

    def fit(self, features, y, **fit_params):
        if self.predict_from_cluster and 'cluster_classes' in features:
            self.cluster_classes_ = features['cluster_classes']
            return self

        X = self._read_features(features)
        return LogisticRegression.fit(self, X, y, **fit_params)

    def predict(self, features):
        X = self._read_features(features)
        if self.predict_from_cluster:
            cluster_indices = np.argmax(X, axis=1)
            return self.cluster_classes_[cluster_indices]

        return LogisticRegression.predict(self, X)

    def predict_proba(self, features):
        X = self._read_features(features)
        if self.predict_from_cluster:
            cluster_indices = np.argmax(X, axis=1)
            return softmax(self.cluster_classes_[cluster_indices], axis=1)

        return LogisticRegression.predict_proba(self, X)


class MultiPrototypes(Pipeline):
    def __init__(self, random_state, verbose=0, cachedir=None, **kwargs):
        pipeline = [
                ('scaler', StandardScaler()),
                ('cluster',
                 MyKMeans(n_clusters=20, random_state=random_state))]
        pipeline.append(
            ('classify',
             TopClassifier(random_state=random_state,
                           multi_class='multinomial',
                           solver='lbfgs',
                           fit_intercept=False))
        )
        # Create a temporary folder to store the transformers of the pipeline
        self.cachedir = cachedir if cachedir else mkdtemp(dir='/mnt/Data/tmp')
        memory = Memory(cachedir=self.cachedir, verbose=verbose)
        Pipeline.__init__(self, pipeline, memory=memory)
        self.set_params(**kwargs)

    def clear_cache(self):
        rmtree(self.cachedir)



class Prototypical(MyKMeans):
    def fit_transform(self, X, y):
        return self.fit(X,y).transform(X)
    def fit(self, X, y):
        prototypes = []
        prototype_class = []
        for k in range(self.n_clusters):
            points = np.ix_(y==k)
            if len(points[0]) == 0:
                raise ValueError("no samples for class %d, n_clusters=%d" % (k, self.n_clusters))
            proto = X[points].mean(axis=0)
            prototypes.append(proto)
            prototype_class.append(k)
        self.cluster_centers_ = np.array(prototypes)
        return self
    def score(self, X, y):
        accuracy_score(y, self.predict(X))


def classify_softmax(X, y, random_state=0, n_Cs=10,
                     return_model=False, verbose=True):
    global N, skf, skf_cv
    acc=[]
    models = []
    for train_index, test_index in skf.split(X, y):
        X_train, X_test = X[train_index], X[test_index]
        y_train, y_test = y[train_index], y[test_index]
        cv=skf_cv
        classif = LogisticRegression(multi_class='multinomial',
                                     solver='lbfgs',
                                     random_state=random_state)
        param_grid = [{'C': np.logspace(-4, +4, n_Cs, base=10)}]
        model = GridSearchCV(classif, param_grid, cv=skf_cv,
                             scoring='accuracy',
                             verbose=1,
                             n_jobs=2, return_train_score=True)
#        model = LogisticRegressionCV(cv=cv, random_state=random_state,
#                                     multi_class='multinomial')
        model.fit(X_train,y_train)
        acc.append(model.score(X_test, y_test))
        models.append(model)
        if verbose: print("accuracy on test", model.score(X_test, y_test))
    if verbose: print("average test accuracy", np.mean(acc), np.std(acc))
    if return_model:
        return np.mean(acc), models
    else:
        return np.mean(acc)


def classify_prototypical(X, y, random_state=0,
                          return_model=False, verbose=True):
    global N, skf, skf_cv
    acc=[]
    models=[]
    for train_val_index, test_index in skf.split(X, y):
        X_train_val, X_test = X[train_val_index], X[test_index]
        y_train_val, y_test = y[train_val_index], y[test_index]
#        for train_index, val_index in skf_cv.split(X_train_val,y_train_val):
#            X_train, X_val = X_train_val[train_index], X_train_val[val_index]
#            y_train, y_val = y_train_val[train_index], y_train_val[val_index]
        model = NearestCentroid()
        model.fit(X_train_val, y_train_val)
        pred = model.predict(X_test)
        acc.append(accuracy_score(y_test, pred))
        if verbose: print("accuracy on test", acc[-1])
        models.append(model)
    if verbose: print("average test accuracy", np.mean(acc), np.std(acc))
    if return_model:
        return np.mean(acc), models
    else:
        return np.mean(acc)


def classify_cluster_softmax(X, y,
                             n_Cs=10, Cs_lim=(2, 3),
                             n_n_clusters=20, n_clusters_lim=(1, 3),
                             n_class_weights=10, class_weight_lim=(0.0, 3.0),
                             n_shortcut=10, shortcut_lim=(-4, 0), shortcut_scale='log',
                             random_state=0,
                             models_to_test=None,
                             return_model=False, verbose=True):
    global N, skf, skf_cv
    pipeline = [('cluster',
                 MyKMeans(n_clusters=2*N, random_state=random_state))]
    pipeline.append(
        ('classify',
         TopClassifier(random_state=random_state,
                       multi_class='multinomial',
                       solver='lbfgs',
                       fit_intercept=False))
    )

    # Create a temporary folder to store the transformers of the pipeline
    cachedir = mkdtemp(dir='/mnt/Data/tmp')
    memory = Memory(cachedir=cachedir, verbose=0)
    pipeline = Pipeline(pipeline, memory=memory)


    C_space = np.logspace(*Cs_lim, num=n_Cs, base=10)
    n_clusters_lim = tuple(N*np.array(n_clusters_lim))
    n_clusters_space = np.array(np.linspace(*n_clusters_lim, num=n_n_clusters),
                                dtype=int)
    class_weight_space = np.linspace(*class_weight_lim, n_class_weights)
    if shortcut_scale == 'linear':
        shortcut_space = np.linspace(*shortcut_lim, num=n_shortcut)
    elif shortcut_scale == 'log':
        shortcut_space = np.logspace(*shortcut_lim, n_shortcut, base=10)
    # multinomial is available on lbfgs and saga solvers
    # lbfgs does not handle l1
    cluster_params_dict = {
            'cluster__n_clusters': n_clusters_space,
            'cluster__class_weight': class_weight_space,
        }

    param_grid_l2 = {
            'classify__penalty': ['l2'],
            'classify__solver': ['lbfgs'],
            'classify__C': C_space,
            'classify__shortcut': shortcut_space,
            'classify__use_shortcut': [True]
        }
    param_grid_l2.update(cluster_params_dict)

    param_grid_l2_shortcut = param_grid_l2.copy()
    param_grid_l2_shortcut.update({'cluster__class_weight': [0.0]})

    param_grid_l2_classweight = param_grid_l2.copy()
    param_grid_l2_classweight.update({'classify__shortcut': [0.0],
                                     'classify__use_shortcut': [False]})

    param_grid_l2_plain = param_grid_l2_shortcut.copy()
    param_grid_l2_plain.update({'classify__shortcut': [0.0],
                                'classify__use_shortcut': [False]})

    param_grid_l1 = {
            'classify__penalty': ['l1'],
            'classify__solver': ['saga'],
            'classify__tol': [0.1],
            'classify__C': C_space,
            'classify__shortcut': shortcut_space,
            'classify__use_shortcut': [True]
        }
    param_grid_l1.update(cluster_params_dict)

    param_grid_l1_shortcut = param_grid_l1.copy()
    param_grid_l1_shortcut.update({'cluster__class_weight': [0.0]})

    param_grid_l1_classweight = param_grid_l1.copy()
    param_grid_l1_classweight.update({'classify__shortcut': [0.0],
                                     'classify__use_shortcut': [False]})

    param_grid_l1_plain = param_grid_l1_shortcut.copy()
    param_grid_l1_plain.update({'classify__shortcut': [0.0],
                                'classify__use_shortcut': [False]})

    direct_classify_grid = {
            'cluster__n_clusters': n_clusters_space,
            'cluster__class_weight': class_weight_space[1:],
            'classify__shortcut': [0],
            'classify__use_shortcut': [False],
            'classify__predict_from_cluster': [True],
        }

    params = dict(
        l1=param_grid_l1,
        l1_classweight=param_grid_l1_classweight,
        l1_shortcut=param_grid_l1_shortcut,
        l1_plain=param_grid_l1_plain,
        l2=param_grid_l2,
        l2_classweight=param_grid_l2_classweight,
        l2_shortcut=param_grid_l2_shortcut,
        l2_plain=param_grid_l2_plain,
        direct=direct_classify_grid)


    def run(param_grid):
        param_grid = [param_grid]
        acc=[]
        models = []
        for train_index, test_index in skf.split(X, y):
            X_train, X_test = X[train_index], X[test_index]
            y_train, y_test = y[train_index], y[test_index]
            model = GridSearchCV(pipeline, param_grid, cv=skf_cv,
                                 verbose=1,
                                 scoring='accuracy',
                                 n_jobs=2, return_train_score=True)
            model.fit(X_train, y_train)
            #print(model.cv_results_)
            acc += [model.best_estimator_.score(X_test, y_test)]
            models += [model]
            if verbose:
                print("best Params", model.best_params_)
                print("mean acuracy of best cv model: ", model.best_score_)
                print("accuracy on test", acc[-1])
        if verbose:
            print("average test accuracy", np.mean(acc), np.std(acc))
        if return_model:
            return np.mean(acc), models
        else:
            return np.mean(acc)

    result = {}
    if models_to_test is not None:
        for model in models_to_test:
            result.update({model: run(params[model])})
    else:
        # if none, defaults to running all
        result = dict(
                l1=run(param_grid_l1),
                l1_noclassweight=run(param_grid_l1_classweight),
                l1_noshortcut=run(param_grid_l1_shortcut),
                l1_plain=run(param_grid_l1_plain),
                l2=run(param_grid_l2),
                l2_noclassweight=run(param_grid_l2_classweight),
                l2_noshortcut=run(param_grid_l2_shortcut),
                l2_plain=run(param_grid_l2_plain),
                direct=run(direct_classify_grid))

    # Delete the temporary cache before exiting
    rmtree(cachedir)
    return result


#%%

def transform_label(l):
    l = l.replace('param_cluster__', '')
    l = l.replace('param_classify__', '')

    label_map = {'n_clusters': r'n clusters J',
                 'C': 'inverse regularization C',
                 'shortcut': r'$\alpha$',
                 'class_weight': r'class weight $\beta$'}

    for key, v in label_map.items():
        l = l.replace(key, v)
    return l


class Report:
    def __init__(self, name, result_dict, model, split=0,
                 directory='Cifar10InceptionV3ClusterAndClassify'):
        self.model = result_dict[model][1][split]
        self.split = split
        self.use_shortcut = self.model.best_estimator_.named_steps.classify.use_shortcut
        self.proto_class_proba()
        self.wc_shortcuts()
        self.report_path = os.path.join(directory, name, model)
        os.makedirs(self.report_path, exist_ok=True)

    def savefig(self, fname, comment=None, **savefig_kwargs):
        fname = '_'.join([self.report_path, fname])
        plt.savefig(fname, dpi=300, **savefig_kwargs)
        if fname.endswith('.eps'):
            metainfo = '\n%% model best_cv_params: ' + str(self.model.best_params_)
            metainfo += '\n%% test split: %d' % self.split
            if comment is not None:
                metainfo += '\n%% ' + comment
            with open(fname, 'a') as f:
                f.write(metainfo)

    def report(self, X, y):
        model = self.model
        print("best Params", model.best_params_)
        print("mean acuracy of best cv model: ", model.best_score_)
        train,test = self._split(X,y)
        print("holdout test accuracy: ", model.score(X[test], y[test]))


    def main_parameter_grids(self, fix='all'):
        plots = [dict(index=['param_classify__C'],
                      columns=['param_cluster__n_clusters'],fix=fix)]

        plots.append(dict(
            index=['param_cluster__n_clusters'],
            columns=['param_classify__C'],fix=fix))

        for p in plots:
            self.parameter_grid(**p)


    def parameter_grid(self, index, columns, fix=None):
        model = self.model
        penalty = model.best_params_['classify__penalty']
        possible_columns = set(['param_classify__C',
                                'param_cluster__n_clusters',
                                'param_cluster__class_weight',
                                'param_classify__shortcut'])
        df = pd.DataFrame(model.cv_results_)
        if fix == 'all':
            fix = possible_columns - set(index+columns)
        elif fix is None:
            fix = []

        for col in fix:
            v = model.best_params_[col.strip('param_')]
            if isinstance(fix, dict):
                if fix[col] != 'best':
                    v = fix[col]
            df = df.loc[df[col]==v, :]

        t = pd.pivot_table(
            df, values='mean_test_score',
            index=index,
            columns=columns,
            aggfunc=np.mean)

        unused_columns = possible_columns - set(index+columns) - set(fix)
        if len(unused_columns) > 0:
            terr =  pd.pivot_table(
                df, values='mean_test_score',
                index=index,
                columns=columns,
                aggfunc=np.std)
        else:
            terr = pd.pivot_table(
                df, values='std_test_score',
                index=index,
                columns=columns,
                aggfunc=np.max)

        if len(index) == 1:
            t.plot(style=[':+',':d',':.',':x']*20, yerr=terr)
        else:
            t.plot.bar(yerr=terr)


        plt.title('mean accuracy %s' % penalty.capitalize())
        if index[0] in ['param_classify__C', 'param_classify__shortcut']:
            plt.xscale('log')
        ax = plt.gca()
        ax.set_xlabel(transform_label(ax.get_xlabel()))

        handles, labels = ax.get_legend_handles_labels()
        if 'param_classify__C' in columns:
            labels = ['%0.0e' % float(l) for l in labels]
        new_title = transform_label(ax.get_legend().get_title().get_text())
        ax.legend(handles, labels,title=new_title)
        return df,t, terr


    def wc_shortcuts(self):
        coef = self.model.best_estimator_.named_steps.classify.coef_
        if self.use_shortcut:
            self.Wc = coef[:, 0:self.n_proto]
            self.shortcut_coef = coef[:, self.n_proto:]
        else:
            self.Wc = coef
            self.shortcut_coef = [0]

    def proto_class_proba(self):
        model = self.model
        self.protos = model.best_estimator_.named_steps.cluster.cluster_centers_
        self.n_proto = model.best_estimator_.named_steps.cluster.n_clusters
        feature = dict(X=np.eye(self.n_proto), X_old=self.protos,
                       protos=self.protos)

        self.cluster_class = (model.best_estimator_.named_steps.
                         classify.predict(feature)) # classes x centers
        self.cluster_proba = (model.best_estimator_.named_steps.
                         classify.predict_proba(feature)) # centers x classes
    def wc_stats(self):
        model = self.model
        print('sparsity @ tol=0.1', sparsity(self.Wc, tol=0.1))
        print('mean: ', self.Wc.mean(), 'std: ', self.Wc.std())

    def params(self):
        print("parameters: ", self.model.best_params_)

    def visualize_weights(self):
        model = self.model
        n_proto = model.best_estimator_.named_steps.cluster.n_clusters
        plt.figure()
        ncols = 3 if self.use_shortcut else 2
        plt.subplot(1,ncols,2)
        # Wc is n_class x n_clusters
        plt.imshow(self.Wc.T, cmap="RdYlBu_r")
        plt.title('Wc')
        plt.xlabel('classes')
        plt.ylabel('prototypes')
        plt.xticks(np.arange(0,N), np.arange(1, N+1))
        plt.yticks(np.arange(0,n_proto), np.arange(1, n_proto+1))
        plt.colorbar()
        plt.subplot(1,ncols,1)
        plt.imshow(self.cluster_proba, cmap="RdYlBu_r")
        plt.title('probas')
        plt.colorbar()
        plt.xlabel('classes')
        plt.ylabel('prototypes')
        plt.xticks(np.arange(0,N), np.arange(1, N+1))
        plt.yticks(np.arange(0,n_proto), np.arange(1, n_proto+1))
        if self.use_shortcut:
            plt.subplot(1, ncols, ncols)
            plt.imshow(self.shortcut_coef.T, cmap="RdYlBu_r")
            plt.title('coeficients for shortcut dimensions')
            plt.axis('tight')
            plt.xlabel('classes')
            plt.ylabel('original feature dimensions')
            plt.colorbar()
        print('proto per class: ', self.cluster_proba.sum(axis=0))
        plt.tight_layout(pad=0.2)


    def _split(self, X, y):
        global skf
        f=0
        for train_index, test_index in skf.split(X, y):
            if f == self.split:
                return train_index, test_index
            elif f > self.split:
                raise ValueError("self.split had invalid value: " + str(self.split))
            f += 1

    def closest_images(self, X_imgs, X, y, show_distances=True):
        model = self.model
        train_index, test_index = self._split(X, y)
        n_proto = model.best_estimator_.named_steps.cluster.n_clusters
        X_train, X_test = X[train_index], X[test_index]
        imgs = X_imgs[train_index]
        labels = y[train_index]
        preds = model.best_estimator_.predict(X_train)
        distances = model.best_estimator_.named_steps.cluster.distances(X_train)
        cluster_idx = model.best_estimator_.named_steps.cluster.predict(X_train)
        cluster_class = np.argmax(self.cluster_proba, axis=1)
        closest, closest_d,closest_l, farthest, farthest_d = [],[],[],[],[]
        used_protos = np.zeros(n_proto)
        for c in range(n_proto):
            candidates = np.where(cluster_idx==c)[0]
            used_protos[c] = candidates.size
            if candidates.size > 0:
                farthest_idx = candidates[np.argmax(distances[candidates, c], axis=0)]
                closest_idx = candidates[np.argmin(distances[candidates, c], axis=0)]
            else:
                farthest_idx = np.argmax(distances[:,c])
                closest_idx = np.argmin(distances[:,c])

            farthest.append(imgs[farthest_idx,...])
            farthest_d.append(distances[farthest_idx,c])
            closest.append(imgs[closest_idx,...])
            closest_l.append((labels[closest_idx,...], preds[closest_idx,...]))
            closest_d.append(distances[closest_idx,c])

        ncols = 8
        nrows = int(np.ceil(len(used_protos)/ncols))
        ix = 0
        plt.figure()
        for i in range(nrows):
            for j in range(ncols):
                if ix < n_proto:
                    plt.subplot2grid((nrows, ncols), (i,j))
                    plt.imshow(closest[ix], cmap='Greys_r')
                    l_true, l_pred = closest_l[ix]
                    xlabel = 'true c %d pred %d' % (l_true+1,l_pred+1) if l_true != l_pred else ''
                    plt.xlabel(xlabel, fontsize='small')
                    title = 'p %d - c %d' % (ix+1, cluster_class[ix]+1)
                    title += '\n%d samples' % (used_protos[ix])
                    plt.title(title,
                              fontsize='small')
                    plt.tick_params(which='both', length=0, labelbottom=False, labelleft=False)
                    if show_distances:
                        ylabel = '$d_{min}=%0.3f$\n$d_{max}=%0.3f$' % (closest_d[ix], farthest_d[ix])
                        plt.ylabel(ylabel, fontsize='small')
                ix += 1
        plt.tight_layout(pad=0.2)

    def plot2D(self, X, y, split='train', plot_n_samples=200,
               cluster_limits=False, arrows=False,
               method='pca', **method_kwargs):
        model = self.model
        train_index, test_index = self._split(X, y)
        X_train, X_test = X[train_index], X[test_index]
        y_train, y_test = y[train_index], y[test_index]
        if split=='test':
            X_train, y_train = X_test, y_test
        elif split=='both':
            X_train = np.concatenate((X_train, X_test), axis=0)
            y_train = np.concatenate((y_train, y_test), axis=0)

        y_pred = model.best_estimator_.predict(X_train)
        n_proto = model.best_estimator_.named_steps.cluster.n_clusters
        distances = model.best_estimator_.named_steps.cluster.distances(X_train)
        cluster_idx = model.best_estimator_.named_steps.cluster.predict(X_train)
        # projection
        clusters = model.best_estimator_.named_steps.cluster.cluster_centers_
        if method == 'tsne':
            proj = TSNE(n_components=2, **method_kwargs)
            data_clusters = np.concatenate([X_train, clusters])
            data_clusters = proj.fit_transform(data_clusters)
            data, clusters = data_clusters[:X_train.shape[0],...], data_clusters[X_train.shape[0]:,...]
            plot2D(data, y_train, y_pred, plot_n_samples=plot_n_samples)
        else:
            pca = plot2D(X_train, y_train, y_pred, **method_kwargs)
            data = pca.transform(X_train)
            clusters = pca.transform(clusters)

        for c in range(n_proto):
            marker = '.'
            candidates = np.where(cluster_idx==c)[0]  # returns a tuple
            if candidates.size > 0:
                marker = 'd'
                farthest_idx = candidates[np.argmax(distances[candidates, c], axis=0)]
                X_farthest = data[farthest_idx,:]
                closest_idx = candidates[np.argmin(distances[candidates, c], axis=0)]
                X_closest = data[closest_idx,:]
                def radius(X):
                    r = np.linalg.norm(clusters[c,:]-X)
                    circle(clusters[c,0],clusters[c,1],r)
                def arrow(X,prefix):
                    if cluster_limits:
                        s = prefix+"%d" % (c+1)
                    else:
                        s = ' '
                    plt.annotate(s=s,# xy=X)
                                 xy=(clusters[c,:]),xytext=X,
                                 arrowprops=dict(arrowstyle='<-'),
                                 fontsize='xx-small')
                if candidates.size >= 2 and arrows:
                    arrow(X_farthest,'f')
                    arrow(X_closest,'n')
                elif arrows:
                    arrow(X_closest,'u')


            plt.plot(clusters[c,0], clusters[c,1], ms=5,
                     marker=marker, linewidth=0, color='k')

            proba = self.cluster_proba[c,:]
            top = np.argsort(proba)[-2:][::-1]
            top_p = proba[top]
            s = " p%d"
            if not arrows:
                s += '\n '
            if top_p[0] < 0.95:
                s += ','.join(["c%d:%0.2f" % (ix, p) for ix, p in zip(top+1, top_p)])
            else:
                s += 'c%d' % (top[0]+1)
            plt.annotate(xy=(clusters[c,:]),
                         s=s%(c+1), fontsize='medium')
        plt.axis("off")



#%%
def make_dataframe(res):
    """Make dataframe with a single entry from a result entry from the
    dict returned by classify_cluster_softmax"""
    acc, models = res
    model = models[0]
    dict_info = model.cv_results_
    df = pd.DataFrame(dict_info)
    # repeat index to return dataframe and not series
    df = df.loc[model.best_index_:model.best_index_]
    df = df.assign(holdout_test_accuracy=[acc])
    return df


def run_one_split(X, y, model_list):
    global N, random_state

    result = dict()
    if 'single_prototype' in model_list:
        res_proto = classify_prototypical(X, y, random_state=random_state,
                                          return_model=True, verbose=False)
        df_proto = pd.DataFrame(dict(holdout_test_accuracy=[res_proto[0]]))
        result.update({'single_prototype': df_proto})

    if 'softmax':
        res_softmax = classify_softmax(
                X, y, n_Cs=5,
                random_state=random_state,
                return_model=True, verbose=False)
        result.update({'softmax': make_dataframe(res_softmax)})

    model_list = list(set(model_list) - {'single_prototype','softmax'})
    res_cluster_classify = classify_cluster_softmax(
        X, y, n_Cs=3, Cs_lim=(0, 2),
        n_n_clusters=3, n_clusters_lim=(8, 10),
        n_class_weights=4, class_weight_lim=(0.0, 50),
        n_shortcut=1, shortcut_lim=(0, 1), shortcut_scale='linear',
        random_state=random_state,
        models_to_test=model_list,
        return_model=True,
        verbose=False)

    for k, v in res_cluster_classify.items():
        result[k] = make_dataframe(v)

    return result


def get_or_load_data_random_states(n_episodes, path):
    """Generates `n_episodes` random states for numpy RNG and saves to
       `path`/data_random_states.pkl, or loads them if file exists
    """
    drs_file = os.path.join(path,'data_random_states.pkl')
    if not os.path.exists(drs_file):
        data_random_states = []
        for _ in range(n_episodes):
            state = np.random.RandomState().get_state()
            data_random_states.append(state)
        # save data random states
        pd.to_pickle(data_random_states, drs_file)
    else:
        data_random_states = pd.read_pickle(drs_file)
    return data_random_states


def run_multiple_splits(model_list, train_size=100,
                        n_splits=10, overwrite=None):
    """Run mutiple `n_splits` with `train_size` samples
    for models in `model_list`. Loads existing results file and appends to it.
    Overwrite is either None or an iterable with models names to be re-run,
    overwriting existing results.

    """
    global N, skf, skf_cv, random_state

    np.random.seed(random_state)
    skf, skf_cv = spliters(random_state, train_size, cv_repeats=5)
    X_imgs, X, y = gen_cifar10(plot=False)

    path = 'Cifar10InceptionV3ClusterAndClassify'
    path = os.path.join(path, "{}_splits_{}_training".format(n_splits, train_size))
    if not os.path.exists(path):
        os.makedirs(path)

    results_file = os.path.join(path, 'results.pd.pkl')
    results = dict()
    # if there is a file, load its results
    if os.path.exists(results_file):
        results = pd.read_pickle(results_file)
        # check which models to compute
        model_list = set(model_list) - set(results.keys())
        if overwrite is not None:
            model_list = list(model_list.union(overwrite))
            for item in overwrite:
                del results[item]
        else:
            model_list = list(model_list)
    if model_list != []:
        # run episodes appending to existing dict
        for i in tqdm(range(0, n_splits), "splits", total=n_splits):
            res_dict = run_one_split(
                    X, y,
                    model_list=model_list)
            for k in res_dict:
                if k not in results:
                    results[k] = res_dict[k]
                else:
                    results[k] = results[k].append(
                        res_dict[k], ignore_index=True)
        #save results and data random states
        pd.to_pickle(results, results_file)
    return results

def run():
    model_list=['l1_plain', 'l2_plain', 'l2_shortcut', 'l2_classweight']
    run_multiple_splits(model_list, 200)
    run_multiple_splits(model_list, 300)
    run_multiple_splits(model_list, 400)


#%%
def nemenyi_multitest(ranks):
    """
        From STAC library, BSD 2 license.
        See:
        - https://github.com/citiususc/stac
        - https://gitlab.citius.usc.es/ismael.rodriguez/stac
        ------------------------------------------------------------------------
        Performs a Nemenyi post-hoc test using the pivot quantities obtained by a ranking test.
        Tests the hypothesis that the ranking of each pair of groups are different.

        Parameters
        ----------
        pivots : dictionary_like
            A dictionary with format 'groupname':'pivotal quantity'

        Returns
        ----------
        Comparions : array-like
            Strings identifier of each comparison with format 'group_i vs group_j'
        Z-values : array-like
            The computed Z-value statistic for each comparison.
        p-values : array-like
            The associated p-value from the Z-distribution wich depends on the index of the comparison
        Adjusted p-values : array-like
            The associated adjusted p-values wich can be compared with a significance level

        References
        ----------
        Bonferroni-Dunn: O.J. Dunn, Multiple comparisons among means, Journal of the American Statistical Association 56 (1961) 52–64.
    """
    import itertools as it
    import scipy as sp
    import scipy.stats as st

    k = len(ranks)
    values = list(ranks.values())
    keys = list(ranks.keys())
    versus = list(it.combinations(range(k), 2))

    comparisons = [keys[vs[0]] + " vs " + keys[vs[1]] for vs in versus]
    z_values = [abs(values[vs[0]] - values[vs[1]]) for vs in versus]
    p_values = [2*(1-st.norm.cdf(abs(z))) for z in z_values]
    # Sort values by p_value so that p_0 < p_1
    p_values, z_values, comparisons = map(list, zip(*sorted(zip(p_values, z_values, comparisons), key=lambda t: t[0])))
    m = int(k*(k-1)/2.)
    adj_p_values = [min(m*p_value,1) for p_value in p_values]

    return comparisons, z_values, p_values, adj_p_values


def test_all_pairs(res_dict):
    test = dict()
    pri = dict(zip(res_dict.keys(),range(len(res_dict))))
    for method1, df1 in res_dict.items():
        for method2, df2 in res_dict.items():
            if method1 != method2 and pri[method1] < pri[method2]:
                res1 = df1.holdout_test_accuracy
                res2 = df2.holdout_test_accuracy
                _, pval = ttest_ind(res1, res2, equal_var=False)
                test[(method1, method2)] = {'ttest': pval}
                _, pval = wilcoxon(res1, res2)
                test[(method1, method2)]['wilcoxon'] = pval
    return test

