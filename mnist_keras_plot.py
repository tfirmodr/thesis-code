#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May 29 19:45:32 2018

@author: thalita
"""
import numpy as np
import matplotlib.pyplot as plt
import os
import pandas as pd

DIR = "./Results/mnist_keras"
result = pd.read_pickle(os.path.join(DIR,'scores.pkl'))

plt.plot(result['train_size'], result['scores'], 'o:')
plt.xlabel('nbr. training samples per class')
plt.ylabel('accuracy on test set')
plt.title('training of a simple CNN over MNIST with limited training set sizes')
