#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Sep  6 10:23:40 2017

@author: thalita
"""

import tensorflow as tf


x = tf.get_variable('var', [8, 50, 3], dtype=tf.float32)
w = tf.get_variable('filter', [7, 3, 10], dtype=tf.float32)

op = tf.nn.conv1d(x, w, stride=1, padding='SAME')

with tf.Session() as sess:
    sess.run(tf.global_variables_initializer())
    sess.run(op)
    sess.close()
    print('OK!')
