#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar  3 16:35:23 2017

@author: thalita

Main experiment module

"""

import tensorflow as tf
from tensorflow.contrib.learn import Experiment, monitors, RunConfig
from tensorflow.contrib.learn.python.learn import learn_runner
import argparse
import os
import shutil
import sacred
from sacred.config.config_files import save_config_file, load_config_file
from sacred.utils import iterate_flattened, get_by_dotted_path
from stflow import LogFileWriter, LogEstimatorEval

import models
import datasets
from models.metrics import get_pr_function
from utils import make_hash
from utils import get_name, expand_params

tf.logging.set_verbosity(tf.logging.INFO)
tf.logging.info("Current working directory" + os.getcwd())
ex = sacred.Experiment('cnn_pixel_classification')
ex.logger = tf.logging._logger
ex.add_package_dependency("scikit-learn", "0.18.1")
ex.add_package_dependency("scikit-image", "0.12.3")


@ex.named_config
def free_seed():
    seed = None


@ex.config
def default_params():
    seed = 1406182976
    mode = 'train_and_evaluate'
    output = 'results'
    resume = True  # wether to resume previous training session


    batch_size = 1  # mini-batch size for SGD

    data = 'stanford'  # dataset name
    test_size = 0.1  # pct of data reserved for test
    train_size = 0.9  # pct of data on train set
    data_params = datasets.get_by_name(data).default_params

    model = 'multiscale_cnn'  # model name
    model_params = models.get_by_name(model).default_params

    run_params = dict(
        master=None,
        num_cores=0,  # If 0 (default), system picks an appropriate number
        log_device_placement=False,
        gpu_memory_fraction=0.75,
        save_summary_steps=500,
        keep_checkpoint_max=3,
        keep_checkpoint_every_n_hours=10000,
        evaluation_master='')

    exp_params = dict(
        eval_delay_secs=120,  # delay in sec. untill 1st evaluation
        continuous_eval_throttle_secs=60,
        min_eval_frequency=1,
        delay_workers_by_global_step=False)


@ex.named_config
def vgg_default():
    model = 'vgg'
    model_params = models.get_by_name(model).default_params

    data = 'pascalvoc'
    data_params = datasets.get_by_name(data).default_params


@ex.capture
def create_experiment_fn(run_params, exp_params,
                         model, model_params, seed,
                         data, data_params,
                         test_size, train_size,
                         batch_size):
    # Dataset instance to get input fn from
    dataset = datasets.get_dataset(data,
                                   train_size=train_size, test_size=test_size,
                                   seed=seed,
                                   **data_params)
    # Update model params with dataset's n_classes
    model_params.update(n_classes=dataset.n_classes)

    train_input_fn = dataset.get_input_fn('train', batch_size)
    eval_input_fn = dataset.get_input_fn('test', batch_size)

    run_params['save_checkpoints_steps'] = dataset.len_train()//batch_size
    run_params['save_checkpoints_secs'] = None
    run_config = RunConfig(tf_random_seed=seed, **run_params)

    # defining metrics
    def class_accuracy_fn(pred, labels):
        return tf.metrics.mean_per_class_accuracy(labels, pred,
                                                  dataset.n_classes)
    def mean_iou_fn(pred, labels):
        return tf.metrics.mean_iou(labels, pred,
                                   dataset.n_classes)

    eval_metrics = {'pixel_accuracy': tf.metrics.accuracy,
                    'class_accuracy': class_accuracy_fn,
                    'mean_iou': mean_iou_fn}
    # Monitors
    # tf. Experiment uses validation monitor when train_and_evaluate is called
    # I want to use this monitor with the early stopping option , so I defined
    # it here and passed min_eval_fequency=None to tf Experiment so it won't
    # instantiate another validation moitor
    early_stop_monitor = monitors.ValidationMonitor(
            input_fn=eval_input_fn,
            eval_steps=dataset.len_test()//batch_size,
            metrics=eval_metrics,
            every_n_steps=exp_params['min_eval_frequency'],
            early_stopping_rounds=4 * run_params['save_checkpoints_steps'],
            early_stopping_metric='loss',
            early_stopping_metric_minimize=True)
    exp_params['min_eval_frequency'] = None
    # Other training hooks can be passed in the train_monitors param,
    # they are all treated as SessionRunHooks by BaseEstimator in the end
    train_monitors = [early_stop_monitor]

    def experiment_fn(output_dir):
        # get Estimator instance
        estimator = models.get_by_name(model)(
                output_dir,
                params=model_params,
                config=run_config)

        for m in train_monitors:
            m.set_estimator(estimator)

        tfexp = Experiment(
                estimator,
                train_input_fn,
                eval_input_fn,
                eval_metrics=eval_metrics,
                eval_hooks=None,
                eval_steps=dataset.len_test()//batch_size,
                train_monitors=train_monitors,
                train_steps=None,  # number of training steps (None = inf)
                export_strategies=None,
                **exp_params)
        return tfexp

    return experiment_fn, early_stop_monitor


def cmd_line():
    parser = argparse.ArgumentParser(
            description='Run multiscale net experiment')
    parser.add_argument('--mode', default='test',
                        choices=['train_and_evaluate',
                                 'evaluate',
                                 'train',
                                 'test'])
    parser.add_argument('--output', default='result/')
    args = parser.parse_args()
    return args

@ex.capture
def matching_plot_config(output, _run, _config):
    sweep = None
    xaxis = None
    for key, val in iterate_flattened(_config):
        if val is 'sweep':
            sweep = key
        if val is 'xaxis':
            xaxis = key

    result_dir = output
    results = os.listdir(result_dir)

    match_result = []
    xaxis_vals = []
    sweep_vals = []
    # Check all results and gather those with matching model_configs
    for res in results:
        cfg_file = os.path.join(result_dir, res, 'model_config.json')
        model_config = load_config_file(cfg_file)
        match = True
        for key, val in iterate_flattened(model_config):
            if get_by_dotted_path(_config, key) != val:
                if key == sweep:
                    continue
                elif key == xaxis:
                    continue
                else:
                    match = False
                    break
        if match:
            match_result.append(res)
        if match and xaxis:
            xaxis_vals.append(get_by_dotted_path(model_config, xaxis))
        if match and sweep:
            sweep_vals.append(get_by_dotted_path(model_config, sweep))

    output = [match_result]
    if xaxis:
        output.append({xaxis: xaxis_vals})
    if sweep:
        output.append({sweep: sweep_vals})
    return tuple(output)


@ex.command
def delete_model(_run):
    path = get_path()
    named_path = _run.info['named_path']
    shutil.rmtree(path)
    os.remove(named_path)
    ex.logger.info("deleted model: %s, aka %s " % (path, named_path))

@ex.capture
def named_path_from_updates(path, updated_params, _log):
    os.makedirs('named_results', exist_ok=True)
    named = ('named_' + path.split('/')[0] + '/'
             + get_name(updated_params))
    if not os.path.exists(named):
        os.system('ln -s ../' + path + ' ' + named)
    _log.info('Named results: %s', named)
    return named


def named_path(path):
    global ex
    # expand params returns tup (param_grid (list), local(bool))
    updated_params = expand_params(ex)[0][0]
    return named_path_from_updates(path, updated_params)


@ex.command
def get_path(_run, _config, _log,  output,
             model, data,
             seed, train_size, test_size, batch_size):
    def_model = dict(seed=seed,
                     train_size=train_size,
                     test_size=test_size,
                     batch_size=batch_size,
                     model=model,
                     data=data)
    def_model.update(_config['model_params'])
    def_model.update(_config['data_params'])
    subdir = str(make_hash(def_model))
    # path for this model_params dir
    # will be created if does not exist
    path = '/'.join([output, subdir])
    _log.info('Results in %s', path)
    os.makedirs(path, exist_ok=True)
    _run.info['model_hash'] = subdir
    _run.info['model_path'] = path
    save_config_file(def_model, path + '/model_config.json')
    return path


@ex.command
def check_model_exists(path, _run, _config, resume, _log):
    # if it exists, model can be resumed
    def is_empty(d):
        if os.path.exists(path):
            return os.listdir(path) == []
        else:
            return True
    if (os.path.exists(path) and
        os.path.exists(os.path.join(path,'model.ckpt'))):
        if resume:
            _log.warn('Continuing from previously saved model in %s'
                            % path)
            _run.info['resumed_model'] = True
        else:
            raise RuntimeError(
                'Resuming model in %s not allowed (resume=False).\n' +
                'Please specify diferent output directory'+
                ' or run with resume=True' % path)
    else:
        os.makedirs(path, exist_ok=True)
        _run.info['resumed_model'] = False


@ex.capture
@LogFileWriter(ex)
@LogEstimatorEval(ex)
def tf_run(path, mode, _run, _log):
    _log.info('Running in mode: %s', mode)
    # Save this run id in the model dir
    with open(path+'/run_ids', 'a') as f:
        f.write(str(_run._id)+ "\n")

    experiment_fn, validation_monitor = create_experiment_fn()

    result = learn_runner.run(experiment_fn, path, mode)

    return {mode: result,
            'early_stopped' : validation_monitor.early_stopped,
            'best_step' : validation_monitor.best_step,
            'best_value' : validation_monitor.best_value}


@ex.command
def test(_log):
    mode = 'test'
    path = os.path.join(get_path(), 'test/')
    _log.info('Running in mode: %s', mode)

    experiment_fn, validation_monitor = create_experiment_fn()

    result = learn_runner.run(experiment_fn, path, mode)


@ex.command
def launcher(_run, _log):
    path = get_path()
    cfg_file = [f for f in os.listdir(path) if f.find('config_upd_') > -1][0]
    cfg = load_config_file(os.path.join(path, cfg_file))
    _run.info['named_path'] = named_path_from_updates(path, cfg)
    check_model_exists(path)
    tf_run(path)


@ex.main
def main(_run):
    path = get_path()
    _run.info['named_path'] = named_path(path)
    check_model_exists(path)
    tf_run(path)


if __name__=="__main__":
    ex.run_commandline()