#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr 28 12:24:28 2017

@author: thalita
"""
import tensorflow as tf
import numpy as np

from experiment import ex

class SacredRunHook(tf.train.LoggingTensorHook):
    def __init__(self, *varargs, **kwargs):
        tf.train.LoggingTensorHook.__init__(self, *varargs, **kwargs)

    @ex.capture
    def after_run(self, run_context, run_values, _run=None):
        _ = run_context
        if self._should_trigger:
            original = np.get_printoptions()
            np.set_printoptions(suppress=True)
            elapsed_secs, _ = self._timer.update_last_triggered_step(self._iter_count)
            if self._formatter:
                logging.info(self._formatter(run_values.results))
            else:
                stats = []
                for tag in self._tag_order:
                    stats.append("%s = %s" % (tag, run_values.results[tag]))
                    # Saving into sacred's _run.info dict
                    if tag in _run.info:
                        _run.info[tag].append(run_values.results[tag])
                    else:
                        _run.info[tag] = [run_values.results[tag]]

                    if elapsed_secs is not None:
                        logging.info("%s (%.3f sec)", ", ".join(stats), elapsed_secs)
                    else:
                        logging.info("%s", ", ".join(stats))

            np.set_printoptions(**original)
            self._iter_count += 1